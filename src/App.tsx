import '@progress/kendo-theme-default/dist/all.css';

import React from 'react';
import { ThemeProvider } from 'styled-components';
import { I18nextProvider } from 'react-i18next';
import { BrowserRouter } from 'react-router-dom';
import { Provider } from 'react-redux';

import './App.css';
import defaultTheme from './style/themes';
import './style/font.css';
import i18n from './i18n/index';

import storeObj from './store/index';
import MainApp from './MainApp';

function App(): JSX.Element {
   return (
      <ThemeProvider theme={defaultTheme}>
         <I18nextProvider i18n={i18n}>
            <Provider store={storeObj.store}>
               <BrowserRouter>
                  <div className="App">
                     <MainApp />
                  </div>
               </BrowserRouter>
            </Provider>
         </I18nextProvider>
      </ThemeProvider>
   );
}

export default App;
