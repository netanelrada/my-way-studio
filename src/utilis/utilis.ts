import { FilterDescriptor } from '@progress/kendo-data-query';
import { GridColumnProps } from '@progress/kendo-react-grid';
import moment from 'moment';
import { start } from 'repl';
import { IAddress, IFutureLine } from 'src/api/passengerMangerApi/types';
import { StorageKeys } from 'src/types/global';

export const mobilePhoneRegex = new RegExp(/^05[0-9]{1}(-)?\d{7}$/);

export const digitRegex = new RegExp('[0-9]');
export const isoDateRegex = new RegExp(
   /^\d{4}–\d{2}–\d{2}T\d{2}:\d{2}:\d{2}(\.\d{1,}|)Z$/,
);

export const fleetDateFormat = new RegExp(
   /^\d{4}[.|-]\d{2}[.|-]\d{2}\s\d{2}:\d{2}$/,
);

export const stringToBoolean = (value: string | undefined): boolean => {
   if (!value) return false;

   switch (value.toLowerCase().trim()) {
      case 'true':
      case 'yes':
      case '1':
         return true;
      case 'false':
      case 'no':
      case '0':
      case null:
         return false;
      default:
         return Boolean(value);
   }
};

export const isNumber = (value: unknown): boolean => {
   return value !== undefined && value !== null && !Number.isNaN(value);
};

export const isBoolean = (value: unknown): boolean => {
   return typeof value === 'boolean' || value === 0 || value === 1;
};

export const isDate = (value: unknown): boolean => {
   return moment.isDate(value);
};

export const buildFilter = (
   { field, editor }: GridColumnProps,
   value: string | boolean | number | Date,
   operator?: string,
): FilterDescriptor => {
   let filterOpratore: string | Function = 'contains';
   let filterValue;

   if (!operator) {
      if (editor === 'numeric') filterOpratore = 'eq';
      if (editor === 'date') filterOpratore = 'eq';
      if (editor === 'boolean') filterOpratore = 'eq';
   }

   if (editor === 'numeric') filterValue = +value;

   if (editor === 'date') filterValue = (isDate(value) && value) || undefined;

   if (editor === 'boolean')
      filterValue = (isBoolean(value) && !!value) || undefined;

   if (editor === 'text' || !editor) filterValue = value;

   return {
      field,
      operator: filterOpratore,
      value: filterValue,
   };
};

export const distinct = <T = any>(array: Array<T>, key: string): Array<T> => {
   if (array.length && typeof array[0] === 'object' && key) {
      return Array.from(
         new Map(array.map((item: any) => [item[key], item])).values(),
      );
   }

   return Array.from(new Set(array));
};

/**
 * Performs a deep merge of objects and returns new object. Does not modify
 * objects (immutable) and merges arrays via concatenation.
 *
 * @param {...object} objects - Objects to merge
 * @returns {object} New object with merged key/values
 */
// eslint-disable-next-line @typescript-eslint/no-explicit-any
export const mergeDeep = (...objects: any[]): object => {
   const isObject = (obj: unknown) => obj && typeof obj === 'object';

   return objects.reduce((prev, obj) => {
      Object.keys(obj).forEach((key) => {
         const pVal = prev[key];
         const oVal = obj[key];

         if (Array.isArray(pVal) && Array.isArray(oVal)) {
            // eslint-disable-next-line no-param-reassign
            prev[key] = pVal.concat(...oVal);
         } else if (isObject(pVal) && isObject(oVal)) {
            // eslint-disable-next-line no-param-reassign
            prev[key] = mergeDeep(pVal, oVal);
         } else {
            // eslint-disable-next-line no-param-reassign
            prev[key] = oVal;
         }
      });

      return prev;
   }, {});
};

export const getLocalStorageValue = (key: StorageKeys): string => {
   return localStorage.getItem(key) || '';
};

export const RemoveLocalStorageKey = (key: StorageKeys): void => {
   localStorage.removeItem(key);
};

export const setLocalStorageValue = (key: StorageKeys, value: string): void => {
   localStorage.setItem(key, value);
};

export const getSessionStorageValue = (key: StorageKeys): string => {
   return sessionStorage.getItem(key) || '';
};

export const RemoveSessionStorageKey = (key: StorageKeys): void => {
   sessionStorage.removeItem(key);
};

export const setSessionStorageValue = (
   key: StorageKeys,
   value: string,
): void => {
   sessionStorage.setItem(key, value);
};

export const jsonDateReviver = (key: string, value: unknown): unknown => {
   if (typeof value === 'string' && isoDateRegex.test(value)) {
      return new Date(value);
   }

   return value;
};

export const getDatesBetween = (
   startDate: Date,
   EndDate: Date,
): Array<Date> => {
   const a = moment(startDate);
   const b = moment(EndDate);

   const result = [];
   for (let m = moment(a); m.isBefore(b); m.add(1, 'days')) {
      result.push(m.toDate());
   }
   return result;
};

export function* getDatesBetweenItrator(
   startDate: Date,
   EndDate: Date,
): Iterator<Date> {
   const a = moment(startDate);
   const b = moment(EndDate);

   for (let m = moment(a); m.isBefore(b); m.add(1, 'days')) {
      yield m.toDate();
   }
   return null;
}

export const buidAdress = (adress: IAddress | undefined | null): string => {
   if (!adress) return '';

   return `${adress.city}, ${adress.street} ${adress.houseNum}`;
};

export const addOrUpdateItemById = <T>(
   orignalArray: Array<T>,
   data: T,
   idPropName: keyof T,
): Array<T> => {
   let isUpdate = false;

   const arr = [...orignalArray];

   arr.every((item, index) => {
      if (item[idPropName] === data[idPropName]) {
         Object.assign(arr[index], data);
         isUpdate = true;
         return false;
      }
      return true;
   });

   if (!isUpdate) {
      arr.push(data);
   }

   return arr;
};
