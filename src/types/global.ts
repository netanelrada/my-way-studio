export enum StorageKeys {
   DeviceToken = 'deviceToken',
   OnTimeLogin = 'onTimeLogin',
   ReduxState = 'reduxState',
}

export enum StorageType {
   LocalStorage,
   SessionStorag,
}

export enum DateTypeEnum {
   daily = 1,
   weekly = 2,
}

export interface IDateRange {
   startDate: Date;
   endDate: Date | undefined;
}
