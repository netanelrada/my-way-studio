export interface IPassenger {
   code: string;
   passCode: string;
   passId: string;
   fullName: string;
   phone1: string;
   phone2: string;
   orderStation: string;
   time: string;
   city: string;
   street: string;
   house: string;
   actualTime: string;
   remarks?: string;
}

export interface ILine {
   lineCode: number;
   startTime: string | Date;
   endTime: string | Date;
   lineDescription: string;
   lineStatus: number;
   passQty: number;
   driverName: string;
   driverCode: number;
   carNumber: string;
   carTypeName: string;
   visaNumber: string;
   departmentName: string;
   totalKm: string;
   passengers: IPassenger[];
   clientPrice: string;
   accountCode: number;
   departmentCode: string;
   mobilePhone?: string;
}

export interface ILocation {
   dc: number;
   lt: number;
   lg: number;
   t: Date;
   s: string;
   h: string;
}

export enum LineStatus {
   Undifined = 0,
   Ride = 3,
   Ended = 4,
}

export interface IClient {
   accountCode: number;
   clientName: string;
}

export interface IDepartment {
   code: number;
   departmentName: string;
}

export interface IItem {
   value: string | number;
   name: string | number | any;
}

export type DropDownItem<P> = P & { value: number; name: string };
