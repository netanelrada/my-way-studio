import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { languageSettings } from './locales/translations';

i18n.use(initReactI18next).init({
   resources: languageSettings,
   lng: 'he',
   interpolation: {
      escapeValue: false,
   },
});

export default i18n;
