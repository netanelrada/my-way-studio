import 'react-i18next';
// import all namespaces (for the default language, only)
import he from './locales/he.json';

declare module 'react-i18next' {
   // and extend them!
   interface Resources {
      he: typeof he;
   }
}
