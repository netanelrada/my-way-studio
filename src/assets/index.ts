import minibus from './images/Minibus.png';
import target from './images/Vector.svg';
import frontViewSmallBus from './images/frontViewSmallBus.png';
import busAlertIcon from './images/bus_alert_icon.svg';
import alartIcon from './icons/alart.svg';
import steeringWheelIcon from './icons/SteeringWheel.svg';

export const images = {
   minibus,
   target,
   steeringWheelIcon,
   frontViewSmallBus,
   busAlertIcon,
   alartIcon,
};
