import React, { CSSProperties, FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import {
   CompositeFilterDescriptor,
   filterBy,
} from '@progress/kendo-data-query';

import { setFcAccount } from 'src/store/actions/loginAction';
import {
   onDeletePassengers,
   onRetrivePassengersSuccess,
   onSetClientId,
   onSetModefiedPassenger,
} from 'src/store/actions/passengerAction';

import { loginSelector } from 'src/store/selectores/loginSelectors';
import { passengerSelector } from 'src/store/selectores/passengersSelector';

import { IRootReducer } from 'src/store/reducers';
import { IItem } from 'src/types/line';
import {
   IDeletePassengerSagaRequest,
   IPassenger,
} from 'src/api/passengerMangerApi/types';
import { IProps as IDropDown } from 'src/components/DropDown/DropDown';
import { IAccount } from 'src/types/login';

import { getPassengers } from 'src/api/passengerMangerApi/passengerMangerApi';
import { getClinets } from 'src/api/api';

import styles from 'src/components/StyledComponents/StyledComponents.style';
import MyWayTitle from 'src/components/MyWayTitle/MyWayTitle';
import RefreshButton from 'src/components/RefreshButton/RefreshButton';
import Input from 'src/components/commons/Input/Input';
import {
   Button,
   DialogContentText,
   Grid,
   IconButton,
   InputAdornment,
} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteOutlineRoundedIcon from '@material-ui/icons/DeleteOutlineRounded';
import SearchIcon from '@material-ui/icons/Search';
import ConfirmationDialog from 'src/components/ConfirmationDialog/ConfirmationDialog';

import { buidAdress } from 'src/utilis/utilis';

import DropDown, {
   IProps as IDropDownProp,
} from '../../../../components/DropDown/DropDown';
import useFilters, { DropDownState } from './hooks/useFilters';
import PassengersGrid from './PassengersGrid/PassengersGrid';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IPassengerComponents {}

const Passengers: FC<IPassengerComponents> = () => {
   const { t } = useTranslation();
   const dispatch = useDispatch();

   const [providers, setProviders] = useState<IItem[]>([]);
   const [clinetFilter, setClinetFilter] = useState<DropDownState>({
      options: [],
      value: '',
   });

   const [filters, setFilters] = useState<CompositeFilterDescriptor>();
   const [isDeleteDialogOpen, setIsDeleteDialogOpen] = useState<{
      isDeleteAllDialog?: boolean;
      isDeleteDialog?: boolean;
   }>({});

   const [deletePassanger, setDeletePassanger] = useState<
      IPassenger | undefined
   >();

   const { fcAccounts, selectedFcAccount, token } = useSelector(
      (state: IRootReducer) => loginSelector(state),
   );

   const passengers = useSelector((state: IRootReducer) =>
      passengerSelector(state),
   );

   const {
      globalFilterProp,
      cityFilterProp,
      departmentFilterProp,
      passengerTypeFilterProp,
      emplyeeeTypeFilterProp,
      restFilters,
   } = useFilters({
      setFilters,
      clientId: clinetFilter.value,
   });

   const onSetSelectedFcAccount = (payload: IAccount) =>
      dispatch(setFcAccount(payload));

   const onDeletePassengersDispatch = (payload: IDeletePassengerSagaRequest) =>
      dispatch(onDeletePassengers(payload));

   const onSetSelectedClient = (payload: string | number | undefined | null) =>
      dispatch(onSetClientId(payload));

   const onRefresh = () => {
      if (selectedFcAccount) onSetSelectedFcAccount({ ...selectedFcAccount });
   };

   const onDelete = React.useCallback(
      (dataItem: IPassenger) => {
         setDeletePassanger(dataItem);
         setIsDeleteDialogOpen({
            isDeleteAllDialog: false,
            isDeleteDialog: true,
         });
      },
      [setDeletePassanger, setIsDeleteDialogOpen],
   );

   const onEdit = React.useCallback(
      (dataItem: IPassenger) => {
         dispatch(onSetModefiedPassenger(dataItem));
      },
      [dispatch],
   );

   const onClickDeleteAllSelected = React.useCallback(() => {
      const deletedPassanger = passengers.filter((x) => x.isSelected);

      if (!deletedPassanger.length) return;

      setIsDeleteDialogOpen({
         isDeleteAllDialog: true,
         isDeleteDialog: false,
      });
   }, [passengers]);

   const onAddPassenger = React.useCallback(() => {
      const newPassenger: IPassenger = {
         passCode: '',
         contactName: '',
         fullName: '',
         address: [],
         contactPhone1: '',
         eMail: '',
         firstName: '',
         internetLevel: '',
         isActive: '0',
         lastName: '',
         mobilePhone: '',
         passId: '',
         passTypeCode: '',
         phone1: '',
         mainAdress: '',
      };

      dispatch(onSetModefiedPassenger(newPassenger));
   }, [dispatch]);

   const onDeleteAllSelectedConfirmed = () => {
      const deletedPassanger = passengers
         .filter((x) => x.isSelected)
         .map((x) => x.passCode);

      if (!selectedFcAccount) return;

      const { dbUrl = '', proxyUrl } = selectedFcAccount;

      onDeletePassengersDispatch({
         token,
         dbUrl,
         proxyUrl,
         passengers: deletedPassanger,
      });

      setDeletePassanger(undefined);
      setIsDeleteDialogOpen({
         isDeleteAllDialog: false,
         isDeleteDialog: false,
      });
   };

   const onDeleteConfirmed = () => {
      if (!deletePassanger) return;
      const deletedPassanger = [deletePassanger.passCode];

      if (!selectedFcAccount) return;

      const { dbUrl = '', proxyUrl } = selectedFcAccount;

      onDeletePassengersDispatch({
         token,
         dbUrl,
         proxyUrl,
         passengers: deletedPassanger,
      });

      setDeletePassanger(undefined);
      setIsDeleteDialogOpen({
         isDeleteAllDialog: false,
         isDeleteDialog: false,
      });
   };

   const onCloseDeleteDialog = () => {
      setDeletePassanger(undefined);
      setIsDeleteDialogOpen({
         isDeleteAllDialog: false,
         isDeleteDialog: false,
      });
   };

   useEffect(() => {
      const res: IItem[] = fcAccounts.map(
         ({ accountCode, accountName }: IAccount) => ({
            value: accountCode,
            name: accountName,
         }),
      );

      setProviders(res);
   }, [fcAccounts]);

   useEffect(() => {
      const { proxyUrl } = selectedFcAccount || {};

      if (selectedFcAccount) {
         getClinets({ proxyUrl, token, dbUrl: selectedFcAccount.dbUrl }).then(
            (res) => {
               const { clients } = res.data;

               setClinetFilter((preState: any) => ({
                  value: clients.some((x) => x.accountCode === preState.value)
                     ? preState.value || ''
                     : clients.find((x) => x)?.accountCode,
                  options: clients.map((c) => ({
                     value: c.accountCode,
                     name: c.clientName,
                  })),
               }));
            },
         );
      }
   }, [selectedFcAccount, token, dispatch]);

   useEffect(() => {
      dispatch(onSetModefiedPassenger(undefined));
   }, [clinetFilter.value, dispatch, selectedFcAccount]);

   useEffect(() => {
      onSetSelectedClient(clinetFilter.value);
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [clinetFilter.value]);

   useEffect(() => {
      async function loadPassengers() {
         if (selectedFcAccount) {
            const apiResponse = await getPassengers({
               dbUrl: selectedFcAccount?.dbUrl || '',
               proxyUrl: selectedFcAccount?.proxyUrl || '',
               token,
               clientCode: clinetFilter.value,
            });

            const { passList = [] } = apiResponse.data;

            const mappedPassengers = passList.map((x) => ({
               ...x,
               mainAdress: buidAdress(x.address.find((y) => y.isDefault)),
               remarks: x.address.find((y) => y.isDefault)?.remarks,
            }));

            dispatch(onRetrivePassengersSuccess(mappedPassengers));
         }
      }

      if (selectedFcAccount) loadPassengers();
   }, [selectedFcAccount, token, clinetFilter, dispatch]);

   const clinetFilterProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '50%', padding: '0px 5px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('clinet'),
      label: t('clinet'),
      menueItem: clinetFilter.options,
      native: false,
      value: clinetFilter.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | unknown;
         }>,
      ) => {
         const { value } = event.target;

         if (typeof value === 'number' || typeof value === 'string') {
            setClinetFilter((preState) => ({ ...preState, value }));
         }
      },
   };

   const prodvierDrpodown: IDropDownProp = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 170, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('provider'),
      label: t('provider'),
      menueItem: providers,
      native: false,
      value: selectedFcAccount?.accountCode,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: unknown;
         }>,
      ) => {
         const { value } = event.target;
         const fcAccountTarget = fcAccounts.find(
            (f) => f.accountCode === value,
         );
         if (fcAccountTarget) {
            onSetSelectedFcAccount(fcAccountTarget);
         }
      },
   };

   const styleIcon: CSSProperties = {
      fill: '#2196F3',
      border: '1px solid #2196F3',
   };

   const disabledIcon: CSSProperties = {
      fill: 'rgba(0, 0, 0, 0.26)',
      border: '1px solid rgba(0, 0, 0, 0.26)',
   };

   const isDisabled = !passengers.some((x) => x.isSelected);

   const DeleteAllDialogContent = () => {
      return (
         <DialogContentText>
            <span>{t('isApproveDelete')} </span>{' '}
            {(passengers.filter((x) => x.isSelected).length === 1 && (
               <span>{t('passenger')}</span>
            )) || <span>{t('passengers')}</span>}
         </DialogContentText>
      );
   };

   const DeleteDialogContent = () => {
      return (
         <DialogContentText>
            <span>{t('isApproveDelete')} </span> <span>{t('passenger')}</span>
         </DialogContentText>
      );
   };

   const DeleteDialogTitle = () => {
      return <span> {t('approveDelete')} </span>;
   };

   return (
      <>
         <ConfirmationDialog
            isDialogOpen={isDeleteDialogOpen.isDeleteAllDialog || false}
            onCloseDialog={onCloseDeleteDialog}
            onConfirmaDialog={onDeleteAllSelectedConfirmed}
            DialogContent={DeleteAllDialogContent()}
            DialogTitle={DeleteDialogTitle()}
         />
         <ConfirmationDialog
            isDialogOpen={isDeleteDialogOpen.isDeleteDialog || false}
            onCloseDialog={onCloseDeleteDialog}
            onConfirmaDialog={onDeleteConfirmed}
            DialogContent={DeleteDialogContent()}
            DialogTitle={DeleteDialogTitle()}
         />

         <styles.Container style={{ paddingBottom: '0px' }}>
            <styles.HeaderContainer>
               <MyWayTitle />
               <styles.LeftHeaderContainer>
                  <DropDown {...prodvierDrpodown} />
                  <DropDown {...clinetFilterProp} />
                  <RefreshButton onClick={onRefresh} />
               </styles.LeftHeaderContainer>
            </styles.HeaderContainer>
            <styles.Hr />
            <Grid
               container
               direction="row"
               justifyContent="center"
               alignItems="center"
            >
               <Grid item xs={11}>
                  <styles.FilterHeaderContianer>
                     <styles.FilterText>{`${t('emplyeesList')} (${
                        filters
                           ? filterBy(passengers, filters).length
                           : passengers.length
                     })`}</styles.FilterText>
                     <styles.Btn
                        size="small"
                        disabled={!filters}
                        onClick={restFilters}
                     >
                        <styles.FilterText style={{ margin: '0px' }}>
                           {t('clearFilter')}
                        </styles.FilterText>
                     </styles.Btn>
                  </styles.FilterHeaderContianer>
                  <styles.FilterContainer>
                     <Input
                        {...globalFilterProp}
                        endAdornment={
                           <InputAdornment position="end">
                              <SearchIcon />
                           </InputAdornment>
                        }
                     />
                     <Input {...cityFilterProp} />
                     <DropDown {...departmentFilterProp} />
                     <DropDown {...passengerTypeFilterProp} />
                     <DropDown {...emplyeeeTypeFilterProp} />
                  </styles.FilterContainer>
               </Grid>
               <Grid item xs={1} style={{ height: '100%' }}>
                  <styles.FlexContainerWithSpace
                     style={{ height: '100%', alignItems: 'flex-end' }}
                  >
                     <span
                        style={{
                           borderRight: '1px solid #BEBEBE',
                           paddingRight: '1em',
                           height: '40%',
                        }}
                     />
                     <IconButton size="small" onClick={onAddPassenger}>
                        <AddIcon style={styleIcon} />
                     </IconButton>
                     <IconButton
                        size="small"
                        disabled={isDisabled}
                        onClick={onClickDeleteAllSelected}
                     >
                        <DeleteOutlineRoundedIcon
                           style={(!isDisabled && styleIcon) || disabledIcon}
                        />
                     </IconButton>
                  </styles.FlexContainerWithSpace>
               </Grid>
            </Grid>

            <styles.TableContainer>
               <PassengersGrid
                  data={passengers}
                  compositeFilters={filters}
                  onDelete={onDelete}
                  onEdit={onEdit}
               />
            </styles.TableContainer>
         </styles.Container>
      </>
   );
};

export default Passengers;
