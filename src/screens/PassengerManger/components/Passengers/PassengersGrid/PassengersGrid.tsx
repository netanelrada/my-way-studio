import React, {
   useState,
   useCallback,
   FunctionComponent,
   useEffect,
} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { onChangeSelection } from 'src/store/actions/passengerAction';
import { selectedFcAccountSelector } from 'src/store/selectores/loginSelectors';
import { IRootReducer } from 'src/store/reducers';

import {
   getSelectedState,
   GridDataStateChangeEvent,
} from '@progress/kendo-react-grid';
import {
   State,
   SortDescriptor,
   filterBy,
   CompositeFilterDescriptor,
   process,
} from '@progress/kendo-data-query';
import { getter } from '@progress/kendo-react-common';
import KendoGrid, { TableProp } from 'src/components/KendoGrid/KendoGrid';
import KendoDeleteEditeCell from 'src/components/KendoGridCutomeColumn/KendoDeleteEditeCell';

import { IPassenger } from 'src/api/passengerMangerApi/types';
import useColumns from '../hooks/useColumns';

const dataItemKey = 'passCode';
const selectedField = 'isSelected';
const idGetter = getter(dataItemKey);

interface IPassengersGrid {
   data: IPassenger[];
   compositeFilters: CompositeFilterDescriptor | undefined;
   onDelete: (dataItem: IPassenger) => unknown;
   onEdit: (dataItem: IPassenger) => unknown;
}

const PassengersGrid: FunctionComponent<IPassengersGrid> = ({
   data,
   compositeFilters,
   onDelete,
   onEdit,
}: IPassengersGrid) => {
   const dispatch = useDispatch();

   const pageSize = 25;

   const [take, setTake] = useState<number>(pageSize);
   const [skip, setSkip] = useState<number>(0);
   const [sort, setSort] = useState<SortDescriptor[]>([]);
   const [selectedState, setSelectedState] = useState<{
      [id: string]: boolean | number[];
   }>({});

   const selectedFcAccount = useSelector((state: IRootReducer) =>
      selectedFcAccountSelector(state),
   );

   const onSelectionChange = React.useCallback(
      (event) => {
         const newSelectedState = getSelectedState({
            event,
            selectedState,
            dataItemKey,
         });
         setSelectedState(newSelectedState);
      },
      [selectedState],
   );

   const onHeaderSelectionChange = React.useCallback((event) => {
      const checkboxElement = event.syntheticEvent.target;
      const { checked } = checkboxElement;
      const newSelectedState: any = {};
      event.dataItems.forEach((item: any) => {
         newSelectedState[idGetter(item)] = checked;
      });
      setSelectedState(newSelectedState);
   }, []);

   const onDataStateChange = useCallback(
      (event: GridDataStateChangeEvent) => {
         setTake(event.dataState?.take || pageSize);
         setSkip(event.dataState?.skip || 0);
         setSort(event.dataState?.sort || []);
      },
      [setTake, setSkip, setSort],
   );

   const DeleteUpdateCell = useCallback(
      (props) => (
         <KendoDeleteEditeCell {...props} onEdit={onEdit} onDelete={onDelete} />
      ),
      [onDelete, onEdit],
   );

   const dataState: State = {
      take,
      skip,
      sort,
      filter: compositeFilters,
   };

   const processedData = process(data, dataState);

   const { columns } = useColumns({
      overCell: DeleteUpdateCell,
   });

   useEffect(() => {
      const mapped: IPassenger[] | any = data.map((item) => ({
         ...item,
         [selectedField]: selectedState[idGetter(item)],
      }));

      dispatch(onChangeSelection(mapped));

      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [selectedState]);

   useEffect(() => {
      if (compositeFilters) {
         const newSelectedState: any = {};

         filterBy(data, compositeFilters).forEach((item: any) => {
            newSelectedState[idGetter(item)] = selectedState[idGetter(item)];
         });

         setSelectedState(newSelectedState);
      }

      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [compositeFilters]);

   useEffect(() => {
      setSelectedState({});
   }, [selectedFcAccount]);

   const tableProp: TableProp = {
      columns,
      className: 'k-rtl',
      style: {
         height: '100%',
         width: '100%',
         borderRadius: '0.3em',
      },
      wrapperDivStyle: {
         height: '100%',
         width: '100%',
      },
      selectedField,
      selectable: {
         enabled: true,
         drag: false,
         cell: false,
         mode: 'multiple',
      },
      onSelectionChange,
      onHeaderSelectionChange,
      dataItemKey,
      rowHeight: 60,
      navigatable: false,
      data: processedData,
      total: processedData.total,
      pageSize,
      skip,
      sort,
      sortable: true,
      resizable: true,
      reorderable: true,
      scrollable: 'virtual',
      onDataStateChange,
   };

   return <KendoGrid {...tableProp} />;
};

export default PassengersGrid;
