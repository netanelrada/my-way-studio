import { useEffect, useState, Dispatch, SetStateAction } from 'react';
import { useSelector } from 'react-redux';
import { getDepartments, getPassengerType } from 'src/api/api';
import {
   selectedFcAccountSelector,
   tokenSelector,
} from 'src/store/selectores/loginSelectors';
import { IProps as IDropDown } from 'src/components/DropDown/DropDown';
import {
   CompositeFilterDescriptor,
   FilterDescriptor,
} from '@progress/kendo-data-query';
import { useTranslation } from 'react-i18next';
import { InputProp } from 'src/components/commons/Input/Input';
import { IItem } from 'src/types/line';
import { IAddress, IPassenger } from 'src/api/passengerMangerApi/types';

export interface DropDownState {
   options: IItem[];
   value: null | number | string;
}

interface IProps {
   setFilters: Dispatch<SetStateAction<CompositeFilterDescriptor | undefined>>;
   clientId?: number | string | undefined | null;
}

interface IfilterHookResult {
   globalFilterProp: InputProp;
   cityFilterProp: InputProp;
   departmentFilterProp: IDropDown;
   passengerTypeFilterProp: IDropDown;
   emplyeeeTypeFilterProp: IDropDown;
   restFilters: () => void;
}

const useFilters = ({ setFilters, clientId }: IProps): IfilterHookResult => {
   const { t } = useTranslation();

   const selectedFcAccount = useSelector((state: any) =>
      selectedFcAccountSelector(state),
   );

   const token = useSelector((state: any) => tokenSelector(state));

   const [globalFilter, setGlobalFilter] = useState<string>('');
   const [cityFilter, setCityFilter] = useState<string>('');
   const [departmentFilter, setDepartmentFilter] = useState<DropDownState>({
      options: [],
      value: '',
   });
   const [passengerTypeFilter, setPassengerTypeFilter] =
      useState<DropDownState>({
         options: [],
         value: '',
      });
   const [emplyeeStatusFilter, setEmplyeeStatusFilter] =
      useState<DropDownState>({
         options: [
            { name: t('active'), value: '1' },
            { name: t('nonActive'), value: '0' },
         ],
         value: '',
      });

   const restValueDropDown = (preState: DropDownState): DropDownState => ({
      ...preState,
      value: '',
   });

   const restFilters = () => {
      setGlobalFilter('');
      setCityFilter('');
      setDepartmentFilter(restValueDropDown);
      setPassengerTypeFilter(restValueDropDown);
      setEmplyeeStatusFilter(restValueDropDown);
      setFilters(undefined);
   };

   const buildFilters = (): void => {
      const globalFilterArray: Array<FilterDescriptor> = globalFilter
         ? [
              {
                 field: '',
                 operator: (items: IPassenger, currValue: string) => {
                    return (
                       items &&
                       (items.passCode === currValue ||
                          items.firstName?.includes(currValue) ||
                          items.lastName.includes(currValue))
                    );
                 },
                 value: globalFilter,
                 ignoreCase: true,
              },
           ]
         : [];

      const cityFilterArray: Array<FilterDescriptor> = cityFilter
         ? [
              {
                 field: 'address',
                 operator: (items: IAddress[], currValue: string) => {
                    return items?.some((p: IAddress) =>
                       p.city?.includes(currValue),
                    );
                 },
                 value: cityFilter,
                 ignoreCase: true,
              },
           ]
         : [];

      const departmentFilterArray: Array<FilterDescriptor> =
         departmentFilter.value
            ? [
                 {
                    field: 'departmentCode',
                    operator: 'eq',
                    value: departmentFilter.value.toString(),
                 },
              ]
            : [];

      const passengerTypeFilterArray: Array<FilterDescriptor> =
         passengerTypeFilter.value
            ? [
                 {
                    field: 'passTypeCode',
                    operator: 'eq',
                    value: `${passengerTypeFilter.value}`,
                 },
              ]
            : [];

      const emplyeeeStatusFilterArray: Array<FilterDescriptor> =
         emplyeeStatusFilter.value
            ? [
                 {
                    field: 'isActive',
                    operator: 'eq',
                    value: emplyeeStatusFilter.value,
                 },
              ]
            : [];

      const spesificFiltersArr = [
         ...cityFilterArray,
         ...departmentFilterArray,
         ...passengerTypeFilterArray,
         ...emplyeeeStatusFilterArray,
      ];

      if (globalFilterArray.length && spesificFiltersArr.length) {
         setFilters({
            logic: 'and',
            filters: [
               { logic: 'and', filters: spesificFiltersArr },
               { logic: 'or', filters: globalFilterArray },
            ],
         });

         return;
      }

      if (!globalFilterArray.length && spesificFiltersArr.length) {
         setFilters({
            logic: 'and',
            filters: spesificFiltersArr,
         });

         return;
      }

      if (globalFilterArray.length && !spesificFiltersArr.length) {
         setFilters({
            logic: 'or',
            filters: globalFilterArray,
         });

         return;
      }

      setFilters(undefined);
   };

   useEffect(buildFilters, [
      globalFilter,
      cityFilter,
      departmentFilter,
      passengerTypeFilter,
      emplyeeStatusFilter,
      setFilters,
   ]);

   useEffect(() => {
      const { proxyUrl, dbUrl } = selectedFcAccount || {};

      if (token && proxyUrl && dbUrl) {
         getPassengerType({
            proxyUrl,
            dbUrl,
            token,
         }).then((res) => {
            const { passTypes } = res.data;
            setPassengerTypeFilter((preState) => ({
               ...preState,
               options: passTypes.map((d) => ({
                  value: d.passTypeCode,
                  name: d.passType,
               })),
            }));
         });
      }
   }, [selectedFcAccount, clientId, token]);

   useEffect(() => {
      const { proxyUrl } = selectedFcAccount || {};

      if (selectedFcAccount) {
         getDepartments({
            proxyUrl,
            dbUrl: selectedFcAccount.dbUrl,
            token,
            clientCode: clientId,
         }).then((res) => {
            const { departments } = res.data;
            setDepartmentFilter((preState) => ({
               ...preState,
               options: departments.map((d) => ({
                  value: d.code,
                  name: d.departmentName,
               })),
            }));
         });
      }
   }, [selectedFcAccount, clientId, token]);

   const globalFilterProp: InputProp = {
      value: globalFilter,
      label: t('search'),
      size: 'small',
      style: { width: '20%' },
      labelWidth: 50,
      onChange: (e: React.FormEvent<HTMLInputElement> | any) => {
         const { value } = e.currentTarget;
         setGlobalFilter(value);

         buildFilters();
      },
   };

   const cityFilterProp: InputProp = {
      value: cityFilter,
      label: t('city'),
      size: 'small',
      style: { width: '15%' },
      labelWidth: 50,
      onChange: (e: React.FormEvent<HTMLInputElement> | any) => {
         const { value } = e.currentTarget;
         setCityFilter(value);

         buildFilters();
      },
   };

   const dropDownplaceholder: IItem[] = [{ name: t('all'), value: '' }];

   const departmentFilterProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '15%' },
         size: 'small',
      },
      disabled: (!clientId && true) || false,
      autoWidth: false,
      multiple: false,
      labalName: t('department'),
      label: t('department'),
      menueItem: [...departmentFilter.options, ...dropDownplaceholder],
      native: false,
      value: departmentFilter.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | unknown;
         }>,
      ) => {
         const { value } = event.target;

         if (typeof value === 'number' || typeof value === 'string') {
            setDepartmentFilter((preState) => ({ ...preState, value }));
         }

         buildFilters();
      },
   };

   const passengerTypeFilterProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '15%' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('passengerType'),
      label: t('passengerType'),
      menueItem: [...passengerTypeFilter.options, ...dropDownplaceholder],
      native: false,
      value: passengerTypeFilter.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | unknown;
         }>,
      ) => {
         const { value } = event.target;

         if (typeof value === 'number' || typeof value === 'string') {
            setPassengerTypeFilter((preState) => ({ ...preState, value }));
         }

         buildFilters();
      },
   };

   const emplyeeeTypeFilterProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '15%' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('emplyeeStatus'),
      label: t('emplyeeStatus'),
      menueItem: [...emplyeeStatusFilter.options, ...dropDownplaceholder],
      native: false,
      value: emplyeeStatusFilter.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | unknown;
         }>,
      ) => {
         const { value } = event.target;

         if (typeof value === 'number' || typeof value === 'string') {
            setEmplyeeStatusFilter((preState) => ({ ...preState, value }));
         }

         buildFilters();
      },
   };

   return {
      globalFilterProp,
      cityFilterProp,
      departmentFilterProp,
      passengerTypeFilterProp,
      emplyeeeTypeFilterProp,
      restFilters,
   };
};

export default useFilters;
