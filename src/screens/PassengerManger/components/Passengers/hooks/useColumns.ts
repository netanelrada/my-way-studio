import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { GridColumnProps } from '@progress/kendo-react-grid';

export interface IColumnHook {
   overCell: React.ReactNode | any;
   headerSelectionValue?: boolean;
}

const useColumns = ({
   overCell,
   headerSelectionValue,
}: IColumnHook): { columns: Array<GridColumnProps> } => {
   const { t } = useTranslation();

   const columns = useMemo<Array<GridColumnProps>>(
      () => [
         {
            field: 'isSelected',
            headerSelectionValue,
            minResizableWidth: 50,
            reorderable: false,
            width: '50px',
         },
         {
            field: 'passId',
            title: t('passengerCode'),
            editor: 'numeric',
            width: 150,
            minResizableWidth: 50,
         },
         {
            field: 'firstName',
            width: '90px',
            title: t('privateName'),
            minResizableWidth: 50,
         },
         {
            field: 'lastName',
            width: '76px',
            title: t('familyName'),
            minResizableWidth: 50,
         },
         {
            field: 'mobilePhone',
            title: t('description'),
            minResizableWidth: 50,
         },
         {
            field: 'departmentName',
            title: t('department'),
            editor: 'text',
            minResizableWidth: 50,
         },
         {
            field: 'mainAdress',
            title: t('mainAdress'),
            editor: 'text',
            minResizableWidth: 50,
         },
         {
            field: 'remarks',
            title: t('remarks'),
            editor: 'text',
            cell: overCell,
            minResizableWidth: 50,
         },
      ],
      [t, overCell, headerSelectionValue],
   );

   return { columns };
};

export default useColumns;
