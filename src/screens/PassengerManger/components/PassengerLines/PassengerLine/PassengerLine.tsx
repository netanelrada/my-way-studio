import React, { FC, CSSProperties } from 'react';
import { IFutureLine } from 'src/api/passengerMangerApi/types';
import { Box, Typography } from '@material-ui/core';
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import BusinessIcon from '@material-ui/icons/Business';
import { useTranslation } from 'react-i18next';
import useLineStatusTranslate from 'src/screens/Main/components/Lines/hooks/useLineStatusTranslate';
import { images } from 'src/assets/index';

const PassengerLine: FC<IFutureLine> = ({
   line_status,
   line_description,
   driver_name,
   driver_mobile,
   start_time,
   end_time,
   client_name,
   line_date,
}: IFutureLine) => {
   const { t } = useTranslation();
   const getLineTranslation = useLineStatusTranslate();

   const elipesStyle: CSSProperties = {
      backgroundColor: '#EBC951',
      height: '5px',
      width: '5px',
      borderRadius: '50%',
   };

   const textStyle: CSSProperties = {
      whiteSpace: 'nowrap',
      textOverflow: 'ellipsis',
      overflow: 'hidden',
   };

   return (
      <Box minHeight="80px" display="flex" alignItems="center" flexWrap="wrap">
         <Box flexBasis="100%" paddingBottom="5px">
            <Box flexBasis="50%">
               <Typography align="left" style={{ textDecoration: 'underline' }}>
                  {line_description}
               </Typography>
            </Box>

            <Box flexBasis="50%">
               <Typography align="right">
                  <span>{`${getLineTranslation(line_status)}`}</span>
                  {line_status !== 0 && <span style={elipesStyle} />}
               </Typography>
            </Box>
         </Box>

         <Box flexBasis="100%" display="flex">
            <Box flexBasis="33%">
               <div>
                  <Box
                     display="flex"
                     alignItems="center"
                     flexWrap="nowrap"
                     fontWeight="bold"
                  >
                     <AccessTimeIcon />
                     <span style={textStyle}>{line_date}</span>
                  </Box>
               </div>
               <div>
                  <Box display="flex" alignItems="center" flexWrap="nowrap">
                     <AccessTimeIcon style={{ visibility: 'hidden' }} />
                     <span style={textStyle}>{`${start_time || '00:00'} - ${
                        end_time || '00:00'
                     }`}</span>
                  </Box>
               </div>
            </Box>

            <Box flexBasis="33%">
               <div>
                  <Box
                     display="flex"
                     alignItems="center"
                     flexWrap="nowrap"
                     fontWeight="bold"
                  >
                     <img
                        src={images.steeringWheelIcon}
                        style={{ width: '1.5rem' }}
                        alt="steeringWheelIcon"
                     />
                     <span style={textStyle}>{driver_name}</span>
                  </Box>
               </div>
               <div>
                  <Box display="flex" alignItems="center" flexWrap="nowrap">
                     <img
                        src={images.steeringWheelIcon}
                        style={{ width: '1.5rem', visibility: 'hidden' }}
                        alt="steeringWheelIcon"
                     />
                     <span style={textStyle}>{driver_mobile}</span>
                  </Box>
               </div>
            </Box>

            <Box flexBasis="33%">
               <div>
                  <Box display="flex" alignItems="center" flexWrap="nowrap">
                     <BusinessIcon />
                     <span style={textStyle}>{client_name}</span>
                  </Box>
               </div>
               <div> </div>
            </Box>
         </Box>
      </Box>
   );
};

export default PassengerLine;
