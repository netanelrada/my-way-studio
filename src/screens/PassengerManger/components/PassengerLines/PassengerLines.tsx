import React, {
   FC,
   useState,
   useEffect,
   CSSProperties,
   ChangeEvent,
} from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { IRootReducer } from 'src/store/reducers';
import {
   clientIdSelector,
   passengerSelector,
} from 'src/store/selectores/passengersSelector';
import { filterBy } from '@progress/kendo-data-query';

import DirectionsBusIcon from '@material-ui/icons/DirectionsBus';

import { IFutureLine, IPassenger } from 'src/api/passengerMangerApi/types';
import { loginSelector } from 'src/store/selectores/loginSelectors';
import { GetFutureLines } from 'src/api/passengerMangerApi/passengerMangerApi';
import { Box, InputAdornment, Typography, Divider } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import Input from 'src/components/commons/Input/Input';
import { FcResponseState } from 'src/api/types';
import { RefreshTokenStatus } from 'src/types/login';
import { setTokenRefreshStatus } from 'src/store/actions/loginAction';
import PassengerLine from './PassengerLine/PassengerLine';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const PassengerLines: FC<IProps> = () => {
   const { t } = useTranslation();

   const dispatch = useDispatch();

   const [filterQuery, setFilterQuery] = useState<string | undefined>();
   const [lines, setLines] = useState<IFutureLine[]>([]);
   const [filteredLines, setFilteredLines] = useState<IFutureLine[]>([]);
   const [selectedPassenger, setSelectedPassenger] = useState<
      IPassenger | undefined
   >();

   const passengers = useSelector((state: IRootReducer) =>
      passengerSelector(state).filter((x) => x.isSelected),
   );
   const clientId = useSelector((state: IRootReducer) =>
      clientIdSelector(state),
   );

   const { token, selectedFcAccount } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
      const textFilter = e.target.value;
      setFilterQuery(textFilter);
   };

   useEffect(() => {
      const runQuery = () => {
         if (!filterQuery) {
            setFilteredLines(lines);
            return;
         }

         setFilteredLines(
            filterBy(lines, {
               logic: 'and',
               filters: [
                  {
                     field: 'line_description',
                     operator: 'contains',
                     value: filterQuery,
                     ignoreCase: true,
                  },
               ],
            }),
         );
      };

      runQuery();
   }, [filterQuery, lines]);

   useEffect(() => {
      const passenger =
         (passengers.length === 1 && passengers.find((x) => x)) || undefined;
      setSelectedPassenger(passenger);
   }, [passengers]);

   useEffect(() => {
      const getLines = async () => {
         if (
            !selectedFcAccount ||
            !selectedFcAccount.dbUrl ||
            !selectedPassenger
         )
            return;

         const apiResponse = await GetFutureLines({
            dbUrl: selectedFcAccount.dbUrl,
            proxyUrl: selectedFcAccount.dbUrl,
            passCode: selectedPassenger.passCode,
            token,
            clientCode: clientId,
         });

         const { data = [], response } = apiResponse.data;

         if (response === FcResponseState.TokenExpired)
            dispatch(setTokenRefreshStatus(RefreshTokenStatus.Invalid));

         setLines(data);
      };

      getLines();
   }, [selectedPassenger, token, selectedFcAccount, clientId, dispatch]);

   return (
      <div style={{ height: '100%', padding: '5px 5px' }}>
         <Box display="flex" alignItems="center" flexWrap="wrap">
            <Box
               display="flex"
               alignItems="center"
               flexWrap="wrap"
               marginRight="10px"
            >
               <DirectionsBusIcon
                  style={{
                     marginLeft: '10px',
                     background: '#ffedb1',
                     borderRadius: '50%',
                  }}
               />

               <Typography variant="h5">{t('embeddedTravel')}</Typography>
            </Box>

            <Typography align="right">
               <Input
                  size="small"
                  label={t('search')}
                  labelWidth={50}
                  value={filterQuery}
                  onChange={handleChange}
                  endAdornment={
                     <InputAdornment position="end">
                        <SearchIcon />
                     </InputAdornment>
                  }
               />
            </Typography>
         </Box>
         <div style={{ padding: '10px 0px' }}>
            <Divider />
         </div>

         <div style={{ overflowY: 'auto', height: '80%', overflowX: 'hidden' }}>
            {filteredLines.map((add, index, array) => {
               return (
                  (index === array.length - 1 && (
                     <PassengerLine key={add.line_description} {...add} />
                  )) || (
                     <>
                        <PassengerLine key={add.line_description} {...add} />
                        <Divider />
                     </>
                  )
               );
            })}
         </div>
      </div>
   );
};

export default PassengerLines;
