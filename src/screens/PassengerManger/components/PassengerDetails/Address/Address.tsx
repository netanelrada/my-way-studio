import React, { FC } from 'react';
import { IAddress } from 'src/api/passengerMangerApi/types';
import { Grid, Typography } from '@material-ui/core';
import { buidAdress } from 'src/utilis/utilis';
import { useTranslation } from 'react-i18next';

import styles from './Address.style';

const Address: FC<IAddress> = (prop: IAddress) => {
   const { t } = useTranslation();

   const address = buidAdress(prop);
   const { remarks } = prop;
   return (
      <Grid container style={{ minHeight: '50px', margin: '5px 0px' }}>
         <Grid item xs={6}>
            <Typography align="left" variant="body1">
               <span style={{ textDecoration: 'underline' }}>
                  {t('address')}
               </span>
               <span> {address}</span>
            </Typography>
         </Grid>
         <Grid item xs={6}>
            <Typography align="left" variant="body1">
               <span style={{ textDecoration: 'underline' }}>
                  {t('remarks')}
               </span>
               <span> {remarks}</span>
            </Typography>
         </Grid>
      </Grid>
   );
};

export default Address;
