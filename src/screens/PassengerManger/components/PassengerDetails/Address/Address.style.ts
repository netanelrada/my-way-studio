import styled from 'styled-components';

const Container = styled.div`
   width: 100%;
   box-sizing: border-box;
`;

export default {
   Container,
};
