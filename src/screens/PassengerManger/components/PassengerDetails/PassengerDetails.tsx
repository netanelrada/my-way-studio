import React, { FC, useState, useEffect, CSSProperties } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import { IRootReducer } from 'src/store/reducers';
import {
   clientIdSelector,
   passengerSelector,
} from 'src/store/selectores/passengersSelector';

import { Box, Grid } from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
// import PhoneIcon from '@material-ui/icons/Phone';
import PhoneIcon from '@material-ui/icons/PhoneOutlined';
import MailOutlineIcon from '@material-ui/icons/MailOutline';

import { IPassenger } from 'src/api/passengerMangerApi/types';
import { loginSelector } from 'src/store/selectores/loginSelectors';
import Address from './Address/Address';
import styles from './PassengerDetails.style';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface IProps {}

const PassengerDetails: FC<IProps> = () => {
   const { t } = useTranslation();

   const passengers = useSelector((state: IRootReducer) =>
      passengerSelector(state).filter((x) => x.isSelected),
   );

   const clientId = useSelector((state: IRootReducer) =>
      clientIdSelector(state),
   );

   const { token, selectedFcAccount } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const [selectedPassenger, setSelectedPassenger] = useState<
      IPassenger | undefined
   >();

   useEffect(() => {
      const passenger =
         (passengers.length === 1 && passengers.find((x) => x)) || undefined;
      setSelectedPassenger(passenger);
   }, [passengers, token, selectedFcAccount, clientId]);

   const alignIconText: CSSProperties = {
      display: 'flex',
      alignItems: 'center',
      flexWrap: 'wrap',
   };

   const cssRight: CSSProperties = { textAlign: 'right' };
   const paddingStyle: CSSProperties = { paddingRight: '10px' };
   return (
      <>
         {(!selectedPassenger && <div />) || (
            <>
               <styles.Header>
                  <Box
                     display="flex"
                     width="70%"
                     alignItems="center"
                     flexWrap="nowrap"
                     fontWeight="bold"
                  >
                     <PersonIcon
                        color="action"
                        fontSize="medium"
                        style={{
                           marginLeft: '10px',
                           background: '#ffedb1',
                           borderRadius: '50%',
                        }}
                     />
                     <Typography variant="h5" style={{ fontWeight: 'bold' }}>
                        {selectedPassenger?.fullName ||
                           `${
                              selectedPassenger?.firstName &&
                              selectedPassenger?.firstName
                           } ${
                              selectedPassenger?.lastName &&
                              selectedPassenger?.lastName
                           }`}
                     </Typography>
                  </Box>
                  <Box
                     display="flex"
                     width="30%"
                     flexWrap="wrap"
                     alignContent="space-between"
                     alignItems="center"
                  >
                     <Typography variant="subtitle1">
                        {selectedPassenger?.departmentName}
                     </Typography>
                     <Divider
                        orientation="vertical"
                        flexItem
                        style={{
                           height: '1rem',
                           alignSelf: 'center',
                           margin: '0px 5px',
                        }}
                     />
                     <Typography variant="subtitle1">
                        {selectedPassenger?.internetLevel}
                     </Typography>
                  </Box>
               </styles.Header>

               <div>
                  <Typography align="left" variant="h6" style={paddingStyle}>
                     {t('employeeDetails')}
                  </Typography>
               </div>
               <div>
                  <Grid
                     container
                     spacing={2}
                     style={{ padding: '8px' }}
                     alignItems="center"
                  >
                     <Grid item xs={4} style={cssRight}>
                        <div style={alignIconText}>
                           <PhoneIcon
                              color="action"
                              fontSize="small"
                              style={{ fill: '#c7c727' }}
                           />
                           <span> {selectedPassenger?.mobilePhone}</span>
                        </div>
                     </Grid>
                     <Grid item xs={4} style={cssRight}>
                        <div style={alignIconText}>
                           <PhoneIcon
                              color="action"
                              fontSize="small"
                              style={{ fill: '#c7c727' }}
                           />
                           <span>{selectedPassenger?.phone1}</span>
                        </div>
                     </Grid>
                     <Grid item xs={4} style={cssRight}>
                        <div style={alignIconText}>
                           <MailOutlineIcon
                              color="action"
                              fontSize="small"
                              style={{ fill: '#c7c727' }}
                           />
                           <span>{selectedPassenger?.eMail}</span>
                        </div>
                     </Grid>
                  </Grid>
               </div>
               <div>
                  <Typography align="left" variant="subtitle1">
                     <span
                        style={{ ...paddingStyle, textDecoration: 'underline' }}
                     >
                        {t('contact')}
                     </span>{' '}
                     <span>{selectedPassenger?.contactName}</span>
                  </Typography>
               </div>
               <div>
                  <Typography align="left" variant="h6" style={paddingStyle}>
                     {t('antherAddress')}
                  </Typography>
               </div>
               <styles.AddressConatiner>
                  {selectedPassenger?.address?.map((add, index, array) => {
                     return (
                        (index === array.length - 1 && (
                           <Address key={add.addressCode} {...add} />
                        )) || (
                           <>
                              <Address key={add.addressCode} {...add} />
                              <Divider />
                           </>
                        )
                     );
                  })}
               </styles.AddressConatiner>
            </>
         )}
      </>
   );
};

export default PassengerDetails;
