import React, { FC, useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { v4 as GuidService } from 'uuid';

import { useDispatch, useSelector } from 'react-redux';
import {
   selectedFcAccountSelector,
   tokenSelector,
} from 'src/store/selectores/loginSelectors';
import { clientIdSelector } from 'src/store/selectores/passengersSelector';
import { IRootReducer } from 'src/store/reducers';
import { Button, Card, Grid, TextField, Typography } from '@material-ui/core';
import DropDown, {
   IProps as IDropDown,
   MenuItemProps,
} from 'src/components/DropDown/DropDown';
import {
   IModefiedPassenger,
   IPassenger,
   ISetSagaPassenger,
} from 'src/api/passengerMangerApi/types';
import { getDepartments } from 'src/api/api';
import PhoneNumber from 'src/components/PhoneNumber/PhoneNumber';
import {
   onSetPassengers,
   onSetModefiedPassenger,
} from 'src/store/actions/passengerAction';
import AddressForm from './AddressForm/AddressForm';

export interface IPassengerForm {
   passanger: IPassenger;
}

const PassengerForm: FC<IPassengerForm> = ({ passanger }: IPassengerForm) => {
   const { t } = useTranslation();
   const dispatch = useDispatch();

   const selectedFcAccount = useSelector((state: IRootReducer) =>
      selectedFcAccountSelector(state),
   );
   const token = useSelector((state: IRootReducer) => tokenSelector(state));

   const clientId = useSelector((state: IRootReducer) =>
      clientIdSelector(state),
   );

   const [modefiedPassenger, setModefiedPassenger] =
      useState<IModefiedPassenger>(passanger);

   const [emplyeeStatus, setEmplyeeStatus] = useState<MenuItemProps[]>([
      { name: t('active'), value: '1' },
      { name: t('nonActive'), value: '0' },
   ]);

   const [permissions, setPermissions] = useState<MenuItemProps[]>([
      { name: '1', value: '1' }, // [...Array(5).keys()].map((x) => {
      { name: '2', value: '2' }, //    return { name: x, value: x };
      { name: '3', value: '3' }, // }),
      { name: '4', value: '4' },
      { name: '5', value: '5' },
   ]);

   const onSetPassengersDispatch = (payload: ISetSagaPassenger) =>
      dispatch(onSetPassengers(payload));

   // map passanger to Modefied Passenger
   useEffect(() => {
      let { address } = passanger;
      if (!passanger.address || !passanger.address.length) {
         address = [
            { addressCode: GuidService(), isDefault: '1', isNew: true },
         ];
      }

      setModefiedPassenger({
         ...passanger,
         noSuppliermobileNumer:
            passanger.mobilePhone.length > 3
               ? passanger.mobilePhone.substring(3)
               : '',
         supplierMobileNumer:
            passanger.mobilePhone.length > 3
               ? passanger.mobilePhone.substr(0, 3)
               : '',
         supplierphone1:
            passanger.phone1.length > 3 ? passanger.phone1.substr(0, 3) : '',
         noSupplierPhone1:
            passanger.phone1.length > 3 ? passanger.phone1.substring(3) : '',
         address,
      });
   }, [passanger]);

   const [departmentOptions, setDepartmentOptions] = useState<MenuItemProps[]>(
      [],
   );

   const isPassengerValid = (): boolean => {
      return (
         (modefiedPassenger.lastName && modefiedPassenger.firstName && true) ||
         false
      );
   };

   const onSubmit = () => {
      const { dbUrl, proxyUrl } = selectedFcAccount || {};
      if (!isPassengerValid() || !clientId || !dbUrl || !token) return;

      const phone1 = `${
         (modefiedPassenger.supplierphone1 || '') +
         (modefiedPassenger.noSupplierPhone1 || '')
      }`;
      const mobilePhone = `${
         (modefiedPassenger.supplierMobileNumer || '') +
         (modefiedPassenger.noSuppliermobileNumer || '')
      }`;

      const payload: ISetSagaPassenger = {
         clientCode: clientId,
         dbUrl,
         proxyUrl,
         token,
         passenger: {
            ...modefiedPassenger,
            phone1,
            mobilePhone,
         },
         address: modefiedPassenger.address,
         firstName: modefiedPassenger.firstName,
         lastName: modefiedPassenger.lastName,
         contactName: modefiedPassenger.contactName,
         contactPhone1: modefiedPassenger.contactPhone1,
         departmentCode: modefiedPassenger.departmentCode,
         eMail: modefiedPassenger.eMail,
         internalCode: modefiedPassenger.passId,
         passcode: modefiedPassenger.passCode,
      };
      onSetPassengersDispatch(payload);
   };

   const onCancel = () => {
      dispatch(onSetModefiedPassenger(undefined));
   };

   const onChangePhoneNumber =
      (prop: 'supplier' | 'phoneNumber') => (value: string | undefined) => {
         setModefiedPassenger({
            ...modefiedPassenger,
            [prop === 'supplier'
               ? 'supplierMobileNumer'
               : 'noSuppliermobileNumer']: value,
         });
      };

   const onChangePhone1 =
      (prop: 'supplier' | 'phoneNumber') => (value: string | undefined) => {
         setModefiedPassenger({
            ...modefiedPassenger,
            [prop === 'supplier' ? 'supplierphone1' : 'noSupplierPhone1']:
               value,
         });
      };

   const handleChange =
      (prop: keyof IPassenger) =>
      (event: React.ChangeEvent<HTMLInputElement>) => {
         setModefiedPassenger({
            ...modefiedPassenger,
            [prop]: event.target.value,
         });
      };

   useEffect(() => {
      if (selectedFcAccount) {
         const { proxyUrl } = selectedFcAccount;

         getDepartments({
            proxyUrl,
            dbUrl: selectedFcAccount.dbUrl,
            token,
            clientCode: clientId,
         }).then((res) => {
            const { departments } = res.data;

            setDepartmentOptions(
               departments.map((d) => ({
                  value: d.code,
                  name: d.departmentName,
               })),
            );
         });
      }
   }, [selectedFcAccount, clientId, token]);

   const departmentProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '100%' },
         size: 'small',
      },

      autoWidth: false,
      multiple: false,
      labalName: t('department'),
      label: t('department'),
      menueItem: departmentOptions,
      native: false,
      value: modefiedPassenger.departmentCode,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: string | undefined | any;
         }>,
      ) => {
         const { value } = event.target;

         setModefiedPassenger({
            ...modefiedPassenger,
            departmentCode: value,
         });
      },
   };

   const emplyeeeStatusProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '100%' },
         size: 'small',
      },

      autoWidth: false,
      multiple: false,
      labalName: t('emplyeeStatus'),
      label: t('emplyeeStatus'),
      menueItem: emplyeeStatus,
      native: false,
      value: modefiedPassenger.isActive,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: string | undefined | any;
         }>,
      ) => {
         const { value } = event.target;

         setModefiedPassenger({
            ...modefiedPassenger,
            isActive: value,
         });
      },
   };

   const permissionProp: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: '100%' },
         size: 'small',
      },

      autoWidth: false,
      multiple: false,
      labalName: t('permission'),
      label: t('permission'),
      menueItem: permissions,
      native: false,
      value: modefiedPassenger.internetLevel,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: string | undefined | any;
         }>,
      ) => {
         const { value } = event.target;

         setModefiedPassenger({
            ...modefiedPassenger,
            internetLevel: value,
         });
      },
   };

   return (
      <Card style={{ padding: '10px' }}>
         <Typography variant="h4" gutterBottom>
            {t('addEmplyee')}
         </Typography>

         <Typography variant="h6" gutterBottom>
            {t('employeeDetails')}
         </Typography>
         <Grid container spacing={3} alignContent="space-around">
            <Grid item xs={12} md={12}>
               <TextField
                  value={modefiedPassenger.passId}
                  onChange={handleChange('passId')}
                  variant="outlined"
                  size="small"
                  label={t('passengerCode')}
                  fullWidth
                  autoComplete="cc-name"
                  type="number"
               />
            </Grid>
            <Grid item xs={6} md={6}>
               <TextField
                  required
                  size="small"
                  variant="outlined"
                  onChange={handleChange('firstName')}
                  value={modefiedPassenger.firstName}
                  error={!modefiedPassenger.firstName}
                  label={t('privateName')}
                  fullWidth
                  autoComplete="cc-name"
               />
            </Grid>
            <Grid item xs={6} md={6}>
               <TextField
                  value={modefiedPassenger.lastName}
                  size="small"
                  onChange={handleChange('lastName')}
                  required
                  error={!modefiedPassenger.lastName}
                  variant="outlined"
                  label={t('familyName')}
                  fullWidth
                  autoComplete="cc-number"
               />
            </Grid>
            <Grid item xs={12} md={12}>
               <DropDown {...departmentProp} />
            </Grid>
            <Grid item xs={12} md={12}>
               <PhoneNumber
                  phoneNumber={modefiedPassenger.noSuppliermobileNumer}
                  supplierPhoneNumber={modefiedPassenger.supplierMobileNumer}
                  isRequired={false}
                  labal={t('mobilePhone')}
                  onChangePhoneNumber={onChangePhoneNumber('phoneNumber')}
                  onChangeSuplliernumber={onChangePhoneNumber('supplier')}
               />
            </Grid>
            <Grid item xs={12} md={12}>
               <Typography variant="h6" gutterBottom>
                  פרטים נוספים
               </Typography>
            </Grid>

            <Grid item xs={12} md={12}>
               <DropDown {...emplyeeeStatusProp} />
            </Grid>
            <Grid item xs={12} md={12}>
               <DropDown {...permissionProp} />
            </Grid>
            <Grid item xs={12} md={12}>
               <TextField
                  size="small"
                  type="email"
                  value={modefiedPassenger.eMail}
                  onChange={handleChange('eMail')}
                  variant="outlined"
                  label={t('email')}
                  fullWidth
               />
            </Grid>
            <Grid item xs={12} md={12}>
               <TextField
                  size="small"
                  value={modefiedPassenger.contactName}
                  onChange={handleChange('contactName')}
                  variant="outlined"
                  label={t('nameOfContractName')}
                  fullWidth
               />
            </Grid>
            <Grid item xs={12} md={12}>
               <PhoneNumber
                  phoneNumber={modefiedPassenger.noSupplierPhone1}
                  supplierPhoneNumber={modefiedPassenger.supplierphone1}
                  isRequired={false}
                  labal={t('anotherPhone')}
                  onChangePhoneNumber={onChangePhone1('phoneNumber')}
                  onChangeSuplliernumber={onChangePhone1('supplier')}
               />
               {/* <TextField
                  size="small"
                  value={modefiedPassenger.phone1}
                  onChange={handleChange('phone1')}
                  variant="outlined"
                  label={t('anotherPhone')}
                  fullWidth
               /> */}
            </Grid>
            {modefiedPassenger.address.map((add) => (
               <AddressForm
                  {...add}
                  updateFunction={setModefiedPassenger}
                  key={add.addressCode}
               />
            ))}

            <Grid item xs={6} md={6}>
               <Button
                  type="button"
                  fullWidth
                  variant="contained"
                  onClick={() => {
                     onCancel();
                  }}
                  color="primary"
               >
                  {t('cancel')}
               </Button>
            </Grid>
            <Grid item xs={6} md={6}>
               <Button
                  onClick={onSubmit}
                  type="button"
                  fullWidth
                  variant="contained"
                  color="primary"
               >
                  {t('approve')}
               </Button>
            </Grid>
         </Grid>
      </Card>
   );
};

export default PassengerForm;
