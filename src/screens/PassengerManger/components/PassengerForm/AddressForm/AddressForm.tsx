import React, { FC, ChangeEvent } from 'react';
import { Grid, TextField, Typography } from '@material-ui/core';
import { IAddress, IPassenger } from 'src/api/passengerMangerApi/types';
import AddressInput, {
   IAddressInput,
} from 'src/components/AddressInput/AddressInput';
import { useTranslation } from 'react-i18next';

export interface IAddressForm extends IAddress {
   updateFunction: React.Dispatch<React.SetStateAction<IPassenger>>;
}

const AddressForm: FC<IAddressForm> = ({
   city,
   houseNum,
   remarks,
   street,
   isDefault,
   addressCode,
   updateFunction,
}: IAddressForm) => {
   const { t } = useTranslation();

   const handelChange = (prop: keyof IAddress, value: string | undefined) => {
      updateFunction((prev: IPassenger) => {
         return {
            ...prev,
            address: prev.address.map((x) => {
               if (x.addressCode === addressCode)
                  return Object.assign(x, { [prop]: value });
               return x;
            }),
         };
      });
   };

   const adressProp: IAddressInput = {
      cityProp: {
         required: true,
         value: city,
         variant: 'outlined',
         fullWidth: true,
         size: 'small',
         label: t('city'),
      },
      streetProp: {
         required: true,
         value: street,
         variant: 'outlined',
         fullWidth: true,
         size: 'small',
         label: t('street'),
      },
      housProp: {
         required: true,
         value: houseNum,
         variant: 'outlined',
         fullWidth: true,
         size: 'small',
         label: t('houseNum'),
      },
      onChange: (
         prop: 'city' | 'street' | 'houseNum',
         value: string | undefined,
      ) => {
         handelChange(prop, value);
      },
   };

   return (
      <>
         <Grid item xs={12} md={12}>
            <Typography variant="h6" gutterBottom>
               {isDefault === '1' ? t('mainAdress') : t('address')}
            </Typography>
         </Grid>
         <Grid item xs={12} md={12}>
            <AddressInput {...adressProp} />
         </Grid>
         <Grid item xs={12} md={12}>
            <TextField
               onChange={(event: ChangeEvent<HTMLInputElement>) => {
                  const { value } = event.target;
                  handelChange('remarks', value);
               }}
               size="small"
               value={remarks}
               variant="outlined"
               label={t('remarks')}
               fullWidth
            />
         </Grid>
      </>
   );
};

export default AddressForm;
