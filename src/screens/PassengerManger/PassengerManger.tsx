import React, { FC } from 'react';
import Passengers from './components/Passengers/Passengers';
import styles from './PassengerManger.style';
import WidgetLayout from './components/WidgetLayout/WidgetLayout';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

const PassengerManger: FC<Props> = () => {
   return (
      <styles.Container>
         <styles.GridContainer
            container
            style={{
               boxShadow: '-1px -1px 11px 0px rgb(0 0 0 / 25%)',
               zIndex: 10,
            }}
         >
            <styles.GridItem item xs={12}>
               <styles.LinesContainer>
                  <Passengers />
               </styles.LinesContainer>
            </styles.GridItem>
         </styles.GridContainer>
         <styles.LayotGridItem className="hidden-scrollbar">
            <WidgetLayout />
         </styles.LayotGridItem>
      </styles.Container>
   );
};

export default PassengerManger;
