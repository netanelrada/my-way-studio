import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';

const Container = styled.div`
   width: 100%;
   height: 100%;

   display: flex;
`;

const LinesContainer = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   justify-content: center;
`;

const GridContainer = styled(Grid)`
   height: 100%;
   overflow: hidden;
`;

const GridItem = styled(Grid)`
   height: 100%;
`;

const LayotGridItem = styled.div`
   height: 100%;
   overflow-y: auto;
   min-width: 500px;
   max-width: 500px;
   background: #ffedb1;
   box-shadow: inset 0px 0px 16px rgba(0, 0, 0, 0.25);
`;

export default {
   Container,
   GridContainer,
   GridItem,
   LinesContainer,
   LayotGridItem,
};
