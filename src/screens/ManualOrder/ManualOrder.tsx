import React, { FC, useEffect, useState } from 'react';
import moment from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { useTranslation } from 'react-i18next';
import './ManualOrder.css';
import { getClinets } from 'src/api/api';
import { IRootReducer } from 'src/store/reducers';
import { IClient, IItem } from 'src/types/line';
import { IAccount, RefreshTokenStatus } from 'src/types/login';
import { DateTypeEnum, IDateRange } from 'src/types/global';
import { FcResponseState } from 'src/api/types';

import { getPassngerShifts } from 'src/api/manualOrderApi/manualOrderApi';

import Box from '@material-ui/core/Box';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import DatePicker, {
   IDatePickerProps,
} from 'src/components/DatePicker/DatePicker';
import WeekDataPicker, {
   IWeekDatePicker,
} from 'src/components/WeekDatePicker/WeekDatePicker';

import { loginSelector } from 'src/store/selectores/loginSelectors';
import { onRetrivePassengerSuccess } from 'src/store/actions/PassengersShiftActionType';
import {
   setFcAccount,
   setTokenRefreshStatus,
} from '../../store/actions/loginAction';

import DropDown, {
   IProps as IDropDownProp,
} from '../../components/DropDown/DropDown';
import styles from './ManualOrder.style';
import ManualOrderWrapper from './ManualOrderWrapper/ManualOrderWrapper';
import useDateFilter from './hook/useDateFilter';
import { ManualOrderShiftDropDown } from './ManualOrderShiftDropDown/ManualOrderShiftDropDown';

export const ManualOrder: FC<{}> = () => {
   const { t } = useTranslation();
   const dispatch = useDispatch();

   const { fcAccounts, selectedFcAccount, token } = useSelector(
      (state: IRootReducer) => loginSelector(state),
   );

   const onSetCompanySelected = (payload: IAccount) =>
      dispatch(setFcAccount(payload));

   const [providers, setProviders] = useState<IItem[]>([]);

   const [clinets, setClinets] = useState<IItem[]>([]);
   const [selectedClinet, setSelectedClinet] = useState<IItem>({
      value: '',
      name: '',
   });

   const [manuallyType, setManuallyType] = useState<DateTypeEnum>(
      DateTypeEnum.weekly,
   );
   const [selectedDate, setSelectedDate] = useState<Date>(new Date());

   const [selectedWeek, setSelectedWeek] = useState<IDateRange>({
      startDate: moment(new Date()).startOf('week').toDate(),
      endDate: moment(new Date()).endOf('week').toDate(),
   });

   const dateFilter = useDateFilter({
      weeklyDate: selectedWeek,
      dayilyDate: selectedDate,
      dateType: manuallyType,
   });

   const onChangeManuallyType = (
      event: React.MouseEvent<HTMLElement>,
      sortKey: string,
   ) => {
      if (+sortKey === DateTypeEnum.daily) setManuallyType(DateTypeEnum.daily);

      if (+sortKey === DateTypeEnum.weekly)
         setManuallyType(DateTypeEnum.weekly);
   };

   useEffect(() => {
      const res: IItem[] = fcAccounts.map(
         ({ accountCode, accountName }: IAccount) => ({
            value: accountCode,
            name: accountName,
         }),
      );

      setProviders(res);
   }, [fcAccounts]);

   useEffect(() => {
      const { proxyUrl } = selectedFcAccount || {};

      if (!selectedFcAccount) return;

      getClinets({ proxyUrl, token, dbUrl: selectedFcAccount.dbUrl }).then(
         (res) => {
            const { clients = [] } = res.data;

            const mapedClients: IItem[] = clients.map(
               ({ accountCode, clientName }: IClient) => ({
                  value: accountCode,
                  name: clientName,
               }),
            );

            setClinets(mapedClients);
         },
      );
   }, [selectedFcAccount, token]);

   useEffect(() => {
      if (clinets.length) setSelectedClinet(clinets[0]);
   }, [clinets]);

   useEffect(() => {
      const { proxyUrl, dbUrl } = selectedFcAccount || {};
      if (!selectedFcAccount || !selectedClinet?.value || !proxyUrl || !dbUrl)
         return;

      const fromDate = moment(dateFilter.startDate)
         .startOf('week')
         .format('yyyy-MM-DD');
      const toDate = moment(dateFilter.endDate)
         .endOf('week')
         .format('yyyy-MM-DD');

      getPassngerShifts({
         proxyUrl,
         dbUrl,
         token,
         fromDate,
         toDate,
         clientCode: +selectedClinet.value,
      }).then((res) => {
         const { passengers = [], response } = res.data;

         if (response === FcResponseState.TokenExpired)
            dispatch(setTokenRefreshStatus(RefreshTokenStatus.Invalid));

         dispatch(onRetrivePassengerSuccess(passengers));
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [selectedClinet, selectedFcAccount, token, dateFilter, dispatch]);

   const prodvierDrpodown: IDropDownProp = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 208, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('provider'),
      label: t('provider'),
      menueItem: providers,
      native: false,
      value: selectedFcAccount?.accountCode,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: unknown;
         }>,
      ) => {
         const { value } = event.target;
         const fcAccountTarget = fcAccounts.find(
            (f) => f.accountCode === value,
         );
         if (fcAccountTarget) {
            onSetCompanySelected(fcAccountTarget);
         }
      },
   };

   const clinetFilterProp: IDropDownProp = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 208, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      menueItem: clinets,
      labalName: t('clinet'),
      label: t('clinet'),

      native: false,
      value: selectedClinet?.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | any;
         }>,
      ) => {
         const { value } = event.target;

         const client = clinets?.find((x) => x.value === +value) || {
            value: '',
            name: '',
         };

         if (typeof value === 'number' || typeof value === 'string') {
            setSelectedClinet(client);
         }
      },
   };

   const weekPickerProp: IWeekDatePicker = {
      prop: {
         inputVariant: 'outlined',
         style: { width: 270 },
         invalidDateMessage: 'תאריך תחילת שבוע לא תקין',
         size: 'small',
         format: 'dd.MM.yyyy',
         okLabel: t('ok'),
         cancelLabel: t('cancel'),
         labelFunc: (
            date: MaterialUiPickersDate,
            invalidLabel: string,
         ): string => {
            const dateClone = moment(date).clone();

            return dateClone && dateClone.isValid()
               ? `${dateClone
                    .startOf('week')
                    .format('DD.MM.yyyy')} - ${dateClone
                    .endOf('week')
                    .format('DD.MM.yyyy')}`
               : invalidLabel;
         },
         onChange: (date: MaterialUiPickersDate) => {
            if (date)
               setSelectedWeek({
                  startDate: moment(date).startOf('week').toDate(),
                  endDate: moment(date).endOf('week').toDate(),
               });
         },

         value: selectedWeek.startDate,
      },
   };

   const datePickerProps: IDatePickerProps = {
      showTodayButton: true,
      inputVariant: 'outlined',
      style: { width: 170 },
      size: 'small',
      format: 'dd.MM.yyyy',
      value: selectedDate,
      onChange: (date: Date | null) => {
         setSelectedDate(date || new Date());
      },
      KeyboardButtonProps: {
         'aria-label': 'change date',
      },
   };

   return (
      <styles.Container className="Manual-Order">
         <styles.GridContainer container spacing={4}>
            <styles.GridItem xs={8} item>
               <Box display="flex" flexDirection="row">
                  <styles.HeaderText>{t('manualOrder')}</styles.HeaderText>
               </Box>
            </styles.GridItem>

            <styles.GridItem xs={4} item>
               <Box display="flex" flexDirection="row-reverse">
                  <DropDown {...clinetFilterProp} />
                  <DropDown {...prodvierDrpodown} />
               </Box>
            </styles.GridItem>

            <styles.GridItem xs={8} item>
               <Box display="flex" flexDirection="row">
                  {manuallyType === DateTypeEnum.weekly && (
                     <WeekDataPicker {...weekPickerProp} />
                  )}

                  {manuallyType === DateTypeEnum.daily && (
                     <DatePicker {...datePickerProps} />
                  )}

                  <ManualOrderShiftDropDown selectedDate={dateFilter} />
               </Box>
            </styles.GridItem>

            <styles.GridItem xs={4} item>
               <Box display="flex" flexDirection="row-reverse">
                  <ToggleButtonGroup
                     style={{ backgroundColor: 'white', width: 300 }}
                     size="small"
                     exclusive
                     value={manuallyType}
                     onChange={onChangeManuallyType}
                  >
                     <ToggleButton
                        style={{ width: 150 }}
                        value={DateTypeEnum.daily}
                     >
                        {t('daily')}
                     </ToggleButton>
                     <ToggleButton
                        style={{ width: 150 }}
                        value={DateTypeEnum.weekly}
                     >
                        {t('weekly')}
                     </ToggleButton>
                  </ToggleButtonGroup>
               </Box>
            </styles.GridItem>
         </styles.GridContainer>
         <styles.GridContainer>
            <ManualOrderWrapper
               startDate={dateFilter.startDate}
               endDate={
                  dateFilter.endDate ||
                  moment(dateFilter.startDate).endOf('day').toDate()
               }
            />
         </styles.GridContainer>
      </styles.Container>
   );
};
