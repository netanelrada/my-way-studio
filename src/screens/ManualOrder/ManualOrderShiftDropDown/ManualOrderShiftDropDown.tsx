import React, { FC, useEffect, useState } from 'react';
import moment from 'moment';

import { IDateRange } from 'src/types/global';
import { IItem } from 'src/types/line';
import { RefreshTokenStatus } from 'src/types/login';

import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import { getShifts } from 'src/api/manualOrderApi/manualOrderApi';

import {
   onRetriveShiftsSuccess,
   onSetSelectedShift,
} from 'src/store/actions/PassengersShiftActionType';
import { setTokenRefreshStatus } from 'src/store/actions/loginAction';
import { IRootReducer } from 'src/store/reducers';
import { loginSelector } from 'src/store/selectores/loginSelectors';

import { FcResponseState } from 'src/api/types';

import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { IShifts, IShiftsDate } from 'src/api/manualOrderApi/types';
import DropDown, {
   MenuItemProps,
   IProps as IDropDown,
} from 'src/components/DropDown/DropDown';
import ShiftDropItem from 'src/components/ShiftDropItem/ShiftDropItem';
import ShiftPickUpItem from 'src/components/ShiftPickUpItem/ShiftPickUpItem';
import { createStyles, makeStyles } from '@material-ui/core/styles';
import { distinct } from 'src/utilis/utilis';
import Tooltip from 'src/components/ToolTip/ToolTip';

const useStyles = makeStyles(() =>
   createStyles({
      root: {
         position: 'relative',
      },
      timeContiner: {
         width: '300px',
      },
      icon: {
         position: 'relative',
         backgroundColor: 'white',
         top: '-10px',
         left: '-10px',
         zIndex: 20,
         color: '#FF4B4B',
      },
   }),
);

export interface IManualOrderShiftDropDown {
   selectedDate: IDateRange;
}
export const ManualOrderShiftDropDown: FC<IManualOrderShiftDropDown> = ({
   selectedDate,
}: IManualOrderShiftDropDown) => {
   const classes = useStyles();

   const { t } = useTranslation();
   const dispatch = useDispatch();

   const { selectedFcAccount, token } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const [shifts, setShifts] = useState<{
      items: MenuItemProps[];
      pickUpItems: MenuItemProps[];
      dropItems: MenuItemProps[];
      rowItems: IShifts[];
   }>({
      items: [],
      dropItems: [],
      pickUpItems: [],
      rowItems: [],
   });
   const [isSelectedDropNonDefault, setIsSelectedDropNonDefault] =
      useState<boolean>(false);
   const [isSelectedPickUpNonDefault, setIsSelectePickUpNonDefault] =
      useState<boolean>(false);
   const [isShown, setIsShown] = useState<{
      isDropShow?: boolean;
      isPickUpShow?: boolean;
   }>({ isDropShow: false, isPickUpShow: false });

   const [selectedShiftId, setselectedShiftId] = useState<IItem>({
      value: '',
      name: '',
   });

   const [selectedPickUpShift, setSelectedPickUpShift] = useState<IItem>({
      value: '',
      name: '',
   });

   const [selectedDropShift, setSelectedDropShift] = useState<IItem>({
      value: '',
      name: '',
   });

   const [selectedShift, setSelectedShift] = useState<IShifts | undefined>();

   useEffect(() => {
      if (!selectedShift || !shifts.rowItems) {
         dispatch(onSetSelectedShift(undefined));
      } else {
         const dateByPickUp: IShiftsDate[] = shifts.rowItems
            .filter((x) => x.pickupTime === selectedShift?.pickupTime)
            .map((x) => x.dates)
            .flat(1);
         const dateByDrop: IShiftsDate[] = shifts.rowItems
            .filter((x) => x.dropTime === selectedShift?.dropTime)
            .map((x) => x.dates)
            .flat(1);

         const shift = {
            ...selectedShift,
            dateByPickUp,
            dateByDrop,
         };

         dispatch(onSetSelectedShift(shift));
      }
   }, [shifts, selectedShift, dispatch]);

   useEffect(() => {
      const { proxyUrl, dbUrl } = selectedFcAccount || {};
      if (!selectedFcAccount || !proxyUrl || !dbUrl) return;

      const fromDate = moment(selectedDate.startDate)
         .startOf('week')
         .format('yyyy-MM-DD');
      const toDate = moment(selectedDate.endDate)
         .endOf('week')
         .format('yyyy-MM-DD');

      getShifts({ proxyUrl, dbUrl, token, fromDate, toDate }).then((res) => {
         const { response } = res.data;
         let { data = [] } = res.data;

         if (response === FcResponseState.TokenExpired)
            dispatch(setTokenRefreshStatus(RefreshTokenStatus.Invalid));

         if (!data || !Array.isArray(data)) data = [];

         const mapedShiftItem: MenuItemProps[] = data.map(
            ({ shiftId, shiftName }: IShifts) => ({
               name: shiftName,
               value: shiftId,
            }),
         );

         const mapedDropItems: MenuItemProps[] = distinct(
            data.map(({ dropTime }: IShifts) => ({
               shiftName: dropTime || '',
               dropTime,
            })),
            'dropTime',
         ).map(({ shiftName, dropTime }) => ({
            name: shiftName,
            value: dropTime,
            child: <ShiftDropItem dropTime={dropTime} />,
         }));

         const mapedPickUpShiftItems: MenuItemProps[] = distinct(
            data.map(({ pickupTime }: IShifts) => ({
               shiftName: pickupTime || '',
               pickupTime,
            })),
            'pickupTime',
         ).map(({ shiftName, pickupTime }) => ({
            name: shiftName,
            value: pickupTime,
            child: <ShiftPickUpItem pickUpTime={pickupTime} />,
         }));

         setShifts({
            items: mapedShiftItem,
            rowItems: data,
            dropItems: mapedDropItems,
            pickUpItems: mapedPickUpShiftItems,
         });

         dispatch(onRetriveShiftsSuccess(data));

         const shift =
            data.find((x) => x.shiftId === selectedShiftId?.value) || data[0];

         setselectedShiftId({ name: shift?.shiftName, value: shift?.shiftId });
         setSelectedPickUpShift({
            name: shift?.shiftName || '',
            value: shift?.pickupTime || '',
         });
         setSelectedDropShift({
            name: shift?.shiftName || '',
            value: shift?.dropTime || '',
         });

         setIsSelectedDropNonDefault(false);
         setIsSelectePickUpNonDefault(false);

         setSelectedShift(shift);
      });
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [selectedFcAccount, token, selectedDate, dispatch]);

   const shiftNameDrpodown: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 270, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('shift'),
      menueItem: shifts.items,
      native: false,
      value: selectedShiftId?.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: unknown;
         }>,
      ) => {
         const { value } = event.target;
         const shift = shifts.rowItems.find((f) => f.shiftId === value);

         setIsSelectedDropNonDefault(false);
         setIsSelectePickUpNonDefault(false);

         setselectedShiftId({
            name: shift?.shiftName || '',
            value: shift?.shiftId || '',
         });

         setSelectedPickUpShift({
            name: shift?.shiftName || '',
            value: shift?.pickupTime || '',
         });

         setSelectedDropShift({
            name: shift?.shiftName || '',
            value: shift?.dropTime || '',
         });

         setSelectedShift(shift);
      },
   };

   const shiftPickUpDropDown: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 140, marginRight: '20px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('pickUp'),
      menueItem: shifts.pickUpItems,
      native: false,
      value: selectedPickUpShift?.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: any;
         }>,
      ) => {
         const { value } = event.target;

         const shift = shifts.rowItems.find(
            (f) => f.shiftId === selectedShiftId.value,
         );

         setIsSelectePickUpNonDefault(value !== shift?.pickupTime && true);

         setSelectedPickUpShift({
            name: value || '',
            value: value || '',
         });

         setSelectedShift(
            (preShift) => (value && { ...preShift, pickupTime: value }) || null,
         );
      },
   };

   const shiftDropDropDown: IDropDown = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 140, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('drop'),
      menueItem: shifts.dropItems,
      native: false,
      value: selectedDropShift?.value,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: any;
         }>,
      ) => {
         const { value } = event.target;
         const shift = shifts.rowItems.find(
            (f) => f.shiftId === selectedShiftId.value,
         );

         setIsSelectedDropNonDefault(value !== shift?.dropTime && true);

         setSelectedDropShift({
            name: value || '',
            value: value || '',
         });

         setSelectedShift(
            (preShift) => (value && { ...preShift, dropTime: value }) || null,
         );
      },
   };

   return (
      <div className={classes.root}>
         <DropDown {...shiftNameDrpodown} />

         <DropDown {...shiftPickUpDropDown} />
         <Tooltip
            title={t('difrentTimeFromDefult')}
            open={isSelectedPickUpNonDefault && isShown.isPickUpShow}
            arrow
            placement="bottom"
         >
            {(isSelectedPickUpNonDefault && (
               <ErrorOutlineIcon
                  onMouseEnter={() => setIsShown({ isPickUpShow: true })}
                  onMouseLeave={() => setIsShown({ isPickUpShow: false })}
                  className={classes.icon}
               />
            )) || <span />}
         </Tooltip>

         <DropDown {...shiftDropDropDown} />
         <Tooltip
            title={t('difrentTimeFromDefult')}
            open={isSelectedDropNonDefault && isShown.isDropShow}
            arrow
            placement="bottom"
         >
            {(isSelectedDropNonDefault && (
               <ErrorOutlineIcon
                  onMouseEnter={() => setIsShown({ isDropShow: true })}
                  onMouseLeave={() => setIsShown({ isDropShow: false })}
                  className={classes.icon}
               />
            )) || <span />}
         </Tooltip>
      </div>
   );
};
