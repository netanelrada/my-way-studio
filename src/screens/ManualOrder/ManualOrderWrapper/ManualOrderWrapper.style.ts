import styled from 'styled-components';

const Container = styled.div`
   width: 100%;
   height: 99vh;
   display: flex;
   flex-direction: column;
   align-items: center;
   padding: 1.25em 0;
`;

const Div = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
   width: 100%;
   padding: 0 1.6875em;
   height: 8%;
`;

const TableContainer = styled.div`
   width: 100%;
   display: flex;
   flex-direction: column;
   align-items: center;
   padding: 0.5em 1.6875em 0 1.6875em;
   height: 75%;
   box-sizing: border-box;
   overflow: hidden;
`;

const Hr = styled.hr`
   width: 100%;
   color: #dfdfdf;
`;

export default {
   Container,
   Div,
   TableContainer,
   Hr,
};
