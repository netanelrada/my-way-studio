import React, { FC, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';

import { CompositeFilterDescriptor } from '@progress/kendo-data-query';
import {
   passengerSelector,
   selectedShiftSelector,
} from 'src/store/selectores/passengerShiftsSelectores';
import { IRootReducer } from 'src/store/reducers';

import Input, { InputProp } from 'src/components/commons/Input/Input';
import { Divider, Grid, InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import SetTransportationModel from 'src/components/Model/SetTransportationModel';

import styles from './ManualOrderWrapper.style';
import ManualOrderTable from '../ManualOrderTable/ManualOrderTable';

interface IProps {
   startDate: Date;
   endDate: Date;
}

const ManualOrderWrapper: FC<IProps> = ({ startDate, endDate }: IProps) => {
   const { t } = useTranslation();

   const selectedShift = useSelector((state: IRootReducer) =>
      selectedShiftSelector(state),
   );

   const [globalFilter, setGlobalFilter] = useState<string>('');
   const [filters, setFilters] = useState<CompositeFilterDescriptor>();

   const data = useSelector((state: IRootReducer) => passengerSelector(state));

   const GlobalFilterProp: InputProp = {
      value: globalFilter,
      label: t('freeSearch'),
      size: 'small',
      style: { width: '20%', marginRight: 10 },
      labelWidth: 78,
      onChange: (e: any) => {
         const { value } = e.currentTarget;
         setGlobalFilter(value);

         const globalFilterArray: CompositeFilterDescriptor | undefined = value
            ? {
                 filters: [
                    {
                       operator: 'contains',
                       field: 'fullName',
                       value,
                    },
                    {
                       operator: 'eq',
                       field: 'passId',
                       value,
                    },
                    {
                       operator: 'contains',
                       field: 'departmentName',
                       value,
                    },
                 ],
                 logic: 'or',
              }
            : undefined;

         setFilters(globalFilterArray);
      },
   };

   return (
      <styles.Container>
         <Divider variant="middle" style={{ width: '100%' }} />

         <styles.Div>
            <Grid
               container
               alignContent="flex-start"
               justifyContent="flex-start"
               spacing={2}
            >
               <SetTransportationModel
                  startDate={startDate}
                  endDate={endDate}
                  selectedShift={selectedShift}
                  onSubmit={(result: any) => {
                     // eslint-disable-next-line no-console
                     console.log(result);
                  }}
               />
               <Input
                  {...GlobalFilterProp}
                  endAdornment={
                     <InputAdornment position="end">
                        <SearchIcon />
                     </InputAdornment>
                  }
               />
            </Grid>
         </styles.Div>

         <styles.TableContainer>
            <ManualOrderTable
               startDate={startDate}
               endDate={endDate}
               data={data}
               compositeFilters={filters}
            />
         </styles.TableContainer>
      </styles.Container>
   );
};

export default ManualOrderWrapper;
