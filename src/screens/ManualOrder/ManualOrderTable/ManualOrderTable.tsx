import React, {
   useState,
   useCallback,
   FunctionComponent,
   useMemo,
} from 'react';
import { useTranslation } from 'react-i18next';

import { GridDataStateChangeEvent } from '@progress/kendo-react-grid';
import {
   State,
   SortDescriptor,
   CompositeFilterDescriptor,
   process,
} from '@progress/kendo-data-query';
import { getter } from '@progress/kendo-react-common';
import KendoGrid, {
   KendoColumnProps,
   TableProp,
} from 'src/components/KendoGrid/KendoGrid';
import moment from 'moment';
import { getDatesBetween } from 'src/utilis/utilis';
import EmplyeeColumn from 'src/components/KendoGridCutomeColumn/EmplyeeColumn';
import CollectDropColumn from 'src/components/KendoGridCutomeColumn/CollectDropColumn';
import CenteredTitleKendoHeader from 'src/components/KendoGridCutomeColumn/CenteredTitleKendoHeader';

const dataItemKey = 'passId';
const selectedField = 'isSelected';
const idGetter = getter(dataItemKey);

interface IProps {
   startDate: Date;
   endDate: Date | undefined;
   data: any[];
   compositeFilters: CompositeFilterDescriptor | undefined;
}

const ManualOrderTable: FunctionComponent<IProps> = ({
   data,
   startDate,
   endDate,
   compositeFilters,
}: IProps) => {
   const { t } = useTranslation();

   const pageSize = 30;

   const [take, setTake] = useState<number>(pageSize);
   const [skip, setSkip] = useState<number>(0);
   const [sort, setSort] = useState<SortDescriptor[]>([]);
   const [selectedState, setSelectedState] = useState<{
      [id: string]: boolean | number[];
   }>({});

   const columns = useMemo<Array<KendoColumnProps>>(() => {
      const dates = getDatesBetween(startDate, endDate || startDate);
      const result: Array<KendoColumnProps> = [
         // {
         //    field: selectedField,
         //    width: '50px',
         //    headerSelectionValue:
         //       data.findIndex((item) => !selectedState[idGetter(item)]) === -1,
         // },
         {
            field: 'fullName',
            title: `רשימת עובדים לשיבוץ`,
            editor: 'numeric',
            filter: 'text',
            width: (dates.length === 1 && '200px') || undefined,
            cell: EmplyeeColumn,
            sortable: true,
            minResizableWidth: 50,
         },
      ];
      dates.forEach((date) => {
         const dayName: any = moment(date).format('dddd').toLowerCase();
         result.push({
            field: moment(date).format('DD.MM.yyyy'),
            title: `${t(dayName)} ${moment(date).format('DD.MM.yyyy')}`,
            editor: 'numeric',
            sortable: false,
            cell: CollectDropColumn,
            minResizableWidth: 50,
            headerCell: CenteredTitleKendoHeader,
         });
      });

      return result;
   }, [t, startDate, endDate]);

   const dataState: State = {
      take,
      skip,
      sort,
      filter: compositeFilters,
   };

   const onDataStateChange = useCallback(
      (event: GridDataStateChangeEvent) => {
         setTake(event.dataState?.take || pageSize);
         setSkip(event.dataState?.skip || 0);
         setSort(event.dataState?.sort || []);
      },
      [setTake, setSkip, setSort],
   );

   const processedData = process(data, dataState);

   const tableProp: TableProp = {
      columns,
      className: 'k-rtl',
      style: {
         height: '100%',
         width: '100%',
         borderRadius: '0.3em',
      },
      wrapperDivStyle: {
         height: '100%',
         width: '100%',
      },
      dataItemKey,
      rowHeight: 80,
      selectedField,
      navigatable: false,
      data: processedData,
      total: processedData.total,
      pageSize,
      skip,
      sort,
      sortable: true,
      resizable: false,
      reorderable: false,
      scrollable: 'virtual',
      onDataStateChange,
   };

   return <KendoGrid {...tableProp} />;
};

export default ManualOrderTable;
