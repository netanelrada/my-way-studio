import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';

const HeaderText = styled.span`
   font-size: 1.7rem;
`;

const Container = styled.div`
   padding: 10px;
   flex-grow: 1;
`;

const LinesContainer = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   justify-content: center;
`;

const GridContainer = styled(Grid)`
   height: 100%;
`;

const GridItem = styled(Grid)`
   height: 100%;
`;

const LayotGridItem = styled(Grid)`
   height: 100%;
   background: #ffedb1;
   box-shadow: inset 0px 0px 16px rgba(0, 0, 0, 0.25);
`;

export default {
   Container,
   GridContainer,
   GridItem,
   LinesContainer,
   LayotGridItem,
   HeaderText,
};
