export enum OrderType {
   Drop = 1,
   PickUp = 2,
}

export interface DayManualOrder {
   isActive: boolean;
   isEnabled: boolean;
   name: string;
   date: Date;
   orderType?: OrderType;
}
