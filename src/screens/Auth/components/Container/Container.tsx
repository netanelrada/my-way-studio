import React from 'react';
import styled from 'styled-components';

const ContainerStyle = styled.div`
   width: 100%;
   height: 90%;
   display: flex;
   flex-direction: column;
   justify-content: center;
   align-items: center;
`;

interface Props {
   children: React.ReactNode;
}

const Container = ({ children }: Props): JSX.Element => {
   return <ContainerStyle>{children}</ContainerStyle>;
};

export default Container;
