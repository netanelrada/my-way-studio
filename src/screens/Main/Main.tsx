import React, { FC } from 'react';
import Lines from './components/Lines/Lines';
import styles from './Main.style';
import WidgetLayout from './components/WidgetLayout/WidgetLayout';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

const Main: FC<Props> = () => {
   return (
      <styles.Container>
         <styles.GridContainer container>
            <styles.GridItem item xs={12}>
               <styles.LinesContainer>
                  <Lines />
               </styles.LinesContainer>
            </styles.GridItem>
         </styles.GridContainer>
         <styles.LayotGridItem className="hidden-scrollbar">
            <WidgetLayout />
         </styles.LayotGridItem>
      </styles.Container>
   );
};

export default Main;
