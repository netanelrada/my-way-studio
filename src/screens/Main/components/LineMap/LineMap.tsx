import React, {
   FunctionComponent,
   useState,
   useEffect,
   useMemo,
   useRef,
   MutableRefObject,
} from 'react';
import moment from 'moment';
import { ChangeEventValue, Coords } from 'google-map-react';
import { useTranslation } from 'react-i18next';

import Box from '@material-ui/core/Box';
import Container from '@material-ui/core/Container';
import Map from 'src/components/Map/Map';

import { getActualDriver, getLocation } from 'src/api/api';
import { useSelector } from 'react-redux';
import { lineSelector } from 'src/store/selectores/linesSelectores';
import {
   loginSelector,
   selectedFcAccountSelector,
} from 'src/store/selectores/loginSelectors';
import { images } from 'src/assets/index';
import { IRootReducer } from 'src/store/reducers';
import Style from './LineMap.style';

interface IMarkers extends Coords {
   deg: string;
   date: Date;
}

const LineMap: FunctionComponent<{}> = () => {
   const { t } = useTranslation();

   const intervalTimeInSecound = 10 ** 4;
   const intervalRef: MutableRefObject<NodeJS.Timeout | undefined> = useRef();
   const defaultZoom = 15;
   const defaultCenter: Coords = useMemo(() => {
      return {
         lat: 31.896997821344108,
         lng: 34.80000972747803,
      };
   }, []);

   const [actualDriverId, setActualDriverId] = useState<
      number | string | undefined
   >();

   const [markers, setMarkers] = useState<Array<IMarkers>>([]);
   const [zoom, setZoom] = useState<number>(defaultZoom);
   const [center, setCenter] = useState<Coords>(defaultCenter);
   const [isZoom, setIsZoom] = useState<boolean>(true);

   const selectedLine = useSelector((state) => lineSelector(state));

   // eslint-disable-next-line @typescript-eslint/naming-convention
   const { gps_server, gps_token } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const selectedFcAccount = useSelector((state: IRootReducer) =>
      selectedFcAccountSelector(state),
   );

   const onFocus = () => {
      setCenter(markers.length ? markers[0] : defaultCenter);
      setZoom(defaultZoom);
      setIsZoom(true);
   };

   const onMapChange = (e: ChangeEventValue) => {
      setCenter(e.center);
      setZoom(e.zoom);
   };

   const onDrag = () => {
      setIsZoom(false);
   };

   useEffect(() => {
      setIsZoom(true);
   }, [selectedLine]);

   useEffect(() => {
      if (selectedLine && selectedFcAccount)
         getActualDriver({
            accountCode: selectedFcAccount.accountCode,
            lineCode: selectedLine.lineCode,
         }).then((result) => {
            setActualDriverId(result.driverCode || selectedLine.driverCode);
         });
   }, [selectedLine, selectedFcAccount, setActualDriverId]);

   // todo useAnimationFrame
   useEffect(() => {
      const clearIntervalFunc = () => {
         if (intervalRef.current) clearInterval(intervalRef.current);
      };

      const onLineChange = () => {
         if (!selectedLine || !selectedFcAccount || !actualDriverId) return;
         getLocation({
            gps_server,
            accountCode: selectedFcAccount.accountCode,
            token: gps_token,
            driverCode: +actualDriverId,
         }).then((result) => {
            const driverLocation: Array<IMarkers> = result.data.map((x) => {
               return { lat: x.lt, lng: x.lg, deg: x.h, date: x.t };
            });

            setMarkers(driverLocation);
            if (isZoom)
               setCenter(
                  driverLocation.length ? driverLocation[0] : defaultCenter,
               );

            if (!isZoom && !driverLocation.length) setCenter(defaultCenter);
         });
      };

      if (intervalRef.current) clearInterval(intervalRef.current);

      if (!selectedLine) {
         setMarkers([]);
         setCenter(defaultCenter);
         return clearIntervalFunc;
      }

      onLineChange();

      const interval = setInterval(() => {
         onLineChange();
      }, intervalTimeInSecound);

      intervalRef.current = interval;

      return clearIntervalFunc;
   }, [
      selectedLine,
      selectedFcAccount,
      gps_server,
      gps_token,
      defaultCenter,
      intervalTimeInSecound,
      isZoom,
      actualDriverId,
   ]);

   return (
      <>
         <Map
            center={center}
            onDrag={onDrag}
            zoom={zoom}
            defaultCenter={defaultCenter}
            onChange={onMapChange}
         >
            {markers.map((mark) => (
               <Style.Image
                  key={mark.date.toString()}
                  lat={mark.lat}
                  lng={mark.lng}
                  src={images.minibus}
                  style={{ width: '25px' }}
                  deg={mark.deg}
               />
            ))}
         </Map>

         {selectedLine && (
            <Container maxWidth="sm" style={{ width: '80%' }}>
               <Box
                  component="span"
                  display="block"
                  p={1}
                  m={1}
                  bgcolor="background.paper"
                  style={{
                     position: 'absolute',
                     left: '5%',
                     top: '1%',
                     borderRadius: '10px',
                     width: '60%',
                  }}
               >
                  {(markers[0] &&
                     `${t('driverLoactionMassage')} ${moment(
                        markers[0].date,
                     ).format('DD/MM/yyyy HH:mm')}`) ||
                     `${t('driverLoactionNotFound')}`}
               </Box>
            </Container>
         )}

         {selectedLine && markers.length > 0 && (
            <Style.Image
               alt="targe selection"
               src={images.target}
               onClick={onFocus}
               style={{
                  position: 'absolute',
                  left: '6%',
                  bottom: '0%',
                  boxShadow: '0px 1px 10px rgb(0 0 0 / 10%)',
                  borderRadius: '66px',
                  cursor: 'pointer',
                  width: '30px',
               }}
            />
         )}
      </>
   );
};

export default LineMap;
