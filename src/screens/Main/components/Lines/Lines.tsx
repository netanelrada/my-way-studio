import React, { ReactElement, useState, useEffect, CSSProperties } from 'react';
import moment from 'moment';
import { useTranslation } from 'react-i18next';
import {
   CompositeFilterDescriptor,
   filterBy,
} from '@progress/kendo-data-query';
import { ILine, IItem } from 'src/types/line';
import { IAccount } from 'src/types/login';
import { useSelector, useDispatch } from 'react-redux';
import {
   loginSelector,
   selectedFcAccountSelector,
   tokenSelector,
} from 'src/store/selectores/loginSelectors';
import { linesSelector } from 'src/store/selectores/linesSelectores';

import Input from 'src/components/commons/Input/Input';
import styles from 'src/components/StyledComponents/StyledComponents.style';

import { useTheme } from 'styled-components';
import { IRootReducer } from 'src/store/reducers';

import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import RefreshButton from 'src/components/RefreshButton/RefreshButton';
import DatePickerWithPrevNext from 'src/components/DatePicker/DatePickerWithPrevNext';

import useFilters from './hooks/useFilters';

import { setFcAccount, setDate } from '../../../../store/actions/loginAction';
import { onRetriveLines } from '../../../../store/actions/LineAction';
import DatePicker, {
   IDatePickerProps,
} from '../../../../components/DatePicker/DatePicker';

import DropDown, {
   IProps as DropDwonProp,
} from '../../../../components/DropDown/DropDown';
import LinesGrid from './LinesGrid/LinesGrid';

interface Props {}

export default function Lines(props: Props): ReactElement {
   const dispatch = useDispatch();

   const onSetCompanySelected = (payload: IAccount) =>
      dispatch(setFcAccount(payload));
   const onSetDateSelected = (payload: Date | null) =>
      dispatch(setDate(payload));
   const { t } = useTranslation();
   const { msColors } = useTheme();

   // const [filterData, setFilterData] = useState<ILine[]>([]);

   const [filters, setFilters] = useState<CompositeFilterDescriptor>();

   const lines: Array<ILine> =
      useSelector((state) => linesSelector(state)) || [];

   const token = useSelector((state: any) => tokenSelector(state));
   const {
      GlobalFilterProp,
      ClinetFilterProp,
      DepartmentFilterProp,
      PassengerFilterProp,
      StatusLineFilterProp,
      restFilters,
   } = useFilters({
      filters,
      setFilters,
   });

   const [providers, setProviders] = useState<IItem[]>([]);

   const { fcAccounts, selectedDate } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const selectedFcAccount = useSelector((state) =>
      selectedFcAccountSelector(state),
   );

   // const [selectedDate, setSelectedDate] = useState<Date | null>(new Date());

   const handleDateChange = (date: Date | null) => {
      onSetDateSelected(date);
   };

   useEffect(() => {
      if (fcAccounts) {
         const res: IItem[] = fcAccounts.map(
            ({ accountCode, accountName }: IAccount) => ({
               value: accountCode,
               name: accountName,
            }),
         );

         setProviders(res);
      }
   }, [fcAccounts]);

   useEffect(() => {
      if (selectedFcAccount && selectedDate)
         dispatch(
            onRetriveLines({
               token,
               clinetProxy: selectedFcAccount.proxyUrl,
               dbUrl: selectedFcAccount.dbUrl,
               relativeDate: moment(selectedDate).format('yyyy.MM.DD'),
            }),
         );
   }, [selectedFcAccount, selectedDate, token, dispatch]);

   const onRefresh = () => {
      if (selectedFcAccount) onSetCompanySelected({ ...selectedFcAccount });
   };

   const prodvierDrpodown: DropDwonProp = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 170, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      labalName: t('provider'),
      label: t('provider'),
      menueItem: providers,
      native: false,
      value: selectedFcAccount?.accountCode,
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: unknown;
         }>,
      ) => {
         const { value } = event.target;
         const fcAccountTarget = fcAccounts.find(
            (f) => f.accountCode === value,
         );
         if (fcAccountTarget) {
            onSetCompanySelected(fcAccountTarget);
         }
         ///  setPrividerValue(providers.find((p) => p.value === value));
      },
   };

   const datePickerProps: IDatePickerProps = {
      showTodayButton: true,
      inputVariant: 'outlined',
      style: { width: 170 },
      size: 'small',
      format: 'dd.MM.yyyy',
      value: selectedDate,
      onChange: handleDateChange,
      KeyboardButtonProps: {
         'aria-label': 'change date',
      },
   };

   const headlineCss: CSSProperties = { fontSize: '30px' };
   return (
      <styles.Container style={{ paddingBottom: '0px' }}>
         <styles.HeaderContainer>
            <styles.TextContainer className="hidden-wl-1280">
               <styles.Text style={headlineCss} color={msColors.darkTextcolor}>
                  Studio
               </styles.Text>
               <styles.Text style={{ whiteSpace: 'break-spaces' }}>
                  {' '}
               </styles.Text>
               <styles.Text style={headlineCss} color={msColors.yellow}>
                  MyWay
               </styles.Text>
            </styles.TextContainer>
            <styles.LeftHeaderContainer>
               <DatePickerWithPrevNext {...datePickerProps} />
               <DropDown {...prodvierDrpodown} />
               <RefreshButton onClick={onRefresh} />
            </styles.LeftHeaderContainer>
         </styles.HeaderContainer>
         <styles.Hr />
         <styles.FilterHeaderContianer>
            <styles.FilterText>{`${t('lineList')} (${
               filters ? filterBy(lines, filters).length : lines.length
            })`}</styles.FilterText>
            <styles.Btn size="small" disabled={!filters} onClick={restFilters}>
               <styles.FilterText style={{ margin: '0px' }}>
                  {t('clearFilter')}
               </styles.FilterText>
            </styles.Btn>
         </styles.FilterHeaderContianer>
         <styles.FilterContainer>
            <Input
               {...GlobalFilterProp}
               endAdornment={
                  <InputAdornment position="end">
                     <SearchIcon />
                  </InputAdornment>
               }
            />
            <DropDown {...ClinetFilterProp} />
            <DropDown {...DepartmentFilterProp} />
            <Input {...PassengerFilterProp} />
            <DropDown {...StatusLineFilterProp} />
         </styles.FilterContainer>

         <styles.TableContainer>
            <LinesGrid data={lines} compositeFilters={filters} />
         </styles.TableContainer>
      </styles.Container>
   );
}
