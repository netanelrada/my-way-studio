import React, {
   useState,
   useEffect,
   useCallback,
   FunctionComponent,
   useMemo,
} from 'react';
import {
   getSelectedState,
   GridDataStateChangeEvent,
   GridSelectionChangeEvent,
} from '@progress/kendo-react-grid';
import {
   State,
   SortDescriptor,
   CompositeFilterDescriptor,
   process,
} from '@progress/kendo-data-query';
import { getter } from '@progress/kendo-react-common';
import { useDispatch } from 'react-redux';

import { onSelectedLineChange } from 'src/store/actions/LineAction';
import { ILine } from 'src/types/line';
import KendoGrid, { TableProp } from 'src/components/KendoGrid/KendoGrid';
import KendoGridRowRender from 'src/components/KendoGrid/KendoGridRowRender';
import useColumns from '../hooks/useColumns';

import './linesGrid.css';

interface Props {
   data: ILine[];
   compositeFilters: CompositeFilterDescriptor | undefined;
}

const LinesGrid: FunctionComponent<Props> = ({
   data,
   compositeFilters,
}: Props) => {
   const dispatch = useDispatch();

   const onChangeSelectedLine = (payload: ILine | undefined) =>
      dispatch(onSelectedLineChange(payload));

   const { columns } = useColumns();

   const pageSize = 25;
   const dataItemKey = 'lineCode';
   const selectedField = 'isSelected';
   const idGetter = getter(dataItemKey);

   const [take, setTake] = useState<number>(pageSize);
   const [skip, setSkip] = useState<number>(0);
   const [sort, setSort] = useState<SortDescriptor[]>([
      { field: 'startTime', dir: 'asc' },
   ]);
   const [selectedState, setSelectedState] = useState<{
      [id: string]: boolean | number[];
   }>({});

   const onSelectionChange = React.useCallback(
      (event: GridSelectionChangeEvent) => {
         const newSelectedState = getSelectedState({
            event,
            selectedState,
            dataItemKey,
         });

         setSelectedState(newSelectedState);
      },
      [selectedState],
   );

   const dataState: State = {
      take,
      skip,
      sort,
      filter: compositeFilters,
   };

   const onDataStateChange = useCallback(
      (event: GridDataStateChangeEvent) => {
         setTake(event.dataState?.take || pageSize);
         setSkip(event.dataState?.skip || 0);
         setSort(event.dataState?.sort || []);
      },
      [setTake, setSkip, setSort],
   );

   const processedData = process(
      data.map((item) => ({
         ...item,
         [selectedField]: selectedState[idGetter(item)],
      })),
      dataState,
   );

   useEffect(() => {
      onChangeSelectedLine(data.find((item) => selectedState[idGetter(item)]));
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [selectedState, data]);

   const tableProp: TableProp = {
      columns,
      className: 'k-rtl',
      style: {
         height: '100%',
         width: '100%',
         borderRadius: '0.3em',
      },
      wrapperDivStyle: {
         height: '100%',
         width: '100%',
      },
      dataItemKey,
      rowHeight: 50,
      selectable: { cell: false, enabled: true, mode: 'single', drag: false },
      onSelectionChange,
      selectedField,
      navigatable: false,
      data: processedData,
      total: processedData.total,
      pageSize,
      skip,
      sort,
      sortable: true,
      resizable: true,
      reorderable: true,
      scrollable: 'virtual',
      onDataStateChange,
   };

   return <KendoGrid {...tableProp} />;
};

export default LinesGrid;
