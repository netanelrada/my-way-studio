import { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import { GridColumnProps } from '@progress/kendo-react-grid';
import LineStatusColumn from 'src/components/KendoGridCutomeColumn/LineStatusColumn';
import PersonKendoHeader from 'src/components/KendoGridCutomeColumn/PersonKendoHeader';

const useColumns = (): { columns: Array<GridColumnProps> } => {
   const { t } = useTranslation();

   const columns = useMemo<Array<GridColumnProps>>(
      () => [
         {
            field: 'lineStatus',
            title: t('status'),
            editor: 'numeric',
            cell: LineStatusColumn,
            width: 60,
            minResizableWidth: 50,
         },
         {
            field: 'startTime',
            width: '90px',
            title: t('startTime'),
            format: '{0:HH:mm}',
            editor: 'date',
            minResizableWidth: 50,
         },
         {
            field: 'endTime',
            width: '76px',
            title: t('endTime'),
            format: '{0:HH:mm}',
            editor: 'date',
            minResizableWidth: 50,
         },
         {
            field: 'lineDescription',
            title: t('description'),
            editor: 'text',
            minResizableWidth: 50,
         },
         {
            field: 'driverName',
            title: t('driverName'),
            editor: 'text',
            width: 100,
            minResizableWidth: 50,
         },
         {
            field: 'carTypeName',
            title: t('carType'),
            editor: 'text',
            width: 100,
            minResizableWidth: 50,
         },
         {
            field: 'carNumber',
            title: t('carNumber'),
            editor: 'text',
            width: 100,
            minResizableWidth: 50,
         },
         {
            field: 'passQty',
            className: 'person-kendo-header',
            headerCell: PersonKendoHeader,
            title: t('passQty'),
            editor: 'numeric',
            minResizableWidth: 50,
            width: 50,
         },
         {
            field: 'visaNumber',
            title: t('visaNumber'),
            editor: 'text',
            width: 150,
            minResizableWidth: 50,
         },
      ],
      [t],
   );

   return { columns };
};

export default useColumns;
