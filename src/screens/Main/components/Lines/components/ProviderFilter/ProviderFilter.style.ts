import styled from 'styled-components';

const Container = styled.div`
   margin-right: 15px;
`;

export default { Container };
