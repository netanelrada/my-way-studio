import React, { useState, useEffect, useContext } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useTranslation } from 'react-i18next';
import { IItem } from 'src/types/line';
import { providers as mockPrviders } from 'src/utilis/mock';
import styles from './ProviderFilter.style';

interface Props {}

const ProviderFilter = (props: Props) => {
   const { t } = useTranslation();
   const [providers, setProviders] = useState<IItem[]>([]);
   const [value, setValue] = useState<any>(-1);

   useEffect(() => {
      setProviders(
         mockPrviders.map((e) => ({
            value: e.accountCode,
            name: e.clientName,
         })),
      );
   }, []);

   const handleChange = (
      event: React.ChangeEvent<{
         name?: string | undefined;
         value: unknown;
      }>,
   ): void => {
      setValue(event.target.value);
   };

   return (
      <styles.Container>
         <FormControl variant="outlined" size="small" style={{ width: 208 }}>
            <InputLabel id="demo-simple-select-outlined-label">
               {t('provider')}
            </InputLabel>
            <Select
               labelId="demo-simple-select-outlined-label"
               id="demo-simple-select-outlined"
               label={t('provider')}
               value={value}
               onChange={handleChange}
            >
               {providers.map((p: IItem) => (
                  <MenuItem key={p.value} value={p.value}>
                     {p.name}
                  </MenuItem>
               ))}
            </Select>
         </FormControl>
      </styles.Container>
   );
};

export default ProviderFilter;
