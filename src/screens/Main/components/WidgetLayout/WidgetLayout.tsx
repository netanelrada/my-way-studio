import React, { FunctionComponent, useState } from 'react';
import { useTranslation } from 'react-i18next';
import './WidgetLayout.style.css';
import {
   TileLayout,
   TileLayoutItem,
   TilePosition,
   TileLayoutRepositionEvent,
} from '@progress/kendo-react-layout';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import LineDetails from '../LineDetails/LineDetails';
import LineMap from '../LineMap/LineMap';

const WidgetLayout: FunctionComponent<{}> = () => {
   const { t } = useTranslation();

   const maxRows = 28;
   const minRows = 8;
   const [defaultTilePosition, setTilePosition] = useState<Array<TilePosition>>(
      [
         {
            col: 1,
            colSpan: 1,
            rowSpan: 18,
         },
         {
            col: 1,
            colSpan: 1,
            rowSpan: 10,
         },
      ],
   );
   const [layout, setLayout] =
      useState<Array<TilePosition>>(defaultTilePosition);

   const tiles: Array<TileLayoutItem> = [
      {
         item: <LineDetails />,
         resizable: 'vertical',
      },
      {
         item: <LineMap />,
         reorderable: false,
         resizable: false,
      },
   ];

   const handleReposition = (e: TileLayoutRepositionEvent) => {
      const [firstElement, secoundElement] = e.value;

      if (firstElement.rowSpan !== defaultTilePosition[0].rowSpan)
         secoundElement.rowSpan = maxRows - firstElement.rowSpan;

      if (secoundElement.rowSpan !== defaultTilePosition[1].rowSpan)
         firstElement.rowSpan = maxRows - secoundElement.rowSpan;

      if (firstElement.rowSpan <= minRows || secoundElement.rowSpan <= minRows)
         return;

      setTilePosition([firstElement, secoundElement]);
      setLayout([firstElement, secoundElement]);
   };

   return (
      <Grid container direction="column">
         <Grid item xs={2}>
            <Typography
               variant="h4"
               paragraph
               style={{ margin: '10px', gap: '10px' }}
            >
               {t('widgetsTitle')}
            </Typography>
         </Grid>
         <Grid>
            <TileLayout
               autoFlow="column"
               columns={1}
               rowHeight={20}
               positions={layout}
               gap={{
                  rows: 10,
                  columns: 10,
               }}
               items={tiles}
               onReposition={handleReposition}
            />
         </Grid>
      </Grid>
   );
};

export default WidgetLayout;
