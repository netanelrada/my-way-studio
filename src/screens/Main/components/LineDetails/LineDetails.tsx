import React, {
   FC,
   useState,
   ChangeEvent,
   useEffect,
   CSSProperties,
} from 'react';
import { useTranslation } from 'react-i18next';
import { useSelector } from 'react-redux';
import moment from 'moment';
import sortBy from 'lodash.sortby';
import useLineStatusTranslate from 'src/screens/Main/components/Lines/hooks/useLineStatusTranslate';
import { IPassenger, LineStatus } from 'src/types/line';

import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import { images } from 'src/assets/index';

import PersonOutlineIcon from '@material-ui/icons/PersonOutline';
import RoomIcon from '@material-ui/icons/RoomOutlined';
import QueryBuilderOutlinedIcon from '@material-ui/icons/QueryBuilderOutlined';
import {
   Accordion,
   AccordionSummary,
   AccordionDetails,
   InputAdornment,
} from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { lineSelector } from 'src/store/selectores/linesSelectores';
import Input from 'src/components/commons/Input/Input';
import styles from './LineDetails.style';
import Passenger from './Passenger/Passenger';
import passengerStyles from './Passenger/Passenger.style';

import './lineDetails.css';

enum Sort {
   PASSENGER,
   TIME,
}

interface Props {}

const LineDetails: FC<Props> = (props: Props) => {
   const { t } = useTranslation();
   const selectedLine = useSelector((state) => lineSelector(state));
   const {
      visaNumber,
      passengers: allPassengers,
      startTime,
      lineDescription,
      mobilePhone,
   } = selectedLine || {};

   const getLineTranslation = useLineStatusTranslate();

   const [passengers, setPassengers] = useState(allPassengers);
   const [sortByType, setSortByType] = useState<string>();

   const [filterState, setfilterState] = useState({
      sortBy: Sort.PASSENGER,
      textFilter: '',
   });

   useEffect(() => {
      setPassengers(allPassengers);
   }, [allPassengers]);

   const onSortBy = (event: React.MouseEvent<HTMLElement>, sortKey: string) => {
      setSortByType(sortKey);
      const sortablePassengers = sortBy(passengers, [sortKey]);
      setPassengers(sortablePassengers);
   };

   const handleChange = (e: ChangeEvent<HTMLInputElement>): void => {
      const textFilter = e.target.value;
      setfilterState((preState) => ({
         ...preState,
         textFilter,
      }));

      if (!textFilter) {
         setPassengers(allPassengers);
         return;
      }

      setPassengers(
         allPassengers?.filter((p) => p.fullName.includes(textFilter)),
      );
   };

   let color = '';
   if (selectedLine && selectedLine.lineStatus !== LineStatus.Undifined)
      color =
         selectedLine.lineStatus === LineStatus.Ended ? '#23DD04' : '#EBB835';

   const elipesStyle: CSSProperties = {
      backgroundColor: `${color}`,
      height: '5px',
      width: '5px',
      borderRadius: '50%',
   };

   const alignIconText: CSSProperties = {
      display: 'flex',
      alignItems: 'center',
      flexWrap: 'wrap',
   };

   return (
      <>
         {!selectedLine ? (
            <styles.EmptyLine>
               <styles.Text> {t('selectLineFromList')}</styles.Text>
            </styles.EmptyLine>
         ) : (
            <>
               <styles.Header>
                  <styles.Row
                     mt={5}
                     mb={20}
                     style={{ fontSize: '1.5em', fontWeight: 'bold' }}
                  >
                     <styles.Image src={images.alartIcon} />

                     <styles.Title>{t('line')}</styles.Title>
                     {visaNumber && (
                        <styles.Title>{`(${visaNumber})`}</styles.Title>
                     )}
                  </styles.Row>
                  <styles.Row style={{ marginLeft: '2%' }}>
                     <styles.Text>{`${getLineTranslation(
                        selectedLine.lineStatus,
                     )}`}</styles.Text>
                     {selectedLine.lineStatus !== 0 && (
                        <span style={elipesStyle} />
                     )}
                  </styles.Row>
                  {/* <styles.Row>
                     <styles.Btn
                        variant="contained"
                        color="primary"
                        size="small"
                     >
                        {t('openline')}
                     </styles.Btn>
                  </styles.Row> */}
               </styles.Header>
               <Accordion className="non-box-shadow">
                  <AccordionSummary expandIcon={<ExpandMoreIcon />}>
                     <styles.Row>
                        <styles.Text
                           style={{
                              borderLeft: '1px solid #BEBEBE',
                              paddingLeft: '1em',
                           }}
                        >
                           {moment(startTime).format('HH:mm')}
                        </styles.Text>
                        <styles.Text>{lineDescription}</styles.Text>
                     </styles.Row>
                  </AccordionSummary>
                  <AccordionDetails>
                     <styles.Text>
                        {t('phone')} {t('driver')}:
                     </styles.Text>
                     <styles.Text>{mobilePhone}</styles.Text>
                  </AccordionDetails>
               </Accordion>

               <styles.FilterContainer
                  style={{ fontSize: '0.8em', padding: '7px' }}
                  className="line-details"
               >
                  <styles.Title style={{ fontWeight: 'bold' }}>{`${t(
                     'passengers',
                  )} (${allPassengers?.length})`}</styles.Title>

                  <Input
                     style={{ width: '40%', backgroundColor: 'white' }}
                     size="small"
                     label={t('search')}
                     labelWidth={50}
                     value={filterState.textFilter}
                     onChange={handleChange}
                     endAdornment={
                        <InputAdornment position="end">
                           <SearchIcon style={{ height: '0.8em' }} />
                        </InputAdornment>
                     }
                  />
                  <styles.Row>
                     <styles.Label style={{ paddingLeft: '1em' }}>
                        {t('sortBy')}
                     </styles.Label>
                     <ToggleButtonGroup
                        style={{ backgroundColor: 'white' }}
                        size="small"
                        exclusive
                        value={sortByType}
                        onChange={onSortBy}
                     >
                        <ToggleButton value="fullName">
                           {t('passenger')}
                        </ToggleButton>
                        <ToggleButton value="time">{t('time')}</ToggleButton>
                     </ToggleButtonGroup>
                  </styles.Row>
               </styles.FilterContainer>
               <passengerStyles.Row mt={20} mr={7}>
                  <passengerStyles.Text
                     opacity="0.5"
                     width="20%"
                     style={alignIconText}
                  >
                     <QueryBuilderOutlinedIcon fontSize="small" />
                     {t('time')}
                  </passengerStyles.Text>

                  <passengerStyles.Text
                     opacity="0.5"
                     width="30%"
                     style={alignIconText}
                  >
                     <PersonOutlineIcon fontSize="small" />
                     <span>{t('passengerName')}</span>
                  </passengerStyles.Text>

                  <passengerStyles.Text
                     opacity="0.5"
                     width="50%"
                     style={alignIconText}
                  >
                     <RoomIcon fontSize="small" />
                     {t('station')}
                  </passengerStyles.Text>
               </passengerStyles.Row>
               <styles.PassengersConatiner
                  className="scroll-sm-size"
                  style={{ overflowX: 'hidden' }}
               >
                  {passengers?.map((passenger: IPassenger) => (
                     <Passenger key={passenger.code} {...passenger} />
                  ))}
               </styles.PassengersConatiner>
            </>
         )}
      </>
   );
};

export default LineDetails;
