import React, { CSSProperties, FC } from 'react';
import { useTranslation } from 'react-i18next';

import {
   Grid,
   Accordion,
   AccordionSummary,
   AccordionDetails,
   Divider,
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import { IPassenger } from 'src/types/line';

import styles from './Passenger.style';

interface Props extends Partial<IPassenger> {}

const Passenger: FC<Props> = ({
   code,
   fullName,
   city,
   street,
   phone1,
   remarks,
   time,
}: Props) => {
   const { t } = useTranslation();

   const style: CSSProperties = {
      fontWeight: 700,
      fontSize: '14px',
      marginRight: '1em',
      marginLeft: '3px',
   };

   const styleBold: CSSProperties = {
      fontWeight: 700,
      fontSize: '14px',
   };

   return (
      <styles.AccordionContainer>
         <Accordion>
            <AccordionSummary expandIcon={<ExpandMoreIcon />}>
               <styles.Text width="20%" style={styleBold}>
                  {time}
               </styles.Text>
               <styles.Text width="30%" style={styleBold}>
                  {fullName}
               </styles.Text>
               <styles.Text width="50%">
                  <styles.Text style={style}>
                     {city}
                     {city && ','}
                  </styles.Text>
                  <styles.Text>{street}</styles.Text>
               </styles.Text>
            </AccordionSummary>
            <AccordionDetails>
               <Grid
                  container
                  direction="row"
                  justify="flex-start"
                  alignContent="flex-start"
               >
                  <Grid item xs={5} style={{ textAlign: 'start' }}>
                     <styles.Text opacity="0.5">
                        {t('passengerCode')}
                     </styles.Text>
                     <styles.Text>{code}</styles.Text>
                  </Grid>
                  <Grid item xs={2} style={{ textAlign: 'start' }}>
                     <Divider
                        style={{ height: '60%' }}
                        orientation="vertical"
                        flexItem
                     />
                  </Grid>
                  <Grid item xs={5} style={{ textAlign: 'start' }}>
                     <styles.Text opacity="0.5">{t('phone')}</styles.Text>
                     <styles.Text>{phone1}</styles.Text>
                  </Grid>
                  <Grid item xs={12} style={{ textAlign: 'start' }}>
                     <styles.Text opacity="0.5">{t('remarks')}</styles.Text>
                     <styles.Text>{remarks}</styles.Text>
                  </Grid>
               </Grid>
            </AccordionDetails>
         </Accordion>
      </styles.AccordionContainer>
   );
};

export default Passenger;
