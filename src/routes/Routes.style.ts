import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';

const Container = styled.div`
   width: 100%;
   height: 100%;
`;

const GridContainer = styled(Grid)`
   height: 100%;
`;

const GridItem = styled(Grid)`
   height: 100%;
`;

export default { Container, GridContainer, GridItem };
