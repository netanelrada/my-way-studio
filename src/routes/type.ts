export interface INavigtionRoute {
   path: string;
   exact: boolean;
   routeName: string;
   main: () => JSX.Element;
   icon: (isActive: boolean) => JSX.Element;
   isHidden?: boolean;
   isDisabled?: boolean;
}
