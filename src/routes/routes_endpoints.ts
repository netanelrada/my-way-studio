export const routesEndpoints = {
   ROOT_AUTH: '/auth',
   HOME: '/',
   MANUAL_ORDER: '/manualOrder',
   OPTIMIZE: '/optimize',
   MANAGE_PASSENGERS: '/managePassengers',
   SHIFTS: '/shifts',
   SETTINGS: '/settings',
   PASSANGER_SETTINGS: '/passengerSettings',
   AUTH: {
      LOGIN: '/login',
      LOGIN_WITH_PIN_CODE: '/login/pin-code',
      LOGIN_WITH_USER: '/login/user',
      SINGUP: '/signup',
      SINGUP_OPTIONS: '/signup-options',
      SIGNUP_WITH_PIN_CODE: '/signup-options/create-pin-code',
      SIGNUP_WITH_USER: '/signup-options/create-user',
   },
};
