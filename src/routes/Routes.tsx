import React, { ReactElement } from 'react';
import { Switch, Route } from 'react-router-dom';
import { ReqStatus } from 'src/api/types';
import Home from './Home/Home';
import useInitalRoute from './hooks/useInitalRoute';
import Auth from './Auth/Auth';
import PrivateRoute from './ PrivateRoute';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

export default function Routes(): ReactElement {
   const { isAuthenticated } = useInitalRoute();

   if (isAuthenticated === ReqStatus.LOADING) return <div />;

   return (
      <Switch>
         <Route path="/auth" component={Auth} />
         <PrivateRoute path="/" component={<Home />} exact={false} />
         {/* <Redirect path="/" to="/auth/signup" /> */}
      </Switch>
   );
}
