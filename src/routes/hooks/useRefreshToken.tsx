import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateToken } from 'src/api/api';
import { setTokenRefreshStatus } from 'src/store/actions/loginAction';
import { IRootReducer } from 'src/store/reducers';
import { loginSelector } from 'src/store/selectores/loginSelectors';
import { RefreshTokenStatus } from 'src/types/login';

const useRefreshToken = (): void => {
   const dispatch = useDispatch();
   const { fcAccounts, refreshTokenStatus, token } = useSelector(
      (state: IRootReducer) => loginSelector(state),
   );

   useEffect(() => {
      if (
         refreshTokenStatus === RefreshTokenStatus.Invalid ||
         !refreshTokenStatus
      ) {
         const promises: Array<any> = [];

         fcAccounts.forEach(({ proxyUrl, contactUUID, dbUrl }) => {
            promises.push(updateToken({ token, proxyUrl, dbUrl, contactUUID }));
         });

         Promise.all(promises).then(() => {
            dispatch(setTokenRefreshStatus(RefreshTokenStatus.Ok));
         });
      }
   }, [refreshTokenStatus, fcAccounts, token, dispatch]);
};

export default useRefreshToken;
