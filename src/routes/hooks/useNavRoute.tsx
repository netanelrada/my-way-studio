import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import DirectionsBusIcon from '@material-ui/icons/DirectionsBus';
import ImportContactsIcon from '@material-ui/icons/ImportContacts';
import EqualizerIcon from '@material-ui/icons/Equalizer';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';
import UpdateIcon from '@material-ui/icons/Update';
import SettingsIcon from '@material-ui/icons/Settings';

import Main from 'src/screens/Main/Main';
import { ManualOrder } from 'src/screens/ManualOrder/ManualOrder';
import PassengerManger from 'src/screens/PassengerManger/PassengerManger';
import Setting from 'src/screens/Setting/Setting';
import { INavigtionRoute } from '../type';
import { routesEndpoints } from '../routes_endpoints';

const useAppRoute = (): { routes: INavigtionRoute[] } => {
   const { t } = useTranslation();
   const routes = useMemo<INavigtionRoute[]>(
      () => [
         {
            path: routesEndpoints.HOME,
            exact: true,
            routeName: t('main'),
            main: () => <Main />,
            icon: (isActive: boolean) => (
               <DirectionsBusIcon color={isActive ? 'primary' : 'inherit'} />
            ),
         },
         {
            path: routesEndpoints.MANUAL_ORDER,
            exact: true,
            routeName: t('manualOrder'),
            main: () => <ManualOrder />,
            icon: (isActive: boolean) => (
               <ImportContactsIcon color={isActive ? 'primary' : 'inherit'} />
            ),
         },
         {
            path: routesEndpoints.OPTIMIZE,
            exact: true,
            routeName: t('placementOptimization'),
            main: () => <Main />,
            icon: (isActive: boolean) => (
               <EqualizerIcon color={isActive ? 'primary' : 'inherit'} />
            ),
            isDisabled: true,
         },
         {
            path: routesEndpoints.MANAGE_PASSENGERS,
            exact: true,
            routeName: t('managePassengers'),
            main: () => <PassengerManger />,
            icon: (isActive: boolean) => (
               <PeopleAltIcon color={isActive ? 'primary' : 'inherit'} />
            ),
            isDisabled: false,
         },
         {
            path: routesEndpoints.SHIFTS,
            exact: true,
            routeName: t('shift'),
            main: () => <Main />,
            icon: (isActive: boolean) => (
               <UpdateIcon color={isActive ? 'primary' : 'inherit'} />
            ),
            isDisabled: true,
         },
         {
            path: routesEndpoints.SETTINGS,
            exact: true,
            routeName: t('settings'),
            main: () => <Setting />,
            icon: (isActive: boolean) => (
               <SettingsIcon color={isActive ? 'primary' : 'inherit'} />
            ),
            isHidden: true,
         },
         {
            path: routesEndpoints.PASSANGER_SETTINGS,
            exact: true,
            routeName: t('passengerSettings'),
            main: () => <Main />,
            icon: (isActive: boolean) => (
               <SettingsIcon color={isActive ? 'primary' : 'inherit'} />
            ),
            isDisabled: true,
         },
      ],
      [t],
   );

   return { routes };
};

export default useAppRoute;
