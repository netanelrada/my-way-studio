import { useCallback, useEffect, useRef } from 'react';

const useAnimationFrame = (
   callback: (...args: unknown[]) => unknown,
   recurring?: boolean,
): { start: Function; stop: Function } => {
   const callBackFunction = useRef(callback);
   const id = useRef(-1);

   callBackFunction.current = callback;

   const start = useCallback(
      (...args) => {
         cancelAnimationFrame(id.current);
         id.current = requestAnimationFrame(() => {
            callBackFunction.current(...args);
            if (recurring) {
               start(...args);
            }
         });
      },
      [recurring],
   );

   const stop = useCallback(() => {
      cancelAnimationFrame(id.current);
   }, []);

   useEffect(() => () => cancelAnimationFrame(id.current), []);

   return { start, stop };
};

export default useAnimationFrame;
