import React from 'react';
import { Route, Switch } from 'react-router-dom';
import NavBar from 'src/components/NavBar/NavBar';
import Main from 'src/screens/Main/Main';
import useNavRoute from '../hooks/useNavRoute';
import useRefreshToken from '../hooks/useRefreshToken';
import styles from './Home.style';

interface Props {}

const Home = (props: Props) => {
   const { routes } = useNavRoute();

   useRefreshToken();

   return (
      <styles.GridContainer container>
         <styles.GridItem item xs={1}>
            <NavBar routes={routes} />
         </styles.GridItem>
         <styles.GridItem item xs={11}>
            <Switch>
               {routes.map((route) => (
                  // Render more <Route>s with the same paths as
                  // above, but different components this time.
                  <Route
                     key={route.routeName}
                     path={route.path}
                     exact={route.exact}
                  >
                     {route.main}
                  </Route>
               ))}
            </Switch>
         </styles.GridItem>
      </styles.GridContainer>
   );
};

export default Home;
