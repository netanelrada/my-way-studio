import styled from 'styled-components';
import Grid from '@material-ui/core/Grid';

const Container = styled.div`
   width: 100%;
   height: 100%;
`;

const GridContainer = styled(Grid)`
   height: 100%;
   overflow: hidden;
`;

const GridItem = styled(Grid)`
   height: 100%;
   /* border: 2px solid ${({ color = 'black' }) => color}; */
`;

export default { Container, GridContainer, GridItem };
