import React, { useRef } from 'react';

const useFocus = (): [React.MutableRefObject<any>, () => void] => {
   const htmlElRef = useRef<any>(null);

   const setFocus = () => {
      if (htmlElRef.current && htmlElRef.current?.focus)
         htmlElRef.current?.focus();
   };

   return [htmlElRef, setFocus];
};

export default useFocus;
