import React, { FC, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setLogout } from 'src/store/actions/loginAction';

import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import Popover from '@material-ui/core/Popover';
import { loginSelector } from 'src/store/selectores/loginSelectors';
import { getLocalStorageValue } from 'src/utilis/utilis';
import { StorageKeys } from 'src/types/global';
import { IRootReducer } from 'src/store/reducers';

import { getUserDetails, logout } from 'src/api/api';

import metropolineImage from 'src/assets/images/MaskGroup.png';
import styles from './Profile.style';
import Languages from './Languages';

interface IProps {}

const Profile: FC<IProps> = (props: IProps) => {
   const { t } = useTranslation();
   const history = useHistory();
   const dispatch = useDispatch();
   const onSetLogout = () => dispatch(setLogout());
   const { selectedFcAccount, userUUID, token, mobile } = useSelector(
      (state: IRootReducer) => loginSelector(state),
   );
   const [anchorEl, setAnchorEl] = React.useState<null | HTMLElement>(null);
   const [userName, setUserName] = useState('');

   useEffect(() => {
      if (selectedFcAccount) {
         const { proxyUrl, dbUrl } = selectedFcAccount;
         getUserDetails({ clinetProxy: proxyUrl, dbUrl, token, mobile }).then(
            (res) => {
               const name = res?.data.data.userName || '';
               setUserName(name);
            },
         );
      }
   }, [selectedFcAccount, token, mobile]);

   const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
      setAnchorEl(event.currentTarget);
   };

   const handleClose = () => {
      setAnchorEl(null);
   };

   const onLogout = async () => {
      const deviceToken = getLocalStorageValue(StorageKeys.DeviceToken);
      if (deviceToken && userUUID) {
         const res = await logout({
            deviceToken,
            userUUID,
         });
         if (res.response === '0') onSetLogout();
         setAnchorEl(null);
         return;
      }

      onSetLogout();
      setAnchorEl(null);
   };

   const navigateToSettings = () => {
      history.push('/settings');
      handleClose();
   };

   return (
      <styles.Container>
         <styles.AvatarContianer>
            <img src={metropolineImage} alt="" />
         </styles.AvatarContianer>
         <styles.Btn onClick={handleClick}>
            <styles.Text>{userName}</styles.Text>
            <KeyboardArrowDownIcon />
         </styles.Btn>
         <Popover
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            anchorOrigin={{
               vertical: 'bottom',
               horizontal: 'center',
            }}
            transformOrigin={{
               vertical: 'top',
               horizontal: 'center',
            }}
            open={Boolean(anchorEl)}
            onClose={handleClose}
         >
            <styles.LanguageContainer>
               <styles.ItemText>{t('language')}</styles.ItemText>
               <Languages />
            </styles.LanguageContainer>
            <styles.Item onClick={navigateToSettings}>
               <styles.ItemText>{t('userSettings')}</styles.ItemText>
            </styles.Item>
            <styles.Item onClick={onLogout}>
               <styles.ItemText isLogout>{t('logout')}</styles.ItemText>
            </styles.Item>
         </Popover>
      </styles.Container>
   );
};

export default Profile;
