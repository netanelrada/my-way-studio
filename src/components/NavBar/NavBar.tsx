import React, { FC } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import { INavigtionRoute } from 'src/routes/type';
import { Box } from '@material-ui/core';
import Grid from '@material-ui/core/Grid';
import Fotter from 'src/components/Footer/Footer';
import Pofile from './Profile/Profile';
import styles from './NavBar.style';

interface Prop {
   routes: INavigtionRoute[];
}

const NavBar: FC<Prop> = ({ routes }: Prop) => {
   const history = useHistory();
   const location = useLocation();

   const onClickItem = (path: string) => {
      history.push(path);
   };

   return (
      <styles.Conatiner>
         <Pofile />
         <Grid
            className="hidden-scrollbar"
            container
            direction="column"
            wrap="nowrap"
            style={{ overflowY: 'auto', height: '100%' }}
         >
            {routes
               .filter((route) => !route.isHidden)
               .map((route) => (
                  <Grid
                     className={route.isDisabled ? 'de-activ' : ''}
                     item
                     key={route.routeName}
                     onClick={() =>
                        !route.isDisabled && onClickItem(route.path)
                     }
                     style={{ margin: '0.5em' }}
                  >
                     {route.icon(location.pathname === route.path)}
                     <styles.Text isActive={location.pathname === route.path}>
                        {route.routeName}
                     </styles.Text>
                  </Grid>
               ))}

            <Box
               display="flex"
               justifyContent="flex-end"
               height="100%"
               flexDirection="column"
            >
               <Fotter
                  style={{
                     borderTop: '1px solid rgb(23 35 45 / 8%)',
                     marginTop: '5px',
                  }}
               />
            </Box>
         </Grid>
      </styles.Conatiner>
   );
};

export default NavBar;
