import { ReactNode } from 'react';

export interface IRoute {
   name: string;
   children: ReactNode;
}
