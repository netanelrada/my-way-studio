import styled from 'styled-components';
import ButtonBase from '@material-ui/core/ButtonBase';

const Conatiner = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   flex-direction: column;
   font-size: 28px;
   background-color: ${({ theme }) => theme.colors.seaBlue};
`;

const Item = styled(ButtonBase)`
   width: 100%;
   height: 8%;
   display: flex;
   flex-direction: column;
   overflow: hidden;
`;

const Text = styled.p<{ isActive: boolean }>`
   margin: 0;
   font-family: ${({ theme }) => theme.fontFamilies.Rubik};
   font-style: normal;
   font-weight: normal;
   font-size: 16px;
   line-height: 19px;
   text-align: center;
   color: ${({ isActive, theme }) =>
      isActive ? theme.colors.blue : theme.colors.black};
`;

export default { Conatiner, Item, Text };
