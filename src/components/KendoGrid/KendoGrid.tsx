import './KendoGrid.css';
import React, {
   FunctionComponent,
   PropsWithChildren,
   CSSProperties,
} from 'react';
import {
   Grid,
   GridColumn,
   GridNoRecords,
   GridProps,
   GridColumnProps,
} from '@progress/kendo-react-grid';
import { useTranslation } from 'react-i18next';

export interface KendoColumnProps extends GridColumnProps {
   keyTable?: string;
}

export interface TableProp extends PropsWithChildren<GridProps> {
   columns: Array<KendoColumnProps>;
   style?: CSSProperties;
   wrapperDivStyle?: CSSProperties;
}

const KendoGrid: FunctionComponent<TableProp> = ({
   columns,
   wrapperDivStyle,
   ...prop
}: PropsWithChildren<TableProp>) => {
   const { t } = useTranslation();

   const { data }: any = prop;
   const totalRecords =
      (data && Array.isArray(data) && data.length) || data?.total || 0;

   return (
      <div
         className="k-rtl kendo-grid"
         no-rows={totalRecords === 0 ? 'no-rows' : undefined}
         style={wrapperDivStyle}
      >
         <Grid {...prop}>
            <GridNoRecords>{t('noRecordWasFound')}</GridNoRecords>
            {columns.map((column) => {
               return (
                  <GridColumn
                     key={column.keyTable || column.field}
                     {...column}
                  />
               );
            })}
         </Grid>
      </div>
   );
};

export default KendoGrid;
