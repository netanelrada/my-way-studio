import React from 'react';
import { GridRowProps } from '@progress/kendo-react-grid';

export default class KendoGridRowRender {
   onClickCallback: (id: number) => void;

   primaryKey: string;

   constructor(onClickCallback: (id: number) => void, primaryKey: string) {
      this.rowRender = this.rowRender.bind(this);
      this.onClickCallback = onClickCallback;
      this.primaryKey = primaryKey;
   }

   // eslint-disable-next-line class-methods-use-this
   rowRender(
      trElement: React.ReactElement<HTMLTableRowElement>,
      rowProp: GridRowProps,
   ): React.ReactNode {
      const trProps = {
         ...trElement.props,
         onClick: () => {
            if (rowProp.dataItem && rowProp.dataItem[this.primaryKey])
               this.onClickCallback(rowProp.dataItem[this.primaryKey]);
         },
      };
      return React.cloneElement(
         trElement,
         { ...trProps },
         trElement.props.children,
      );
   }
}
