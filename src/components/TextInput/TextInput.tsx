import React, { FunctionComponent } from 'react';
import { BaseTextFieldProps } from '@material-ui/core/TextField';
import styles from './TextInput.style';

const TextInput: FunctionComponent<BaseTextFieldProps> = (
   prop: BaseTextFieldProps,
) => {
   return <styles.TextField {...prop} />;
};

export default TextInput;
