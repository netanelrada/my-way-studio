import React, { CSSProperties, FC } from 'react';
import { useTheme } from 'styled-components';
import styles from './MyWayTitle.style';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IMyWayTitle {}

const MyWayTitle: FC<IMyWayTitle> = () => {
   const { msColors } = useTheme();

   const headlineCss: CSSProperties = { fontSize: '30px' };

   return (
      <styles.TextContainer className="hidden-wl-1280">
         <styles.Text style={headlineCss} color={msColors.darkTextcolor}>
            Studio
         </styles.Text>
         <styles.Text style={{ whiteSpace: 'break-spaces' }}> </styles.Text>
         <styles.Text style={headlineCss} color={msColors.yellow}>
            MyWay
         </styles.Text>
      </styles.TextContainer>
   );
};

export default MyWayTitle;
