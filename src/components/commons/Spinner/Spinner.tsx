import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

const Spinner: React.FC = () => {
   return <CircularProgress size="large" />;
};

export default Spinner;
