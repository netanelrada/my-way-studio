import React, { FunctionComponent } from 'react';
import FilterListSharpIcon from '@material-ui/icons/FilterListSharp';
import { GridHeaderCellProps } from '@progress/kendo-react-grid';

const FilterHeaderColumn: FunctionComponent<GridHeaderCellProps> = ({
   onClick,
}: GridHeaderCellProps) => {
   return (
      <span>
         <FilterListSharpIcon onClick={onClick} />
      </span>
   );
};

export default FilterHeaderColumn;
