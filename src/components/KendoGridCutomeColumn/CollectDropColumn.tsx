import React, {
   FunctionComponent,
   useMemo,
   useState,
   useCallback,
} from 'react';
import { useTranslation } from 'react-i18next';
import { useDispatch, useSelector } from 'react-redux';
import clsx from 'clsx';
import moment from 'moment';

import { GridCellProps } from '@progress/kendo-react-grid';
import { createStyles, makeStyles } from '@material-ui/core';

import {
   IDelShiftOrderSaga,
   IPassengerShifts,
   IPassengerDate,
   ISetShiftOrderSaga,
} from 'src/api/manualOrderApi/types';
import { stringToBoolean } from 'src/utilis/utilis';
import {
   onAddPassngerShifts,
   onDelPassngerShifts,
} from 'src/store/actions/PassengersShiftActionType';
import { IRootReducer } from 'src/store/reducers';
import { loginSelector } from 'src/store/selectores/loginSelectors';
import { selectedShiftSelector } from 'src/store/selectores/passengerShiftsSelectores';
import styled from 'styled-components';

import PickUpSubColumn from './PickUpSubColumn';

const Span = styled.span``;

const useStyles = makeStyles(() =>
   createStyles({
      root: {
         width: '100%',
         height: '100%',
      },
      empty: {
         width: '50%',
         height: '100%',
         cursor: 'pointer',
         display: 'inline-block',
      },
      drop: {
         width: '50%',
         height: '100%',
         backgroundColor: '#FAEDC2',
         cursor: 'pointer',
         display: 'inline-block',
      },
      collect: {
         width: '50%',
         height: '100%',
         backgroundColor: '#E8D2BD',
         cursor: 'pointer',
         display: 'inline-block',
      },
      deActiveDrop: {
         backgroundColor: '#D6D3D3',
         height: '100%',
         width: '50%',
         display: 'inline-block',
         // pointerEvents: 'none',
      },
      deActivePickUp: {
         height: '100%',
         width: '50%',
         backgroundColor: '#C1C1C1',
         display: 'inline-block',
         // pointerEvents: 'none',
      },
      deActive: {
         backgroundColor: '#F5F5F5',
      },
   }),
);

const CollectDropColumn: FunctionComponent<GridCellProps> = ({
   dataItem,
   field,
}: GridCellProps) => {
   const dispatch = useDispatch();
   const { t } = useTranslation();
   const classes = useStyles();
   const passenger: IPassengerShifts = dataItem;
   const [isHoverOnCollect, setHoverOnCollect] = useState<boolean>(false);
   const [isHoverOnDrop, setHoverOnDrop] = useState<boolean>(false);

   const fieldDate = moment(field, 'DD.MM.yyyy');
   const { selectedFcAccount, token } = useSelector((state: IRootReducer) =>
      loginSelector(state),
   );

   const selectedShift = useSelector((state: IRootReducer) =>
      selectedShiftSelector(state),
   );

   const onAddShiftDropPickup = (payload: ISetShiftOrderSaga) =>
      dispatch(onAddPassngerShifts(payload));

   const onDelShiftDropPickup = (payload: IDelShiftOrderSaga) =>
      dispatch(onDelPassngerShifts(payload));

   const onMouseCollect = useCallback(
      (isHover: React.SetStateAction<boolean>) => () => {
         setHoverOnCollect(isHover);
      },
      [],
   );

   const onMouseDrop = useCallback(
      (isHover: React.SetStateAction<boolean>) => () => {
         setHoverOnDrop(isHover);
      },
      [],
   );

   const relativeDataPassengerData: IPassengerDate | undefined = useMemo(() => {
      return passenger.dates.find(
         (x) => moment(x.relativeDate).format('DD.MM.yyyy') === field,
      );
   }, [passenger, field]);

   const isDropActionActive = useMemo(
      () =>
         selectedShift?.dateByDrop
            .filter(
               (x) => moment(x.relativeDate).format('DD.MM.yyyy') === field,
            )
            .some((x) => stringToBoolean(x.isActive)) || false,
      [selectedShift, field],
   );

   const isPickUpActionActive = useMemo(
      () =>
         selectedShift?.dateByPickUp
            .filter(
               (x) => moment(x.relativeDate).format('DD.MM.yyyy') === field,
            )
            .some((x) => stringToBoolean(x.isActive)) || false,
      [selectedShift, field],
   );

   const currentShiftDropOrder = useMemo(() => {
      return (
         relativeDataPassengerData?.dropOrders.filter(
            (x) => x.shiftTime === selectedShift?.dropTime,
         ) || []
      );
   }, [relativeDataPassengerData, selectedShift]);

   const currentShiftPickUpOrder = useMemo(() => {
      return (
         relativeDataPassengerData?.pickupOrders.filter(
            (x) => x.shiftTime === selectedShift?.pickupTime,
         ) || []
      );
   }, [relativeDataPassengerData, selectedShift]);

   const difrentShiftDropOrder = useMemo(() => {
      return (
         relativeDataPassengerData?.dropOrders.filter(
            (x) => x.shiftTime !== selectedShift?.dropTime,
         ) || []
      );
   }, [relativeDataPassengerData, selectedShift]);

   const difrentShiftPickUpOrder = useMemo(() => {
      return (
         relativeDataPassengerData?.pickupOrders.filter(
            (x) => x.shiftTime !== selectedShift?.pickupTime,
         ) || []
      );
   }, [relativeDataPassengerData, selectedShift]);

   const isDropExiest = currentShiftDropOrder.length > 0;
   const isPickupExiest = currentShiftPickUpOrder.length > 0;

   const dropClassName = clsx({
      [classes.drop]: isDropExiest && isDropActionActive,
      [classes.empty]: !isDropExiest && isDropActionActive,
      [classes.deActiveDrop]: !isDropActionActive && isDropExiest,
      [classes.deActive]: !isDropActionActive && !isDropExiest,
   });

   const pickUpClassName = clsx({
      [classes.collect]: isPickupExiest && isPickUpActionActive,
      [classes.empty]: !isPickupExiest && isPickUpActionActive,
      [classes.deActivePickUp]: !isPickUpActionActive && isPickupExiest,
      [classes.deActive]: !isPickUpActionActive && !isPickupExiest,
   });

   const onAddPickUp = (): void => {
      if (
         isPickUpActionActive &&
         selectedFcAccount &&
         token &&
         selectedShift &&
         !isPickupExiest
      )
         onAddShiftDropPickup({
            dbUrl: selectedFcAccount.dbUrl,
            proxyUrl: selectedFcAccount.proxyUrl,
            token,
            pass_id: +passenger.passId,
            pickupTime: selectedShift.pickupTime,
            relative_date: fieldDate.format('yyyy-MM-DD'),
            pickupPassengers: +passenger.passId,
            passenger,
         });
   };

   const onAddDrop = (): void => {
      if (
         isDropActionActive &&
         selectedFcAccount &&
         token &&
         selectedShift &&
         !isDropExiest
      )
         onAddShiftDropPickup({
            dbUrl: selectedFcAccount.dbUrl,
            proxyUrl: selectedFcAccount.proxyUrl,
            token,
            pass_id: +passenger.passId,
            dropTime: selectedShift.dropTime,
            relative_date: fieldDate.format('yyyy-MM-DD'),
            dropPassengers: +passenger.passId,
            passenger,
         });
   };

   const onDeletePickUp = (): void => {
      if (
         isPickUpActionActive &&
         selectedFcAccount &&
         token &&
         relativeDataPassengerData &&
         isPickupExiest
      )
         onDelShiftDropPickup({
            dbUrl: selectedFcAccount.dbUrl,
            proxyUrl: selectedFcAccount.proxyUrl,
            token,
            pass_id: +passenger.passId,
            orders: currentShiftPickUpOrder.map((x) => +x.orderCode),
            relative_date: fieldDate.format('yyyy-MM-DD'),
            passenger,
            isPickUp: true,
         });
   };

   const onDeleteDrop = (): void => {
      if (
         isDropActionActive &&
         selectedFcAccount &&
         token &&
         relativeDataPassengerData &&
         isDropExiest
      )
         onDelShiftDropPickup({
            dbUrl: selectedFcAccount.dbUrl,
            proxyUrl: selectedFcAccount.proxyUrl,
            token,
            pass_id: +passenger.passId,
            orders: currentShiftDropOrder.map((x) => +x.orderCode),
            relative_date: fieldDate.format('yyyy-MM-DD'),
            passenger,
            isPickUp: false,
         });
   };

   return (
      <td className={['collect-drop-column', ' '].join(' ')} height="100%">
         <Span
            style={{
               width: '50%',
               height: '100%',
               display: 'inline-block',
               borderLeft: '1px dotted #F8C0C8',
            }}
            className={pickUpClassName}
            onMouseEnter={onMouseCollect(true)}
            onMouseLeave={onMouseCollect(false)}
            onClick={isPickupExiest ? onDeletePickUp : onAddPickUp}
         >
            <PickUpSubColumn
               name={t('pickUp')}
               orders={difrentShiftPickUpOrder}
               isCurrentShiftOrders={isPickupExiest}
               isHover={isHoverOnCollect}
               isEnabled={isPickUpActionActive}
            />
         </Span>
         <Span
            className={dropClassName}
            style={{ width: '50%', height: '100%', display: 'inline-block' }}
            onMouseEnter={onMouseDrop(true)}
            onMouseLeave={onMouseDrop(false)}
            onClick={isDropExiest ? onDeleteDrop : onAddDrop}
         >
            <PickUpSubColumn
               name={t('drop')}
               orders={difrentShiftDropOrder}
               isCurrentShiftOrders={isDropExiest}
               isHover={isHoverOnDrop}
               isEnabled={isDropActionActive}
            />
         </Span>
      </td>
   );
};

export default CollectDropColumn;
