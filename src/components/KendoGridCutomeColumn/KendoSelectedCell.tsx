import React, { FunctionComponent } from 'react';
import { GridCellProps } from '@progress/kendo-react-grid';
import Checkbox from '@material-ui/core/Checkbox';

export interface IKendoSelectedCell extends GridCellProps {
   onCheckbox: (input: boolean, dataItem: any) => unknown;
   selectedField?: string;
}

const KendoSelectedCell: FunctionComponent<IKendoSelectedCell | any> = ({
   dataItem,
   field = '',
   selectedField = '',
   onCheckbox,
}: IKendoSelectedCell | any) => {
   const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      onCheckbox(event.target.checked, dataItem);
   };

   const value = dataItem[field];
   return (
      <td>
         <div>
            <Checkbox
               checked={dataItem[selectedField] || false}
               onChange={handleChange}
               inputProps={{ 'aria-label': 'בחר רשומה' }}
            />
            <span>{value}</span>
         </div>
      </td>
   );
};

export default KendoSelectedCell;
