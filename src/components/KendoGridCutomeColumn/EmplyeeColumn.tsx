import { GridCellProps } from '@progress/kendo-react-grid';
import React, { CSSProperties, FunctionComponent } from 'react';
import { IPassengerShifts } from 'src/api/manualOrderApi/types';

const EmplyeeColumn: FunctionComponent<GridCellProps> = ({
   dataItem,
}: GridCellProps) => {
   const passnger: IPassengerShifts = dataItem;

   const nameStyle: CSSProperties = { paddingRight: '1vw', height: '50%' };
   const style: CSSProperties = { paddingRight: '1vw' };

   return (
      <td height="100%" className="emplyee-column">
         <div style={nameStyle}>
            <b>{passnger.fullName}</b>
         </div>
         <div style={style}>
            <span style={{ width: '50%', textAlign: 'center' }}>
               {passnger.passId}
            </span>
            <span style={{ width: '50%', textAlign: 'center' }}>
               {'   '}
               {passnger.departmentName}
            </span>
         </div>
      </td>
   );
};

export default EmplyeeColumn;

// using grid
// import { GridCellProps } from '@progress/kendo-react-grid';
// import React, { FunctionComponent } from 'react';
// import Grid from '@material-ui/core/Grid';
// import { IPassenger } from 'src/api/manualOrderApi/types';

// const EmplyeeColumn: FunctionComponent<GridCellProps> = ({
//    dataItem,
// }: GridCellProps) => {
//    const passnger: IPassenger = dataItem;

//    return (
//       <td className="emplyee-column">
//          <Grid
//             container
//             spacing={1}
//             direction="column"
//             style={{ width: '100%' }}
//             justify="center"
//          >
//             <Grid container direction="row">
//                <Grid item xs={12}>
//                   <b>{passnger.fullName}</b>
//                </Grid>
//             </Grid>
//             <Grid container spacing={3} direction="row">
//                <Grid item xs={6}>
//                   {passnger.passId}
//                </Grid>
//                <Grid item xs={6}>
//                   {passnger.departmentName}
//                </Grid>
//             </Grid>
//          </Grid>
//       </td>
//    );
// };

// export default EmplyeeColumn;
