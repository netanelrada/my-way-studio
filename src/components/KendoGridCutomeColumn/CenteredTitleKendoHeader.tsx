import React, { FunctionComponent } from 'react';
import { GridHeaderCellProps } from '@progress/kendo-react-grid';
import { Box } from '@material-ui/core';

const CenteredTitleKendoHeader: FunctionComponent<GridHeaderCellProps> = ({
   onClick,
   children,
   title,
}: GridHeaderCellProps | any) => {
   return (
      <Box
         onClick={onClick}
         component="div"
         style={{ textAlign: 'center', cursor: 'pointer' }}
      >
         <span>{title}</span>
         {children}
      </Box>
   );
};

export default CenteredTitleKendoHeader;
