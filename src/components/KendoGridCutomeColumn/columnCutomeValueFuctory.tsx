import { GridCellProps } from '@progress/kendo-react-grid';
import React, { FunctionComponent } from 'react';

interface IProp {
   modifyValue: (dataItem: any) => string;
}
/* todo useCallback */
const useColumnCutomeValueFuctory = ({
   modifyValue,
}: IProp): FunctionComponent<GridCellProps> => {
   const ColumnCutomeValue = ({ dataItem }: GridCellProps) => {
      const modifyValueFunction = modifyValue;

      return <td>{modifyValueFunction(dataItem)}</td>;
   };

   return ColumnCutomeValue;
};

export default useColumnCutomeValueFuctory;
