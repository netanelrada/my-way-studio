import React, { FunctionComponent, lazy, Suspense } from 'react';
import { GridCellProps } from '@progress/kendo-react-grid';

const CollectDropColumn = lazy(() => import('./CollectDropColumn'));

const LazyCollectDropColumn: FunctionComponent<GridCellProps> = (
   gridCellProp: GridCellProps,
) => {
   return (
      <Suspense fallback={<td>...</td>}>
         <CollectDropColumn {...gridCellProp} />
      </Suspense>
   );
};

export default LazyCollectDropColumn;
