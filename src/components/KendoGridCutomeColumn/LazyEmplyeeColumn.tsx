import React, { FunctionComponent, lazy, Suspense } from 'react';
import { GridCellProps } from '@progress/kendo-react-grid';

const EmplyeeColumn = lazy(() => import('./EmplyeeColumn'));

const LazyEmplyeeColumn: FunctionComponent<GridCellProps> = (
   gridCellProp: GridCellProps,
) => {
   return (
      <Suspense fallback={<td>...</td>}>
         <EmplyeeColumn {...gridCellProp} />
      </Suspense>
   );
};

export default LazyEmplyeeColumn;
