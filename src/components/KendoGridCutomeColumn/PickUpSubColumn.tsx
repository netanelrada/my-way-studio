import React, { FunctionComponent, ReactChild } from 'react';
import { IPickupDropOrder } from 'src/api/manualOrderApi/types';
import { createStyles, makeStyles, Theme } from '@material-ui/core';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import { useTranslation } from 'react-i18next';
import styled from 'styled-components';
import { images } from 'src/assets/index';
import Tooltip from '../ToolTip/ToolTip';

const BusImage = styled.img<{ src: string }>`
   cursor: pointer;
`;

const Container = styled.span`
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100%;
   flex-direction: column;
`;

const BusIconContainer = styled.span`
   width: 100%;
   height: 100%;
   position: relative;
   text-align: right;
   flex-basis: 40%;
`;

const IconContiner = styled.span`
   flex-basis: 60%;
   align-items: center;
`;

const AddIconContiner = styled.span`
   flex-basis: 60%;
   display: flex;
   justify-content: center;
   align-items: flex-start;
`;

const useStyles = makeStyles((theme: Theme) =>
   createStyles({
      fill: {
         width: '100%',
         height: '100%',
         textAlign: 'center',
      },
      iconFontSize: {
         fontSize: '1.1em',
      },
      empty: {
         width: '100%',
         height: '100%',
         textAlign: 'center',
      },
      dottedBorder: {
         border: '1px dotted black',
         width: '100%',
         height: '100%',
         opacity: '0.5',
         textAlign: 'center',
      },
      transprantBorder: {
         opacity: '0.5',
         width: '100%',
         height: '100%',
         textAlign: 'center',
      },
      transprant: {},
      bus: {
         fontSize: '0.8rem',
         position: 'absolute',
         top: '1px',
         left: '1px',
      },
   }),
);

export interface IProp {
   name: string;
   orders: IPickupDropOrder[] | undefined;
   isCurrentShiftOrders: boolean | undefined;
   isHover: boolean;
   isEnabled: boolean;
}

const PickUpSubColumn: FunctionComponent<IProp> = ({
   name,
   orders,
   isCurrentShiftOrders,
   isHover,
   isEnabled,
}: IProp) => {
   const { t } = useTranslation();
   const classes = useStyles();

   const buildRowsByShift = (orderDetials: IPickupDropOrder): string => {
      return `${t('otherOrderExist')} ${orderDetials.shiftTime}`;
   };

   const getToolTip = (): ReactChild => {
      if (!orders) return '';
      return (
         <div style={{ whiteSpace: 'pre' }}>
            {orders.map((x) => buildRowsByShift(x)).join(' \n')}
         </div>
      );
   };

   const deleteElement = (): JSX.Element => {
      return (
         <Container
            className={[classes.transprantBorder, 'show-on-hover'].join(' ')}
         >
            <BusIconContainer
               className={orders && orders.length > 0 ? '' : classes.transprant}
            >
               {orders && orders.length > 0 && (
                  <Tooltip title={getToolTip()} arrow placement="top">
                     <BusImage
                        className={classes.bus}
                        src={images.busAlertIcon}
                     />
                  </Tooltip>
               )}
            </BusIconContainer>

            <IconContiner className={classes.fill}>
               <DeleteOutlineIcon />
            </IconContiner>
         </Container>
      );
   };

   const addElement = (): JSX.Element => {
      return (
         <Container
            className={[classes.dottedBorder, 'show-on-hover'].join(' ')}
         >
            <BusIconContainer
               className={orders && orders.length > 0 ? '' : classes.transprant}
            >
               {orders && orders.length > 0 && (
                  <Tooltip title={getToolTip()} arrow placement="top">
                     <BusImage
                        className={classes.bus}
                        src={images.busAlertIcon}
                     />
                  </Tooltip>
               )}
            </BusIconContainer>

            <AddIconContiner className={classes.fill}>
               <AddOutlinedIcon className={classes.iconFontSize} name={name}>
                  {name}
               </AddOutlinedIcon>
               <span className={classes.iconFontSize}>{name}</span>
            </AddIconContiner>
         </Container>
      );
   };

   const emptyElement = (): JSX.Element => {
      return (
         <Container className={classes.fill}>
            <BusIconContainer
               className={orders && orders.length > 0 ? '' : classes.transprant}
            >
               {orders && orders.length > 0 && (
                  <Tooltip title={getToolTip()} arrow placement="top">
                     <BusImage
                        className={classes.bus}
                        src={images.busAlertIcon}
                     />
                  </Tooltip>
               )}
            </BusIconContainer>

            <IconContiner className={classes.fill}>
               {isCurrentShiftOrders && (
                  <span className={classes.iconFontSize}>{name}</span>
               )}
            </IconContiner>
         </Container>
      );
   };

   if (isHover && isEnabled && isCurrentShiftOrders) return deleteElement();

   if (isHover && isEnabled && !isCurrentShiftOrders) return addElement();

   return emptyElement();
};

export default PickUpSubColumn;

// using flex
// import React, { FunctionComponent, ReactChild } from 'react';
// import { IPickupDropOrder } from 'src/api/manualOrderApi/types';
// import { Box, createStyles, makeStyles, Theme } from '@material-ui/core';
// import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
// import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
// import DirectionsBusOutlinedIcon from '@material-ui/icons/DirectionsBusOutlined';
// import Tooltip from '@material-ui/core/Tooltip';
// import { useSelector } from 'react-redux';
// import { IRootReducer } from 'src/store/reducers';
// import { shiftsSelector } from 'src/store/selectores/passengerSelectores';
// import { useTranslation } from 'react-i18next';

// const useStyles = makeStyles((theme: Theme) =>
//    createStyles({
//       dottedBorder: {
//          border: '1px dotted black',
//          opacity: '0.2',
//       },
//       transprantBorder: {
//          opacity: '0.2',
//       },
//       transprant: {
//          backgroundColor: 'transparent',
//          color: 'transparent',
//       },
//    }),
// );

// export interface IProp {
//    name: string;
//    onClick: () => void;
//    orders: IPickupDropOrder[] | undefined;
//    isCurrentShiftOrders: boolean | undefined;
//    isHover: boolean;
//    isEnabled: boolean;
// }

// const PickUpSubColumn: FunctionComponent<IProp> = ({
//    name,
//    onClick,
//    orders,
//    isCurrentShiftOrders,
//    isHover,
//    isEnabled,
// }: IProp) => {
//    const { t } = useTranslation();
//    const classes = useStyles();

//    const shifts = useSelector((state: IRootReducer) => shiftsSelector(state));

//    const buildRowsByShift = (orderDetials: IPickupDropOrder): string => {
//       return `${t('otherOrderExist')} ${
//          shifts.find((x) => x.shiftId === +orderDetials.shiftId)?.shiftName
//       }`;
//    };

//    const getToolTip = (): ReactChild => {
//       if (!orders) return '';
//       return <pre>{orders.map((x) => buildRowsByShift(x)).join(' \n')}</pre>;
//    };

//    const deleteElement = (): JSX.Element => {
//       return (
//          <Box
//             className={[classes.transprantBorder, 'show-on-hover'].join(' ')}
//             height="100%"
//             width="100%"
//             flexDirection="column"
//             display="flex"
//             onClick={onClick}
//          >
//             <Box
//                className={orders && orders.length > 0 ? '' : classes.transprant}
//                display="flex"
//                flexDirection="row"
//                justifyContent="start"
//             >
//                <Tooltip title={getToolTip()}>
//                   <DirectionsBusOutlinedIcon style={{ fontSize: '0.8rem' }} />
//                </Tooltip>
//             </Box>

//             <Box
//                height="100%"
//                width="100%"
//                flexDirection="row"
//                display="flex"
//                justifyContent="center"
//                alignItems="center"
//             >
//                <DeleteOutlineIcon />
//             </Box>
//          </Box>
//       );
//    };

//    const addElement = (): JSX.Element => {
//       return (
//          <Box
//             className={[classes.dottedBorder, 'show-on-hover'].join(' ')}
//             height="100%"
//             width="100%"
//             flexDirection="column"
//             display="flex"
//             onClick={onClick}
//          >
//             <Box
//                className={orders && orders.length > 0 ? '' : classes.transprant}
//                display="flex"
//                flexDirection="row"
//                justifyContent="start"
//             >
//                <Tooltip title={getToolTip()}>
//                   <DirectionsBusOutlinedIcon style={{ fontSize: '0.8rem' }} />
//                </Tooltip>
//             </Box>

//             <Box
//                height="100%"
//                width="100%"
//                flexDirection="row"
//                display="flex"
//                justifyContent="center"
//                alignItems="center"
//             >
//                <AddOutlinedIcon name={name}>{name}</AddOutlinedIcon>
//                {name}
//             </Box>
//          </Box>
//       );
//    };

//    const emptyElement = (): JSX.Element => {
//       return (
//          <Box height="100%" width="100%" flexDirection="column" display="flex">
//             <Box
//                className={orders && orders.length > 0 ? '' : classes.transprant}
//                display="flex"
//                flexDirection="row"
//                justifyContent="start"
//             >
//                <Tooltip title={getToolTip()}>
//                   <DirectionsBusOutlinedIcon style={{ fontSize: '0.8rem' }} />
//                </Tooltip>
//             </Box>

//             <Box
//                height="100%"
//                width="100%"
//                flexDirection="row"
//                display="flex"
//                justifyContent="center"
//                alignItems="center"
//             >
//                <AddOutlinedIcon className={classes.transprant} name={name} />
//                {!isEnabled && isCurrentShiftOrders && name}
//             </Box>
//          </Box>
//       );
//    };

//    if (isHover && isEnabled && isCurrentShiftOrders) return deleteElement();

//    if (isHover && isEnabled && !isCurrentShiftOrders) return addElement();

//    return emptyElement();
// };

// export default PickUpSubColumn;
