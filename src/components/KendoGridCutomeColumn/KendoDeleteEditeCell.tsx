import React, {
   CSSProperties,
   FunctionComponent,
   useCallback,
   useState,
} from 'react';
import { GridCellProps } from '@progress/kendo-react-grid';
import RemoveIcon from '@material-ui/icons/Remove';
import EditIcon from '@material-ui/icons/Edit';
import IconButton from '@material-ui/core/IconButton';
import { Box } from '@material-ui/core';

export interface IKendoDeleteEditeCell extends GridCellProps {
   onEdit: (dataItem: any) => unknown;
   onDelete: (dataItem: any) => unknown;
}

const KendoDeleteEditeCell: FunctionComponent<IKendoDeleteEditeCell> = ({
   dataItem,
   field = '',
   onEdit,
   onDelete,
}: IKendoDeleteEditeCell) => {
   const iconButtonStyle: CSSProperties = {
      backgroundColor: '#EDBD41',
      fontSize: '14px',
      marginLeft: '5px',
   };

   // eslint-disable-next-line no-console
   if (!onEdit) console.warn('onEdit was not supplied');
   // eslint-disable-next-line no-console
   if (!onDelete) console.warn('onDelete was not supplied');

   const [isHover, setHover] = useState<boolean>(false);

   const onMouse = useCallback(
      (hover: React.SetStateAction<boolean>) => () => {
         setHover(hover);
      },
      [],
   );

   const value = dataItem[field];
   return (
      <td onMouseEnter={onMouse(true)} onMouseLeave={onMouse(false)}>
         <Box
            display="flex"
            justifyContent="space-between"
            alignItems="baseline"
         >
            <span
               style={{
                  flexBasis: '90%',
                  textOverflow: 'ellipsis',
                  overflow: 'hidden',
               }}
            >
               {value || ' '}
            </span>

            {isHover && (
               <span style={{ flexBasis: '10%' }}>
                  <IconButton
                     onClick={() =>
                        typeof onEdit === 'function' && onEdit(dataItem)
                     }
                     style={iconButtonStyle}
                     aria-label="עריכה"
                     size="small"
                  >
                     <EditIcon fontSize="small" />
                  </IconButton>

                  <IconButton
                     onClick={() =>
                        typeof onDelete === 'function' && onDelete(dataItem)
                     }
                     style={iconButtonStyle}
                     aria-label="מחיקה"
                     size="small"
                  >
                     <RemoveIcon fontSize="small" />
                  </IconButton>
               </span>
            )}
         </Box>
      </td>
   );
};

export default KendoDeleteEditeCell;
