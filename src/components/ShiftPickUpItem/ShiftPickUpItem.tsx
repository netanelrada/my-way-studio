import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

export interface IProps {
   pickUpTime?: string;
}

const ShiftPickUpItem: FunctionComponent<IProps> = ({ pickUpTime }: IProps) => {
   const { t } = useTranslation();

   return (
      <span>
         {t('pickUp')}:<b style={{ paddingRight: '4px' }}>{pickUpTime}</b>
      </span>
   );
};

export default ShiftPickUpItem;
