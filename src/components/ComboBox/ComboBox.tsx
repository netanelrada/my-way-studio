import React, { FC } from 'react';
import Autocomplete, {
   AutocompleteProps,
   AutocompleteChangeReason,
} from '@material-ui/lab/Autocomplete';

export type IAutocompleteChangeReason = AutocompleteChangeReason;
export type IComboBox = AutocompleteProps<any, boolean, boolean, boolean>;

// const departmentComboBoxProps: IComboBox = {
//    options: departmentOptions,
//    getOptionLabel: (option: MenuItemProps) => option.name,
//    renderInput: (params) => (
//       <TextField
//          {...params}
//          label={t('department')}
//          size="small"
//          variant="outlined"
//       />
//    ),
//    freeSolo: true,
//    inputValue: modefiedPassenger.departmentName,
//    value: modefiedPassenger.departmentCode,
//    onInputChange: (event, newInputValue) => {
//       setModefiedPassenger({
//          ...modefiedPassenger,
//          departmentName: newInputValue,
//       });
//    },
//    onChange: (
//       event: React.ChangeEvent<{}>,
//       newValue: string | undefined,
//    ) => {
//       setModefiedPassenger({
//          ...modefiedPassenger,
//          departmentCode: newValue,
//       });
//    },
// };

const ComboBox: FC<IComboBox> = (prop: IComboBox) => {
   return <Autocomplete {...prop} />;
};

export default ComboBox;
