import React, {
   FunctionComponent,
   useState,
   useRef,
   KeyboardEvent,
   useEffect,
} from 'react';
import 'date-fns';
import moment from 'moment';
import { v4 as GuidService } from 'uuid';

import DateFnsUtils from '@date-io/date-fns';
import heLocal from 'date-fns/locale/he';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import CalendarTodayIcon from '@material-ui/icons/Today';

import {
   MuiPickersUtilsProvider,
   KeyboardDatePicker,
   KeyboardDatePickerProps,
} from '@material-ui/pickers';
import { useTranslation } from 'react-i18next';
import { TextField, IconButton, Box, TextFieldProps } from '@material-ui/core';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IDatePickerProps extends KeyboardDatePickerProps {}

const DatePickerWithPrevNext: FunctionComponent<IDatePickerProps> = ({
   okLabel,
   cancelLabel,
   todayLabel,
   label,
   onChange,
   value,
   ...datePickerProps
}: IDatePickerProps): JSX.Element => {
   const { t } = useTranslation();

   const okLabaleText = okLabel || t('ok');
   const cancelLabelText = cancelLabel || t('cancel');
   const labelText = label || t('date');
   const labelTodayText = todayLabel || t('today');

   const [isOpen, setIsOpen] = useState(false);
   const input = useRef<any>(null);

   const setIsOpenPicker = () => {
      setIsOpen(true);
   };

   const setNextDay = () => {
      const nextDate =
         moment(value).add('day', 1).isValid() &&
         moment(value).add('day', 1).toDate();
      if (nextDate) onChange(nextDate, nextDate.toDateString());
   };

   const setPreviousDay = () => {
      const nextDate =
         moment(value).add('day', -1).isValid() &&
         moment(value).add('day', -1).toDate();
      if (nextDate) onChange(nextDate, nextDate.toDateString());
   };

   useEffect(() => {
      if (input.current) input.current?.focus();
   }, [value, input]);

   const onKeyDown = ({ keyCode }: KeyboardEvent<HTMLDivElement>) => {
      const keyUp = 38;
      const keyDown = 40;

      if (keyCode === keyUp) setNextDay();

      if (keyCode === keyDown) setPreviousDay();
   };

   // Custom TextField from KeyboardDatePicker loses focus when typing in date
   // using useEffect to focus back to div
   const CustomizedTextField = (props: TextFieldProps) => {
      return (
         <TextField
            onKeyDown={onKeyDown}
            {...props}
            InputProps={{
               inputRef: input,
               endAdornment: (
                  <Box display="flex" flexWrap="no-wrap">
                     <IconButton onClick={setIsOpenPicker} size="small">
                        <CalendarTodayIcon />
                     </IconButton>

                     <Box
                        display="flex"
                        flexWrap="no-wrap"
                        flexDirection="column"
                     >
                        <IconButton
                           size="small"
                           onClick={setNextDay}
                           style={{ padding: '0px' }}
                        >
                           <KeyboardArrowUpIcon fontSize="small" />
                        </IconButton>
                        <IconButton
                           size="small"
                           onClick={setPreviousDay}
                           style={{ padding: '0px' }}
                        >
                           <KeyboardArrowDownIcon fontSize="small" />
                        </IconButton>
                     </Box>
                  </Box>
               ),
            }}
         />
      );
   };

   return (
      <div>
         <MuiPickersUtilsProvider utils={DateFnsUtils} locale={heLocal}>
            <KeyboardDatePicker
               className="DatePicker"
               value={value}
               onChange={onChange}
               {...datePickerProps}
               onClose={() => {
                  setIsOpen(false);
               }}
               autoOk
               open={isOpen}
               label={labelText}
               cancelLabel={cancelLabelText}
               todayLabel={labelTodayText}
               okLabel={okLabaleText}
               TextFieldComponent={CustomizedTextField}
            />
         </MuiPickersUtilsProvider>
      </div>
   );
};

export default DatePickerWithPrevNext;
