import React, { FunctionComponent, KeyboardEvent } from 'react';
import 'date-fns';

import DateFnsUtils from '@date-io/date-fns';
import heLocal from 'date-fns/locale/he';

import {
   MuiPickersUtilsProvider,
   KeyboardDatePicker,
   KeyboardDatePickerProps,
} from '@material-ui/pickers';
import { useTranslation } from 'react-i18next';
import moment from 'moment';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
export interface IDatePickerProps extends KeyboardDatePickerProps {}

const DatePicker: FunctionComponent<IDatePickerProps> = ({
   okLabel,
   cancelLabel,
   todayLabel,
   label,
   onChange,
   value,
   ...datePickerProps
}: IDatePickerProps): JSX.Element => {
   const { t } = useTranslation();

   const okLabaleText = okLabel || t('ok');
   const cancelLabelText = cancelLabel || t('cancel');
   const labelText = label || t('date');
   const labelTodayText = todayLabel || t('today');

   const setNextDay = () => {
      const nextDate =
         moment(value).add('day', 1).isValid() &&
         moment(value).add('day', 1).toDate();
      if (nextDate) onChange(nextDate, nextDate.toDateString());
   };

   const setPreviousDay = () => {
      const nextDate =
         moment(value).add('day', -1).isValid() &&
         moment(value).add('day', -1).toDate();
      if (nextDate) onChange(nextDate, nextDate.toDateString());
   };

   const onKeyDown = ({
      keyCode,
      nativeEvent,
      preventDefault, // not working
      stopPropagation, // not working
   }: KeyboardEvent<HTMLDivElement>) => {
      const keyUp = 38;
      const keyDown = 40;

      if (keyCode === keyUp) {
         setNextDay();
      }

      if (keyCode === keyDown) {
         setPreviousDay();
      }
   };

   return (
      <MuiPickersUtilsProvider utils={DateFnsUtils} locale={heLocal}>
         <KeyboardDatePicker
            className="DatePicker"
            onKeyDown={onKeyDown}
            onChange={onChange}
            value={value}
            label={labelText}
            cancelLabel={cancelLabelText}
            todayLabel={labelTodayText}
            okLabel={okLabaleText}
            {...datePickerProps}
         />
      </MuiPickersUtilsProvider>
   );
};

export default DatePicker;
