import React, { FunctionComponent } from 'react';
import Select, { SelectProps } from '@material-ui/core/Select';

import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl, { FormControlProps } from '@material-ui/core/FormControl';

import { v4 as GuidService } from 'uuid';

export interface MenuItemProps {
   name: string;
   value: string | number | undefined;
   child?: JSX.Element;
}

export interface IProps extends SelectProps {
   labalName: string;
   formControlProp: FormControlProps;
   menueItem: Array<MenuItemProps>;
}

const DropDown: FunctionComponent<IProps> = ({
   labalName,
   formControlProp,
   menueItem,
   ...selectedElemnetProp
}: IProps) => {
   const guid = GuidService();

   return (
      <FormControl {...formControlProp}>
         <InputLabel id={guid}>{labalName}</InputLabel>
         <Select
            defaultValue=""
            {...selectedElemnetProp}
            labelId={guid}
            label={labalName}
         >
            {menueItem.map((d: MenuItemProps) => (
               <MenuItem key={d.value} value={d.value}>
                  {d.child || d.name}
               </MenuItem>
            ))}
         </Select>
      </FormControl>
   );
};

export default DropDown;
