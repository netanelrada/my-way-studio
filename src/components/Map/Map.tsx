import React, { FunctionComponent, ReactNode } from 'react';
import GoogleMap, { Props, BootstrapURLKeys } from 'google-map-react';

export interface IMap extends Props {
   children?: ReactNode;
}

const Map: FunctionComponent<IMap> = ({
   bootstrapURLKeys,
   children,
   ...prop
}: IMap) => {
   const googleUrl: BootstrapURLKeys = {
      key: 'AIzaSyB4txmpozixb-JqskL4F6iemZMO6Zf2KbQ',
      language: 'iw',
      region: 'IL',
   };

   return (
      <GoogleMap {...prop} bootstrapURLKeys={googleUrl}>
         {children}
      </GoogleMap>
   );
};

export default Map;
