import React, { FC, ReactNode } from 'react';
import { useTranslation } from 'react-i18next';

import { Button } from '@material-ui/core';
import Dialog from 'src/components/Dialog/Dialog';

interface IConfirmationDialog {
   onCloseDialog: () => void;
   onConfirmaDialog: () => void;
   isDialogOpen: boolean;
   DialogContent: ReactNode;
   DialogTitle: ReactNode;
}

const ConfirmationDialog: FC<IConfirmationDialog> = ({
   onCloseDialog,
   onConfirmaDialog,
   isDialogOpen,
   DialogContent,
   DialogTitle,
}: IConfirmationDialog) => {
   const { t } = useTranslation();

   const DialogAction = () => {
      return (
         <>
            <Button onClick={onConfirmaDialog} color="primary">
               {t('ok')}
            </Button>
            <Button onClick={onCloseDialog} color="primary">
               {t('no')}
            </Button>
         </>
      );
   };

   return (
      <Dialog
         DialogActionsChildren={DialogAction()}
         DialogContentChildren={DialogContent}
         DialogTitleChildren={DialogTitle}
         open={isDialogOpen}
         onClose={onCloseDialog}
      />
   );
};

export default ConfirmationDialog;
