import React, { FC } from 'react';
import TextField, { TextFieldProps } from '@material-ui/core/TextField';
import { Grid } from '@material-ui/core';

export interface IPhoneNumber {
   supplierPhoneNumber?: string;
   phoneNumber?: string;
   size?: 'small' | 'medium';
   labal: React.ReactNode | string;
   isRequired: boolean;
   onChangePhoneNumber: (item: string) => void;
   onChangeSuplliernumber: (item: string) => void;
}

const PhoneNumber: FC<IPhoneNumber> = ({
   supplierPhoneNumber = '',
   phoneNumber = '',
   size,
   labal,
   isRequired,
   onChangePhoneNumber,
   onChangeSuplliernumber,
}: IPhoneNumber) => {
   const mubileSupplierProp: TextFieldProps = {
      value: supplierPhoneNumber,
      size: size || 'small',
      style: { width: '100%' },
      inputProps: { maxLength: 3 },
      required: isRequired,
      variant: 'outlined',
      onChange: ({ target }: React.ChangeEvent<HTMLInputElement>) => {
         onChangeSuplliernumber(target.value);
      },
   };

   const mubileNumberProp: TextFieldProps = {
      value: phoneNumber || '',
      size: size || 'small',
      inputProps: { maxLength: 7 },
      style: { width: '100%' },
      required: isRequired,
      variant: 'outlined',
      onChange: ({ target }: React.ChangeEvent<HTMLInputElement>) => {
         onChangePhoneNumber(target.value);
      },
   };

   return (
      <Grid
         container
         spacing={1}
         justifyContent="flex-start"
         alignItems="center"
      >
         <Grid item xs={12}>
            <div style={{ textAlign: 'right' }}>{labal}</div>
         </Grid>
         <Grid item xs={8}>
            <TextField {...mubileNumberProp} />
         </Grid>
         <Grid item xs={1}>
            <span> - </span>
         </Grid>

         <Grid item xs={3}>
            <TextField {...mubileSupplierProp} />
         </Grid>
      </Grid>
   );
};

export default PhoneNumber;
