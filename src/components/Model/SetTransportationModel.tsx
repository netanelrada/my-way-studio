import React, { FunctionComponent, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { makeStyles, Theme, createStyles } from '@material-ui/core/styles';
import { IShifts } from 'src/api/manualOrderApi/types';
import {
   Box,
   Button,
   Fade,
   Divider,
   Backdrop,
   Modal,
   Grid,
} from '@material-ui/core';

import WeekDataPicker, {
   IWeekDatePicker,
} from 'src/components/WeekDatePicker/WeekDatePicker';
import { MaterialUiPickersDate } from '@material-ui/pickers/typings/date';
import moment from 'moment';
import { getDatesBetween, stringToBoolean } from 'src/utilis/utilis';
import { DayManualOrder } from 'src/screens/ManualOrder/manualOrderType';
import DailyOrder from './DailyOrder';

const useStyles = makeStyles((theme: Theme) =>
   createStyles({
      modal: {
         display: 'flex',
         alignItems: 'center',
         justifyContent: 'center',
      },
      paper: {
         backgroundColor: theme.palette.background.paper,
         border: '2px solid #000',
         boxShadow: theme.shadows[5],
         padding: theme.spacing(2),
      },
   }),
);

export interface IProps {
   selectedShift: IShifts | undefined;
   onSubmit: (result: Array<DayManualOrder>) => void;
   startDate: Date;
   endDate: Date;
}

const SetTransportationModel: FunctionComponent<IProps> = ({
   selectedShift,
   onSubmit,
   startDate,
   endDate,
}: IProps) => {
   const { t } = useTranslation();

   const classes = useStyles();
   const [orderByDay, setOrderByDay] = React.useState<Array<DayManualOrder>>(
      [],
   );
   const [isModelOpen, setIsModelOpen] = React.useState(false);

   const handleClose = () => {
      setIsModelOpen(false);
   };

   const onApprove = () => {
      onSubmit(orderByDay);
      handleClose();
   };

   const weekPickerProp: IWeekDatePicker = {
      prop: {
         inputVariant: 'outlined',
         disabled: true,
         style: { width: 270 },
         invalidDateMessage: 'תאריך תחילת שבוע לא תקין',
         size: 'small',
         format: 'dd.MM.yyyy',
         okLabel: t('ok'),
         cancelLabel: t('cancel'),
         labelFunc: (
            date: MaterialUiPickersDate,
            invalidLabel: string,
         ): string => {
            const dateClone = moment(date).clone();

            return dateClone && dateClone.isValid()
               ? `${dateClone
                    .startOf('week')
                    .format('DD.MM.yyyy')} - ${dateClone
                    .endOf('week')
                    .format('DD.MM.yyyy')}`
               : invalidLabel;
         },
         onChange: (date: MaterialUiPickersDate) => {},

         value: startDate,
      },
   };

   useEffect(() => {
      const result: Array<DayManualOrder> = [];
      getDatesBetween(startDate, endDate || startDate).forEach((date) => {
         const dayName: string | any = moment(date)
            .format('dddd')
            .toLowerCase();

         const isEnabled = stringToBoolean(
            selectedShift?.dates.find(
               (x) =>
                  moment(date).format('DD.MM.yyyy') ===
                  moment(x.relativeDate).format('DD.MM.yyyy'),
            )?.isActive,
         );

         result.push({
            name: `${t(dayName)}, ${moment(date).format('DD.MM.yyyy')}`,
            date,
            isActive: isEnabled && true,
            isEnabled,
            orderType: undefined,
         });
      });
      setOrderByDay(result);
   }, [t, startDate, endDate, selectedShift]);

   return (
      <div>
         <Button
            size="large"
            variant="outlined"
            onClick={() => {
               setIsModelOpen(true);
            }}
         >
            {t('setTransportation')}
         </Button>
         <Modal
            aria-labelledby={t('setEmplyeesTransportation')}
            aria-describedby={t('setEmplyeesTransportation')}
            className={classes.modal}
            open={isModelOpen}
            onClose={handleClose}
            closeAfterTransition
            BackdropComponent={Backdrop}
            BackdropProps={{
               timeout: 500,
            }}
         >
            <Fade in={isModelOpen}>
               <Box className={classes.paper} width="30vw">
                  <Grid
                     container
                     justify="space-between"
                     alignItems="center"
                     spacing={3}
                  >
                     <Grid item xs={3}>
                        <span> {t('setEmplyeesTransportation')}</span>
                     </Grid>
                     <Grid item xs={9}>
                        <WeekDataPicker {...weekPickerProp} />
                     </Grid>

                     <Grid item xs={10}>
                        <Divider />
                     </Grid>

                     {orderByDay.map((x) => (
                        <DailyOrder
                           key={x.date.toDateString()}
                           order={x}
                           updateFunction={setOrderByDay}
                        />
                     ))}

                     <Grid item xs={10}>
                        <Divider />
                     </Grid>

                     <Grid item xs={10}>
                        <Grid
                           container
                           justify="center"
                           alignContent="flex-start"
                           spacing={3}
                        >
                           <Grid item>
                              <Button
                                 variant="contained"
                                 color="primary"
                                 type="submit"
                                 onClick={onApprove}
                              >
                                 {t('ok')}
                              </Button>
                           </Grid>
                           <Grid item>
                              <Button
                                 variant="contained"
                                 color="default"
                                 type="submit"
                                 onClick={handleClose}
                              >
                                 {t('cancel')}
                              </Button>
                           </Grid>
                        </Grid>
                     </Grid>
                  </Grid>
               </Box>
            </Fade>
         </Modal>
      </div>
   );
};

export default SetTransportationModel;
