import React, { FunctionComponent, useMemo } from 'react';
import { useTranslation } from 'react-i18next';

import { Grid } from '@material-ui/core';
import Checkbox from '@material-ui/core/Checkbox';

import DropDown, {
   IProps as DropDwonProp,
   MenuItemProps,
} from 'src/components/DropDown/DropDown';

import {
   DayManualOrder,
   OrderType,
} from 'src/screens/ManualOrder/manualOrderType';

export interface IProp {
   order: DayManualOrder;
   updateFunction: React.Dispatch<React.SetStateAction<DayManualOrder[]>>;
}

const DailyOrder: FunctionComponent<IProp> = ({
   order,
   updateFunction,
}: IProp) => {
   const { t } = useTranslation();

   const dropPickupMenuItem: Array<MenuItemProps> = useMemo(
      () => [
         { name: t('pickUp'), value: OrderType.PickUp },
         { name: t('drop'), value: OrderType.Drop },
         { name: t('selectOrderOption'), value: '' },
      ],
      [t],
   );

   const handelUpdate = (value: any, key: keyof DayManualOrder) => {
      updateFunction((prev) =>
         prev.map((x) => {
            if (x.date === order.date)
               return Object.assign(x, { [key]: value });
            return x;
         }),
      );
   };

   const onIsActiveChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      handelUpdate(event.target.checked, 'isActive');
   };

   const clinetFilterProp: DropDwonProp = {
      formControlProp: {
         variant: 'outlined',
         style: { width: 208, marginRight: '15px' },
         size: 'small',
      },
      autoWidth: false,
      multiple: false,
      menueItem: dropPickupMenuItem,
      labalName: '',
      label: '',

      native: false,
      value: order.orderType || '',
      onChange: (
         event: React.ChangeEvent<{
            name?: string | undefined;
            value: number | string | any;
         }>,
      ) => {
         const { value } = event.target;

         handelUpdate(value, 'orderType');
      },
   };

   return (
      <Grid item xs={10} className={order.isEnabled ? '' : 'de-activ'}>
         <Grid
            container
            spacing={1}
            justify="space-between"
            alignItems="center"
         >
            <Grid item>
               <Checkbox
                  color="primary"
                  onChange={onIsActiveChange}
                  value={order.isActive}
                  inputProps={{ 'aria-label': 'is active' }}
               />
            </Grid>
            <Grid item>
               <span>{order.name} </span>
            </Grid>
            <Grid item>
               <DropDown {...clinetFilterProp} />
            </Grid>
         </Grid>
      </Grid>
   );
};

export default DailyOrder;
