/* eslint-disable react/prop-types */
import React, { FC, ChangeEvent } from 'react';
import { Grid, TextField, TextFieldProps } from '@material-ui/core';

export type AddressTextFieldProps = TextFieldProps & {
   onChange?: undefined;
};

export interface IAddressInput {
   onChange: (
      changeInput: 'city' | 'street' | 'houseNum',
      value: string | undefined,
   ) => void;
   cityProp: AddressTextFieldProps;
   streetProp: AddressTextFieldProps;
   housProp: AddressTextFieldProps;
}

const AddressInput: FC<IAddressInput> = ({
   onChange,
   cityProp,
   streetProp,
   housProp,
}) => {
   const handleChange =
      (prop: 'city' | 'street' | 'houseNum') =>
      (event: ChangeEvent<HTMLInputElement>) => {
         const { value } = event.target;

         onChange(prop, value);
      };

   return (
      <Grid container justifyContent="space-between">
         <Grid xs={4}>
            <TextField {...cityProp} onChange={handleChange('city')} />
         </Grid>
         <Grid xs={4}>
            <TextField {...streetProp} onChange={handleChange('street')} />
         </Grid>
         <Grid xs={3}>
            <TextField
               {...housProp}
               type="number"
               onChange={handleChange('houseNum')}
            />
         </Grid>
      </Grid>
   );
};

export default AddressInput;
