import React, { useState, useEffect, useMemo, useContext } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useTranslation } from 'react-i18next';
import { IItem, LineStatus } from 'src/types/line';
import FilterContext, { IFilter } from '../context';
import { IColumn } from '../types';

export const statusLineFilter = (rows: any, id: any, filterValue: any): any => {
   return rows.filter((row: any) => {
      const lineStatus = row.values[id];
      return lineStatus === filterValue.value;
   });
};

interface Props {
   column: IColumn;
}

const StatusLineFilter = ({ column: { setFilter, filterValue } }: Props) => {
   const { t } = useTranslation();
   const lineStatus = useMemo<Array<IItem>>(
      () => [
         { value: 0, name: t('withoutStatus') },
         { value: 3, name: t('ride') },
         { value: 4, name: t('ended') },
      ],
      [t],
   );
   const [filters, setFilters] = useContext(FilterContext);
   const handleChange = (
      event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>,
   ): void => {
      const { name, value } = event.target;
      setFilters((preState: IFilter) => ({
         ...preState,
         statusLine: { name, value },
      }));
      setFilter({ name, value });
   };

   const { statusLine } = filters;
   return (
      <FormControl variant="outlined" size="small" style={{ width: 176 }}>
         <InputLabel id="demo-simple-select-outlined-label">
            {t('lineStatus')}
         </InputLabel>
         <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            label={t('lineStatus')}
            value={statusLine?.value}
            onChange={handleChange}
         >
            {lineStatus.map((status: IItem) => (
               <MenuItem key={status.value} value={status.value}>
                  {status.name}
               </MenuItem>
            ))}
         </Select>
      </FormControl>
   );
};

export default StatusLineFilter;
