import { createContext } from 'react';
import { IItem } from 'src/types/line';

export interface IFilter {
   freeSearch: string;
   client: IItem;
   department: IItem;
   passengers: string;
   statusLine: IItem;
}

const emptyFun = () => {};

export const filterInitialState = {
   freeSearch: '',
   client: { value: -1, name: '' },
   department: { value: -1, name: '' },
   passengers: '',
   statusLine: { value: -1, name: '' },
};

export default createContext<[IFilter, (newState: any) => any]>([
   filterInitialState,
   emptyFun,
]);
