import React, { useContext, ChangeEvent } from 'react';

import { useTranslation } from 'react-i18next';
import { Column, HeaderGroup } from 'react-table';
import { IPassenger } from 'src/types/line';
import FilterContext, { IFilter } from '../context';
import styles from './PassengerFilter.style';

export const passengerFilter = (rows: any, id: any, filterValue: any): any => {
   console.log('rows', rows);
   return rows.filter((row: any) => {
      const passengers = row.values[id];
      return passengers.some((p: IPassenger) =>
         p.fullName.includes(filterValue),
      );
   });
};

interface Props {
   column: { filterValue: any; setFilter: any; preFilteredRows: any; id: any };
}

const PassengerFilter = ({ column: { filterValue, setFilter } }: Props) => {
   const { t } = useTranslation();
   const [filters, setFilters] = useContext(FilterContext);

   const onChange = (e: ChangeEvent<HTMLInputElement>) => {
      // setValue(text.target.value);
      setFilters((preState: IFilter) => ({
         ...preState,
         passengers: e.target.value,
      }));
      setFilter(e.target.value);
   };
   const { passengers } = filters;

   return (
      <styles.TextField
         value={passengers}
         onChange={onChange}
         id="outlined-basic"
         label={t('passenger')}
         variant="outlined"
         size="small"
      />
   );
};

export default PassengerFilter;
