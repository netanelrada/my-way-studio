import styled from 'styled-components';
import MaterialTextField from '@material-ui/core/TextField';

const TextField = styled(MaterialTextField)`
   max-width: 11em;
`;

export default { TextField };
