import styled from 'styled-components';
import MaterialOutlinedInput from '@material-ui/core/OutlinedInput';
import MaterialTextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const TableStyle = styled.div`
   overflow: auto;
   width: 65.75em;
   box-sizing: border-box;
   .table {
      direction: rtl;
      .thead {
         overflow-y: auto;
         overflow-x: hidden;
      }

      .tbody {
         overflow-x: hidden;
         min-height: 250px;
      }

      .tr {
         .td {
            height: 58px;
            overflow: hidden;
            font-size: 14px;
         }
         :hover {
            background-color: #ffedb1;
         }
      }
      .th,
      .td {
         padding: 0 8px 0 8px;
         margin: 0;
         position: relative;
         display: flex;
         align-items: center;
         justify-content: space-between;
         font-family: ${({ theme }) => theme.fontFamilies.Rubik};
         border: 1px solid ${({ theme }) => theme.colors.lightBlue};
         :not(:first-child) {
            .resizer {
               right: -2px;
               width: 4px;
               height: 100%;
               position: absolute;
               top: 0;
               z-index: 1;

               touch-action: none;
            }
         }
      }
      .th {
         background: #e6f6fe;
         height: 38px;
         font-size: 12px;
      }
   }
`;

const Container = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   flex-direction: column;
   align-items: center;
   padding-top: 20px;
`;

const FilterContainer = styled.div`
   display: flex;
   justify-content: space-between;
   width: 100%;
   margin-bottom: 20px;
`;

const OutlinedInput = styled(MaterialOutlinedInput)`
   max-width: 18em;
`;

const TextField = styled(MaterialTextField)`
   max-width: 11em;
`;

const FilterHeaderContianer = styled.div`
   display: flex;
   justify-content: space-between;
   width: 100%;
`;

const Text = styled.p`
   font-size: 14px;
   font-family: ${({ theme }) => theme.fontFamilies.Rubik};
   text-decoration: underline;
`;

const Btn = styled(Button)`
   background: none;
   border: none;
   margin: 0;
`;

export default {
   TableStyle,
   FilterContainer,
   OutlinedInput,
   TextField,
   Container,
   FilterHeaderContianer,
   Text,
   Btn,
};
