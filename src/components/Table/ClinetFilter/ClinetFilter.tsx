import React, { useState, useEffect, useContext } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useTranslation } from 'react-i18next';
import { IClient, IItem } from 'src/types/line';
import { clients as mockClinets } from '../../../utilis/mock';
import FilterContext, { IFilter } from '../context';
import { IColumn } from '../types';

export const clientsFilter = (rows: any, id: any, filterValue: any): any => {
   return rows.filter((row: any) => {
      const accountCode = row.values[id];
      return accountCode === filterValue.value;
   });
};

interface Props {
   column: IColumn;
}

const ClinetFilter = ({ column: { setFilter, filterValue } }: Props) => {
   const { t } = useTranslation();
   const [clients, setClients] = useState<IItem[]>([]);
   const [filters, setFilters] = useContext(FilterContext);
   useEffect(() => {}, []);

   const handleChange = (
      event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>,
   ): void => {
      const { name, value } = event.target;
      setFilters((preState: IFilter) => ({
         ...preState,
         client: { name, value },
      }));
      setFilter({ name, value });
   };

   const { client } = filters;
   return (
      <FormControl variant="outlined" size="small" style={{ width: 176 }}>
         <InputLabel id="demo-simple-select-outlined-label">
            {t('clinet')}
         </InputLabel>
         <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            label={t('clinet')}
            value={client?.value}
            onChange={handleChange}
         >
            {clients.map((c: IItem) => (
               <MenuItem value={c.value}>{c.name}</MenuItem>
            ))}
         </Select>
      </FormControl>
   );
};

export default ClinetFilter;
