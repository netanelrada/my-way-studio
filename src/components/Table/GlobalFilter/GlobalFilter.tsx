import React, { useState, useContext } from 'react';
import { useTranslation } from 'react-i18next';
import { useAsyncDebounce } from 'react-table';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import SearchIcon from '@material-ui/icons/Search';
import InputLabel from '@material-ui/core/InputLabel';
import FilterContext, { IFilter } from '../context';
import styles from './GlobalFilter.style';

interface Props {
   preGlobalFilteredRows: any;
   globalFilter: any;
   setGlobalFilter: any;
}

const GlobalFilter = ({
   preGlobalFilteredRows,
   globalFilter,
   setGlobalFilter,
}: Props): JSX.Element => {
   const { t } = useTranslation();
   const [filters, setFilters] = useContext(FilterContext);
   const onChange = useAsyncDebounce((text: string) => {
      setGlobalFilter(text || undefined);
   }, 200);

   const { freeSearch } = filters;

   return (
      <FormControl variant="outlined" size="small">
         <InputLabel htmlFor="outlined-free-search">{t('search')}</InputLabel>
         <styles.OutlinedInput
            id="outlined-free-search"
            type="text"
            value={freeSearch || ''}
            onChange={(e) => {
               setFilters((preState: IFilter) => ({
                  ...preState,
                  freeSearch: e.target.value,
               }));
               onChange(e.target.value);
            }}
            endAdornment={
               <InputAdornment position="end">
                  <SearchIcon />
               </InputAdornment>
            }
            labelWidth={78}
         />
      </FormControl>
   );
};

export default GlobalFilter;
