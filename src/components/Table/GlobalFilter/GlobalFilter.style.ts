import styled from 'styled-components';

import MaterialOutlinedInput from '@material-ui/core/OutlinedInput';

const OutlinedInput = styled(MaterialOutlinedInput)`
   max-width: 18em;
`;

export default { OutlinedInput };
