import React, { useState, useEffect, useContext } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import { useTranslation } from 'react-i18next';
import { IDepartment, IItem } from 'src/types/line';
import { departments as mockDepartments } from '../../../utilis/mock';
import FilterContext, { IFilter } from '../context';
import { IColumn } from '../types';

export const departmentFilter = (rows: any, id: any, filterValue: any): any => {
   return rows.filter((row: any) => {
      const departmentCode = row.values[id];
      return departmentCode === filterValue.value;
   });
};

interface Props {
   column: IColumn;
}

const DepartmentFilter = ({ column: { setFilter, filterValue } }: Props) => {
   const { t } = useTranslation();
   const [departments, setDepartments] = useState<IItem[]>([]);
   const [filters, setFilters] = useContext(FilterContext);

   useEffect(() => {
      setDepartments(
         mockDepartments.map((d: IDepartment) => ({
            value: d.code,
            name: d.departmentName,
         })),
      );
   }, []);

   const handleChange = (
      event: React.ChangeEvent<{ name?: string | undefined; value: unknown }>,
   ): void => {
      const { name, value } = event.target;
      setFilters((preState: IFilter) => ({
         ...preState,
         department: { name, value },
      }));
      setFilter({ name, value });
   };

   const { department } = filters;
   return (
      <FormControl variant="outlined" size="small" style={{ width: 176 }}>
         <InputLabel id="demo-simple-select-outlined-label">
            {t('department')}
         </InputLabel>
         <Select
            labelId="demo-simple-select-outlined-label"
            id="demo-simple-select-outlined"
            label={t('department')}
            value={department?.value}
            onChange={handleChange}
         >
            {departments.map((d: IItem) => (
               <MenuItem key={d.value} value={d.value}>
                  {d.name}
               </MenuItem>
            ))}
         </Select>
      </FormControl>
   );
};

export default DepartmentFilter;
