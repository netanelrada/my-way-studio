/* eslint-disable react/prop-types */
import React, { useState } from 'react';
import {
   Grid,
   GridColumn,
   GridNoRecords,
   GridColumnProps,
   GridFilterChangeEvent,
   GridSortChangeEvent,
} from '@progress/kendo-react-grid';
import {
   orderBy,
   SortDescriptor,
   CompositeFilterDescriptor,
   FilterDescriptor,
} from '@progress/kendo-data-query';
import {
   useTable,
   useResizeColumns,
   useFlexLayout,
   useFilters,
   useGlobalFilter,
   useSortBy,
   Column,
} from 'react-table';
import { useTranslation } from 'react-i18next';
import FiltersContext, { filterInitialState } from './context';
import GlobalFilter from './GlobalFilter/GlobalFilter';
import ArrowDown from '../../assets/icons/arrow_down.svg';
import ArrowUp from '../../assets/icons/arrow_up.svg';
import styles from './Table.style';
import { ILine } from '../../types/line';

const getStyles = (props: any, align = 'left') => [
   props,
   {
      style: {
         justifyContent: align === 'right' ? 'flex-end' : 'flex-start',
         alignItems: 'center',
         display: 'flex',
         textAlign: 'right',
      },
   },
];

const headerProps = (props: any, { column }: any) =>
   getStyles(props, column.align);

const cellProps = (props: any, { cell }: any) =>
   getStyles(props, cell.column.align);

// Define a default UI for filtering
export function DefaultColumnFilter({
   column: { filterValue, preFilteredRows, setFilter },
}: any) {
   return null;
}
function fuzzyTextFilterFn(rows: any, id: any, filterValue: any): any {
   console.log('rows', rows);
   console.log('id', id);
   console.log('rows', filterValue);

   //  return rows..values[id].includes(filterValue);

   // return matchSorter(rows, filterValue, {
   //    keys: [(row: any) => row.values[id]],
   // });
}

function passengersFilter(rows: any, id: any, filterValue: any): any {
   console.log('rows', rows);
   console.log('id', id);
   console.log('rows', filterValue);
   //  return rows..values[id].includes(filterValue);
   // return matchSorter(rows, filterValue, {
   //    keys: [(row: any) => row.values[id]],
   // });
}

function Table({ columns, data, initialState }: any) {
   const { t } = useTranslation();
   const filterTypes = React.useMemo(
      () => ({
         // Add a new fuzzyTextFilterFn filter type.
         fuzzyText: fuzzyTextFilterFn,
         passengersFilter,
         // Or, override the default text filter to use
         // "startWith"
         text: (rows: any, id: any, filterValue: any) => {
            return rows.filter((row: any) => {
               const rowValue = row.values[id];
               return rowValue !== undefined
                  ? String(rowValue)
                       .toLowerCase()
                       .startsWith(String(filterValue).toLowerCase())
                  : true;
            });
         },
      }),
      [],
   );
   const [filters, setFilters] = useState(filterInitialState);

   const defaultColumn = React.useMemo(
      () => ({
         // When using the useFlexLayout:
         minWidth: 72, // minWidth is only used as a limit for resizing
         maxWidth: 1050,
         Filter: DefaultColumnFilter,
      }),
      [],
   );

   const {
      getTableProps,
      headerGroups,
      rows,
      prepareRow,
      state,
      preGlobalFilteredRows,
      setGlobalFilter,
      setAllFilters,
      columns: cols,
   } = useTable(
      {
         columns,
         data,
         initialState,
         defaultColumn,
         filterTypes,
         hiddenColumns: () => {
            const hiddenColumnIds: any = [];
            columns.foreach((col: any) => {
               if (col.isVisible === false) hiddenColumnIds.push(col.id);
            });

            return hiddenColumnIds;
         },
      },
      useResizeColumns,
      useFlexLayout,
      useFilters,
      useGlobalFilter,
      useSortBy,
   );

   const getSortIcon = (column: any): JSX.Element | null => {
      if (!column.isSorted) return null;

      if (column.isSortedDesc) return <img alt="" src={ArrowUp} />;

      return <img alt="" src={ArrowDown} />;
   };

   const restFilters = () => {
      setGlobalFilter(undefined);
      setAllFilters([]);
      setFilters(filterInitialState);
   };

   const [sort, setSort] = React.useState<SortDescriptor[]>([]);
   const [skip, setSkip] = React.useState(0);

   const pageChange = (event: any) => {
      setSkip(event.page.skip);
   };

   const setFilterGrid = (): any[] => {
      let filterData: ILine[] = data;

      if (filters == null) return filterData;

      if (filters.client.value > 0)
         filterData = filterData.filter(
            (x) => x.accountCode === filters.client.value,
         );

      if (filters.department.value > 0)
         filterData = filterData.filter(
            (x) => x.departmentCode === filters.department.name,
         );

      if (filters.passengers)
         filterData = filterData.filter((x) =>
            x.passengers.some((p) => p.fullName === filters.passengers),
         );

      if (filters.statusLine.value > 0)
         filterData = filterData.filter(
            (x) => x.lineStatus === filters.statusLine.value,
         );

      if (filters.freeSearch)
         filterData = filterData.filter((x) =>
            Object.values(x).some(
               (y) =>
                  (y.includes && y.includes(filters.freeSearch)) ||
                  y === +filters.freeSearch,
            ),
         );

      return filterData;
   };

   const getData = (): any[] => {
      const filterData = setFilterGrid();
      return orderBy(filterData, sort).slice(skip, skip + 20);
   };

   const isResizable = true;
   const isReorderable = true;
   const isSortable = true;
   const isNavigatable = false;

   console.log(filters);
   console.log(columns);

   return (
      <styles.Container>
         <styles.FilterHeaderContianer>
            <styles.Text>{`${t('lineList')} (${data.length})`}</styles.Text>
            <styles.Btn size="small" onClick={restFilters}>
               <styles.Text>{t('clearFilter')}</styles.Text>
            </styles.Btn>
         </styles.FilterHeaderContianer>
         <FiltersContext.Provider value={[filters, setFilters]}>
            <styles.FilterContainer>
               <GlobalFilter
                  preGlobalFilteredRows={preGlobalFilteredRows}
                  globalFilter={state.globalFilter}
                  setGlobalFilter={setGlobalFilter}
               />
               {/* <ClinetFilter />
               <DepartmentFilter /> */}
               {cols.map((col) => col.canFilter && col.render('Filter'))}

               {/* <StatusLineFilter /> */}
            </styles.FilterContainer>
         </FiltersContext.Provider>
         <div
            className="k-rtl"
            style={{
               height: '440px',
               width: '100%',
               flex: 'auto',
            }}
         >
            <Grid
               style={{
                  height: '440px',
                  width: '100%',
                  flex: 'auto',
               }}
               rowHeight={40}
               navigatable={isNavigatable}
               data={getData()}
               pageSize={20}
               total={data.length}
               skip={skip}
               sortable={isSortable}
               sort={sort}
               onSortChange={(e: GridSortChangeEvent) => {
                  setSort(e.sort);
               }}
               resizable={isResizable}
               reorderable={isReorderable}
               scrollable="virtual"
               onPageChange={pageChange}
            >
               <GridNoRecords>{t('noRecordWasFound')}</GridNoRecords>
               {columns
                  .filter((x: any) => x.Header)
                  .map((column: any) => {
                     return (
                        <GridColumn field={column.id} title={column.Header} />
                     );
                  })}
            </Grid>
         </div>
      </styles.Container>
   );
}

interface Prop {
   data: Array<ILine>;
   columns: Array<Column>;
   hiddenColumns: Array<string>;
}

const ResizeTable = ({ columns, data, hiddenColumns }: Prop) => {
   return (
      <styles.TableStyle>
         <Table
            columns={columns}
            data={data}
            initialState={{
               sortBy: [
                  {
                     id: 'startTime',
                     desc: false,
                  },
               ],
               hiddenColumns,
            }}
         />
      </styles.TableStyle>
   );
};

export default ResizeTable;
