import styled from 'styled-components';
import MaterialOutlinedInput from '@material-ui/core/OutlinedInput';
import MaterialTextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const Container = styled.div`
   width: 100%;
   height: 100%;
   display: flex;
   flex-direction: column;
   align-items: center;
   padding: 1.25em 0;
`;

const HeaderContainer = styled.div`
   display: flex;
   width: 100%;
   justify-content: space-between;
   align-items: center;
   padding: 0 1.6875em;
   height: 8%;
`;

const LeftHeaderContainer = styled.div`
   display: flex;
   align-items: center;
`;

const FilterContainer = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
   width: 100%;
   padding: 0 1.6875em;
   height: 8%;
`;

const FlexContainerWithSpace = styled.div`
   display: flex;
   width: 100%;
`;

const FilterHeaderContianer = styled.div`
   display: flex;
   justify-content: space-between;
   align-items: center;
   width: 100%;
   padding: 0 1.6875em;
   height: 5%;
`;

const TableContainer = styled.div`
   width: 100%;
   display: flex;
   flex-direction: column;
   align-items: center;
   padding: 0.5em 1.6875em 0 1.6875em;
   height: 75%;
   box-sizing: border-box;
   overflow: hidden;
`;
const OutlinedInput = styled(MaterialOutlinedInput)`
   max-width: 18em;
`;

const TextField = styled(MaterialTextField)`
   font-size: 2rem;
`;

const TextContainer = styled.div`
   display: flex;
   justify-content: center;
   align-items: center;
   height: 100%;
`;

const Text = styled.p<{
   color?: string;
}>`
   font-family: ${({ theme }) => theme.fontFamilies.SalsaRegular};
   font-style: normal;
   font-weight: normal;
   font-size: 36px;
   line-height: 100%;
   color: ${({ color = 'black' }) => color};
   margin: 0;
`;

const Btn = styled(Button)`
   background: none;
   border: none;
   margin: 0;
`;

const FilterText = styled.p`
   font-size: 14px;
   font-family: ${({ theme }) => theme.fontFamilies.Rubik};
   text-decoration: underline;
`;

const Hr = styled.hr`
   width: 100%;
`;

const Label = styled.label`
   font-size: 14px;
   font-family: ${({ theme }) => theme.fontFamilies.Rubik};
`;

export default {
   Container,
   FilterContainer,
   FlexContainerWithSpace,
   HeaderContainer,
   TextContainer,
   LeftHeaderContainer,
   FilterHeaderContianer,
   TableContainer,
   TextField,
   OutlinedInput,
   Text,
   Btn,
   FilterText,
   Hr,
   Label,
};
