import React, { FunctionComponent, CSSProperties } from 'react';
import { useTranslation } from 'react-i18next';
import ListItemText from '@material-ui/core/ListItemText';
import { Box } from '@material-ui/core';

export interface IProps {
   shiftName: string;
   collectTime: string;
   dropTime: string;
}

const ShiftItem: FunctionComponent<IProps> = ({
   shiftName,
   collectTime,
   dropTime,
}: IProps) => {
   const { t } = useTranslation();

   const borderStyle: CSSProperties = {
      border: '1px solid #BEBEBE',
      margin: '0 5px',
      width: '1px',
      height: '1rem',
   };

   const flexBasis10Style: CSSProperties = {
      flexBasis: '10%',
   };
   const flexBasis5Style: CSSProperties = {
      flexBasis: '5%',
   };
   const flexBasis30Style: CSSProperties = {
      flexBasis: '30%',
   };

   return (
      <Box
         display="flex"
         component="span"
         style={{
            width: '100%',
            display: 'flex',
            maxHeight: '1em',
            lineHeight: 1.5,

            position: 'relative',
            boxSizing: 'border-box',
            textAlign: 'right',
            alignItems: 'center',
            justifyContent: 'flex-start',
         }}
      >
         <ListItemText style={flexBasis30Style}>{shiftName}:</ListItemText>
         <ListItemText style={flexBasis10Style}>
            <b>{t('pickUp')}</b>
         </ListItemText>
         <ListItemText style={flexBasis10Style}> {collectTime}</ListItemText>
         <ListItemText style={flexBasis5Style}>
            <span style={borderStyle} />
         </ListItemText>

         <ListItemText style={flexBasis10Style}>
            <b>{t('drop')}</b>
         </ListItemText>
         <ListItemText>{dropTime}</ListItemText>
      </Box>
   );
};

export default ShiftItem;
