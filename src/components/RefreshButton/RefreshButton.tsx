import React, { CSSProperties, FC } from 'react';

import { IconButton } from '@material-ui/core';
import Autorenew from '@material-ui/icons/Autorenew';
import { useTranslation } from 'react-i18next';

export interface IRefreshButton {
   onClick: () => void;
}

const refreshButtonCss: CSSProperties = {
   border: '1px solid #2196F3',
   borderRadius: 'unset',
   padding: '6px',
   marginRight: '5px',
};

const RefreshButton: FC<IRefreshButton> = ({ onClick }: IRefreshButton) => {
   const { t } = useTranslation();
   return (
      <IconButton
         style={refreshButtonCss}
         color="primary"
         onClick={onClick}
         size="small"
      >
         <Autorenew color="primary" titleAccess={t('refresh')} />
         {/* {t('refresh')} */}
      </IconButton>
   );
};

export default RefreshButton;
