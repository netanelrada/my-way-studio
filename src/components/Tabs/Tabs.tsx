import React, { useState } from 'react';
import { makeStyles, Theme } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { useTranslation } from 'react-i18next';

interface Props {
   index: number;
   setIndex: (newIndex: number) => void;
}
const useStyles = makeStyles((theme: Theme) => ({
   root: {
      flexGrow: 1,
      width: '100%',
   },
   tabs: {
      width: '100%',
      padding: 0,
      margin: 0,
   },
   tab: {
      width: '100%',
      border: '1px solid red',
   },
}));

const CustomTabs = ({ index, setIndex }: Props) => {
   const { t } = useTranslation();
   const styles = useStyles();

   const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
      setIndex(newValue);
   };

   return (
      <Paper className={styles.root}>
         <Tabs
            className={styles.tabs}
            value={index}
            onChange={handleChange}
            indicatorColor="primary"
            textColor="primary"
            variant="fullWidth"
         >
            <Tab fullWidth label={t('loginWithPhone')} />
            <Tab fullWidth label={t('loginwithUser')} />
         </Tabs>
      </Paper>
   );
};

export default CustomTabs;
