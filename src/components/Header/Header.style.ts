import styled from 'styled-components';

const Container = styled.div`
   width: 100%;
   height: 82px;
   background: #f9f8f8;
   box-shadow: 0px 10px 20px rgba(0, 0, 0, 0.16);
   display: flex;
   flex-direction: column;
`;

const TextContainer = styled.div`
   display: flex;
   flex-direction: row-reverse;
   justify-content: center;
   align-items: center;
   height: 100%;
`;

const Text = styled.p<{
   color?: string;
}>`
   font-family: ${({ theme }) => theme.fontFamilies.SalsaRegular};
   font-style: normal;
   font-weight: normal;
   font-size: 36px;
   line-height: 100%;
   color: ${({ color = 'black' }) => color};
   margin: 0 10px 0 0;
`;

const Hr = styled.div`
   align-self: flex-start;
   width: 100%;
   height: 6px;
   background-color: ${({ theme }) => theme.colors.blue};
`;

export default { Container, Text, Hr, TextContainer };
