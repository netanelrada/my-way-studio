import React, { FunctionComponent } from 'react';
import { useTranslation } from 'react-i18next';

export interface IProps {
   dropTime?: string;
}

const ShiftDropItem: FunctionComponent<IProps> = ({ dropTime }: IProps) => {
   const { t } = useTranslation();

   return (
      <span>
         {t('drop')}:<b style={{ paddingRight: '4px' }}>{dropTime}</b>
      </span>
   );
};

export default ShiftDropItem;
