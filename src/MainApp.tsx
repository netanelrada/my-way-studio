import React, { ReactElement, useState } from 'react';
import { useSelector } from 'react-redux';
import { useTranslation } from 'react-i18next';

import { Button } from '@material-ui/core';
import { isConnectSelector } from './store/selectores/loginSelectors';
import { IRootReducer } from './store/reducers';
import Fotter from './components/Footer/Footer';
import Routes from './routes/Routes';
import { ReqStatus } from './api/types';
import Dialog from './components/Dialog/Dialog';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface Props {}

export default function MainApp(props: Props): ReactElement {
   const { t } = useTranslation();

   const documentIE: any = document;
   const isIE = false || !!documentIE.documentMode;

   const [isDialogOpen, setIsDialogOpen] = useState<boolean>(isIE);

   const onCloseDialog = () => {
      setIsDialogOpen(false);
   };

   const DialogAction = () => {
      return (
         <>
            <Button onClick={onCloseDialog} color="primary">
               {t('ok')}
            </Button>
         </>
      );
   };

   return (
      <>
         <Routes />
         {/* <Fotter /> */}
         <Dialog
            DialogActionsChildren={DialogAction()}
            DialogContentChildren={<h1>{t('interntExplorerError')}</h1>}
            DialogTitleChildren={<h1>{t('interntExplorerError')}</h1>}
            open={isDialogOpen}
            onClose={onCloseDialog}
         />
      </>
   );
}
