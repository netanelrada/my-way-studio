import { createStore, applyMiddleware } from 'redux';
import createSagaMiddleware from 'redux-saga';
import { SingupType } from 'src/types/login';

import { composeWithDevTools } from 'redux-devtools-extension';
import { setter, getter } from '@progress/kendo-react-common';
import {
   setLocalStorageValue,
   getLocalStorageValue,
   setSessionStorageValue,
   getSessionStorageValue,
   jsonDateReviver,
   mergeDeep,
   RemoveLocalStorageKey,
   RemoveSessionStorageKey,
} from 'src/utilis/utilis';
import { StorageKeys, StorageType } from 'src/types/global';

import { IRootReducer, rootReducer } from './reducers/index';
import rootSaga from './sagas/index';

const Saga = createSagaMiddleware();

const windowPersistedState = JSON.parse(
   getSessionStorageValue(StorageKeys.ReduxState) || '{}',
   jsonDateReviver,
);
const browserPersistedState = JSON.parse(
   getLocalStorageValue(StorageKeys.ReduxState) || '{}',
   jsonDateReviver,
);

const initialState = mergeDeep(windowPersistedState, browserPersistedState);

const store = createStore(
   rootReducer,
   initialState,
   composeWithDevTools(applyMiddleware(Saga)),
);

const whitelist: Array<{ path: string; storageType: StorageType }> = [
   {
      path: 'loginReducer.selectedFcAccount',
      storageType: StorageType.LocalStorage,
   },
   {
      path: 'loginReducer.loginType',
      storageType: StorageType.SessionStorag,
   },
   {
      path: 'loginReducer.selectedDate',
      storageType: StorageType.SessionStorag,
   },
];

const onLoginType = (storeState: IRootReducer) => {
   const { loginReducer }: IRootReducer = storeState;

   if (loginReducer && loginReducer.loginType === SingupType.OnTimeLogin) {
      RemoveLocalStorageKey(StorageKeys.DeviceToken);

      setSessionStorageValue(
         StorageKeys.OnTimeLogin,
         JSON.stringify(loginReducer),
      );
   } else {
      RemoveSessionStorageKey(StorageKeys.OnTimeLogin);
   }
};

const saveToLocalStorage = (storeState: IRootReducer) => {
   const storageObject = {};

   whitelist
      .filter((ele) => ele.storageType === StorageType.LocalStorage)
      .forEach((ele) => {
         const satterObject = setter(ele.path);
         const getterObject = getter(ele.path);
         satterObject(storageObject, getterObject(storeState));
      });

   setLocalStorageValue(StorageKeys.ReduxState, JSON.stringify(storageObject));
};

const saveToSessionStorage = (storeState: IRootReducer) => {
   const sessionObject = {};

   whitelist
      .filter((ele) => ele.storageType === StorageType.SessionStorag)
      .forEach((ele) => {
         const satterObject = setter(ele.path);
         const getterObject = getter(ele.path);
         satterObject(sessionObject, getterObject(storeState));
      });

   setSessionStorageValue(
      StorageKeys.ReduxState,
      JSON.stringify(sessionObject),
   );
};

store.subscribe(() => {
   const storeState: IRootReducer | any = store.getState();

   onLoginType(storeState);

   saveToLocalStorage(storeState);

   saveToSessionStorage(storeState);
});

Saga.run(rootSaga);

export default { store };
