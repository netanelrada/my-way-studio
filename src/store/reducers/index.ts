import { AnyAction, combineReducers, Reducer } from 'redux';
import { ILoginState } from 'src/store/type';
import loginReducer from './loginReducer';
import lineReducer, { ILineState } from './lineReducer';
import passengersShiftReudcer, {
   IPassengerShiftState,
} from './passengersShiftReudcer';
import passengerReudcer, { IPassengerState } from './passengerReducer';

export interface IRootReducer extends AnyAction {
   loginReducer: ILoginState;
   lineReducer: ILineState;
   passengersShiftReudcer: IPassengerShiftState;
   passengerReudcer: IPassengerState;
}

export const rootReducer = combineReducers<Reducer<IRootReducer>>({
   loginReducer,
   lineReducer,
   passengersShiftReudcer,
   passengerReudcer,
});
