import { Reducer } from 'redux';
import { DefaultRootState } from 'react-redux';
import { IPassenger } from 'src/api/passengerMangerApi/types';
import { ReqStatus } from 'src/api/types';
import { addOrUpdateItemById } from 'src/utilis/utilis';

import { IReduxProvider } from '../type';
import { PassengerActionType } from '../actions/actionType';

export interface IPassengerState extends DefaultRootState {
   passengers: Array<IPassenger>;
   clientId?: string | number | undefined;
   modefiedPassenger?: IPassenger;
   loadingPassengers: ReqStatus;
}

const initialState: IPassengerState = {
   passengers: [],
   loadingPassengers: ReqStatus.SUCCESS,
};

const onRetrivePassenger = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.LOADING,
   };
};

const onRetrivePassengerSuccess = (
   state: IPassengerState,
   payload: Array<IPassenger> | any,
): any => {
   return {
      ...state,
      passengers: [...payload],
      loadingPassengers: ReqStatus.SUCCESS,
   };
};

export const onSetClientId = (
   state: IPassengerState,
   payload: string | number | undefined | any,
): any => {
   return {
      ...state,
      clientId: payload,
   };
};

export const onSetModefiedPassenger = (
   state: IPassengerState,
   payload: IPassenger | undefined | any,
): any => {
   return {
      ...state,
      modefiedPassenger: payload,
   };
};

export const onChangeSelection = (
   state: IPassengerState,
   payload: Array<IPassenger> | any,
): any => {
   return {
      ...state,
      passengers: [...payload],
   };
};

const onRetrivePassengerFail = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.FAIL,
   };
};

const onDeletePassenger = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.LOADING,
   };
};

const onDeletePassengerSuccess = (
   state: IPassengerState,
   payload: Array<string> | any,
): IPassengerState => {
   return {
      ...state,
      passengers: state.passengers.map((item: IPassenger) => ({
         ...item,
         isActive: payload.indexOf(item.passCode) > -1 ? '0' : item.isActive,
      })),
      modefiedPassenger: state.modefiedPassenger && {
         ...state.modefiedPassenger,
         isActive:
            payload.indexOf(state.modefiedPassenger) > -1
               ? '0'
               : state.modefiedPassenger.isActive,
      },
      loadingPassengers: ReqStatus.SUCCESS,
   };
};

const onDeletePassengerFail = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.FAIL,
   };
};

const onSetPassenger = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.LOADING,
   };
};

const onSetPassengerSuccess = (
   state: IPassengerState,
   payload: IPassenger | any,
): IPassengerState => {
   return {
      ...state,
      passengers: addOrUpdateItemById(state.passengers, payload, 'passCode'),
      modefiedPassenger: undefined,
      loadingPassengers: ReqStatus.SUCCESS,
   };
};

const onSetPassengerFail = (state: IPassengerState): IPassengerState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.FAIL,
   };
};

const handlerTypes: {
   [index: string]: IReduxProvider<IPassengerState>;
} = {
   [PassengerActionType.retrivePassenger]: onRetrivePassenger,
   [PassengerActionType.retrivePassengerSuccess]: onRetrivePassengerSuccess,
   [PassengerActionType.retrivePassengerFail]: onRetrivePassengerFail,
   [PassengerActionType.changeSelection]: onChangeSelection,
   [PassengerActionType.setClientId]: onSetClientId,
   [PassengerActionType.setModefiedPassenger]: onSetModefiedPassenger,
   [PassengerActionType.deletePassenger]: onDeletePassenger,
   [PassengerActionType.deletePassengerSuccess]: onDeletePassengerSuccess,
   [PassengerActionType.deletePassengerFail]: onDeletePassengerFail,
   [PassengerActionType.setPassenger]: onSetPassenger,
   [PassengerActionType.setPassengerSuccess]: onSetPassengerSuccess,
   [PassengerActionType.setPassengerFail]: onSetPassengerFail,
};

const passengerReudcer = (
   state = initialState,
   { type, payload }: { type: PassengerActionType; payload: IPassengerState },
): any => {
   const handler = handlerTypes[type];
   if (handler) {
      return handler(state, payload);
   }
   return state;
};

export default passengerReudcer;
