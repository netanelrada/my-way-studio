import { ReqStatus } from 'src/api/types';
import { IAccount, RefreshTokenStatus, SingupType } from 'src/types/login';
import { LoginActionType } from '../actions/actionType';
import { IReduxProvider, ILoginState } from '../type';

const initialState: ILoginState = {
   isAuthenticated: null,
   refreshTokenStatus: RefreshTokenStatus.Init,
   token: '',
   fcAccounts: [],
   gps_server: '',
   gps_token: '',
   loginType: null,
   isIgnoreInitialRoute: false,
};

const initialConfiguration = (
   state: ILoginState,
   payload: any,
): ILoginState => {
   return {
      ...state,
      isAuthenticated: ReqStatus.LOADING,
   };
};

const initialConfigurationSucces = (
   state: ILoginState,
   payload: any,
): ILoginState => {
   return {
      ...state,
      ...payload,
      isAuthenticated: ReqStatus.SUCCESS,
      refreshTokenStatus: RefreshTokenStatus.Ok,
      selectedFcAccount:
         !state.selectedFcAccount && payload?.fcAccounts?.length
            ? payload.fcAccounts[0]
            : state.selectedFcAccount,
      selectedDate: state.selectedDate || new Date(),
   };
};

const initConnectFail = (state: ILoginState, payload: any): ILoginState => {
   return {
      ...state,
      loginType: 0,
      isAuthenticated: ReqStatus.FAIL,
   };
};

const setFcAccount = (state: ILoginState, payload: IAccount): ILoginState => {
   return {
      ...state,
      selectedFcAccount: payload,
   };
};

const setSelectedDate = (state: ILoginState, payload: Date): ILoginState => {
   return {
      ...state,
      selectedDate: payload,
   };
};

const setLogout = (state: ILoginState): ILoginState => {
   return {
      ...initialState,
      selectedFcAccount: null,
      loginType: 0,
   };
};

const setLoginType = (state: ILoginState, payload: SingupType): ILoginState => {
   return {
      ...state,
      loginType: payload,
      isIgnoreInitialRoute: true,
   };
};

const setTokenRefreshStatus = (
   state: ILoginState,
   payload: RefreshTokenStatus,
): ILoginState => {
   return {
      ...state,
      selectedFcAccount: state.selectedFcAccount,
      refreshTokenStatus: payload,
   };
};

const handlerTypes: {
   [index: string]: IReduxProvider<any>;
} = {
   [LoginActionType.initialConfiguration]: initialConfiguration,
   [LoginActionType.initialConfigurationSuccess]: initialConfigurationSucces,
   [LoginActionType.initialConfigurationFail]: initConnectFail,
   [LoginActionType.SetFcAccount]: setFcAccount,
   [LoginActionType.SetDate]: setSelectedDate,
   [LoginActionType.Logout]: setLogout,
   [LoginActionType.SetLoginType]: setLoginType,
   [LoginActionType.SetTokenRefreshStatus]: setTokenRefreshStatus,
};

const loginReducer = (
   state = initialState,
   { type, payload }: { type: LoginActionType; payload: object },
): ILoginState => {
   const handler = handlerTypes[type];
   if (handler) {
      return handler(state, payload);
   }
   return state;
};

export default loginReducer;
