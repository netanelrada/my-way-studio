import { Reducer } from 'redux';
import { DefaultRootState } from 'react-redux';
import {
   IPassengerShifts,
   ISelectedShifts,
   IShifts,
} from 'src/api/manualOrderApi/types';

import { ReqStatus } from 'src/api/types';
import { PassengersShiftActionType } from '../actions/actionType';
import { IReduxProvider } from '../type';

export interface IPassengerShiftState extends DefaultRootState {
   passengers: Array<IPassengerShifts>;
   shifts: Array<IShifts>;
   selectedShift?: ISelectedShifts;
   loadingPassengers: ReqStatus;
   updatePassengers: ReqStatus;
   loadingShifts: ReqStatus;
}

const initialState: IPassengerShiftState = {
   passengers: [],
   shifts: [],
   loadingPassengers: ReqStatus.SUCCESS,
   updatePassengers: ReqStatus.SUCCESS,
   loadingShifts: ReqStatus.SUCCESS,
};

const onRetrivePassenger = (
   state: IPassengerShiftState,
): IPassengerShiftState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.LOADING,
   };
};

const onRetrivePassengerSuccess = (
   state: IPassengerShiftState,
   payload: any,
): any => {
   return {
      ...state,
      passengers: [...payload],
      loadingPassengers: ReqStatus.SUCCESS,
   };
};

const onRetrivePassengerFail = (
   state: IPassengerShiftState,
): IPassengerShiftState => {
   return {
      ...state,
      loadingPassengers: ReqStatus.FAIL,
   };
};

const onRetriveShiftsSuccess = (state: any, payload: any): any => {
   return {
      ...state,
      shifts: [...payload],
      loadingPassengers: ReqStatus.SUCCESS,
   };
};

const onAddPassngerShifts = (
   state: IPassengerShiftState | any,
   payload: IPassengerShifts | any,
): any => {
   return {
      ...state,
      updatePassengers: ReqStatus.LOADING,
   };
};

const onAddPassngerShiftsSuccess = (
   state: IPassengerShiftState | any,
   payload: IPassengerShifts | any,
): any => {
   return {
      ...state,
      passengers: state.passengers.map((item: IPassengerShifts) => {
         if (item.passId === payload.passId) return { ...payload };

         return { ...item };
      }),
      updatePassengers: ReqStatus.SUCCESS,
   };
};

const onDelPassngerShifts = (
   state: IPassengerShiftState | any,
   payload: IPassengerShifts | any,
): any => {
   return {
      ...state,
      updatePassengers: ReqStatus.LOADING,
   };
};

const onDelPassngerShiftsSuccess = (
   state: IPassengerShiftState | any,
   payload: IPassengerShifts | any,
): any => {
   return {
      ...state,
      passengers: state.passengers.map((item: IPassengerShifts) => {
         if (item.passId === payload.passId) return { ...payload };

         return { ...item };
      }),
      updatePassengers: ReqStatus.SUCCESS,
   };
};

const setSelectedShift = (
   state: IPassengerShiftState | any,
   payload: IShifts | any,
): any => {
   return {
      ...state,
      selectedShift: payload,
   };
};

const handlerTypes: {
   [index: string]: IReduxProvider<IPassengerShiftState>;
} = {
   [PassengersShiftActionType.retrivePassenger]: onRetrivePassenger,
   [PassengersShiftActionType.retrivePassengerSuccess]:
      onRetrivePassengerSuccess,
   [PassengersShiftActionType.addPassngerShifts]: onAddPassngerShifts,
   [PassengersShiftActionType.addPassngerShiftsSuccess]:
      onAddPassngerShiftsSuccess,
   [PassengersShiftActionType.delPassngerShifts]: onDelPassngerShifts,
   [PassengersShiftActionType.delPassngerShiftsSuccess]:
      onDelPassngerShiftsSuccess,
   [PassengersShiftActionType.retrivePassengerFail]: onRetrivePassengerFail,
   [PassengersShiftActionType.retriveShiftsSuccess]: onRetriveShiftsSuccess,
   [PassengersShiftActionType.setSelectedShift]: setSelectedShift,
};

const passengersShiftReudcer = (
   state = initialState,
   {
      type,
      payload,
   }: { type: PassengersShiftActionType; payload: IPassengerShiftState },
): any => {
   const handler = handlerTypes[type];
   if (handler) {
      return handler(state, payload);
   }
   return state;
};

export default passengersShiftReudcer;
