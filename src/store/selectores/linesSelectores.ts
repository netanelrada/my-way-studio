import { ILine } from 'src/types/line';
import { ReqStatus } from 'src/api/types';
import { ILineState } from '../reducers/lineReducer';

export const linesDataSelector = (state: any): ILineState => state.lineReducer;

export const linesSelector = (state: any): ILine[] =>
   linesDataSelector(state).lines;

export const lineSelector = (state: any): ILine | undefined =>
   linesDataSelector(state).selectedLine;
