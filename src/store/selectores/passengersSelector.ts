import { IPassenger } from 'src/api/passengerMangerApi/types';
import { IRootReducer } from '../reducers';
import { IPassengerState } from '../reducers/passengerReducer';

export const passengerRootDataSelector = (
   state: IRootReducer,
): IPassengerState => state.passengerReudcer;

export const passengerSelector = (state: IRootReducer): Array<IPassenger> =>
   passengerRootDataSelector(state).passengers;

export const clientIdSelector = (
   state: IRootReducer,
): string | number | undefined => passengerRootDataSelector(state).clientId;

export const modefiedPassengerSelector = (
   state: IRootReducer,
): IPassenger | undefined => passengerRootDataSelector(state).modefiedPassenger;
