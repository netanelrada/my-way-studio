import {
   IPassengerShifts,
   ISelectedShifts,
   IShifts,
} from 'src/api/manualOrderApi/types';
import { IRootReducer } from '../reducers';
import { IPassengerShiftState } from '../reducers/passengersShiftReudcer';

export const passengerRootDataSelector = (
   state: IRootReducer,
): IPassengerShiftState => state.passengersShiftReudcer;

export const passengerSelector = (
   state: IRootReducer,
): Array<IPassengerShifts> => passengerRootDataSelector(state).passengers;

export const shiftsSelector = (state: IRootReducer): Array<IShifts> =>
   passengerRootDataSelector(state).shifts;

export const selectedShiftSelector = (
   state: IRootReducer,
): ISelectedShifts | undefined =>
   passengerRootDataSelector(state).selectedShift;
