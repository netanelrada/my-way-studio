import {
   IPassengerShifts,
   ISelectedShifts,
   IShifts,
} from 'src/api/manualOrderApi/types';
import { PassengersShiftActionType } from './actionType';
import { ReduxDispatch } from '../type';

export const onRetrivePassenger = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.retrivePassenger,
      payload,
   };
};

export const onRetrivePassengerSuccess = (
   payload: Array<IPassengerShifts>,
): ReduxDispatch<Array<IPassengerShifts>> => {
   return {
      type: PassengersShiftActionType.retrivePassengerSuccess,
      payload,
   };
};

export const onRetrivePassengerFaild = <T = {}>(
   payload: T,
): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.retrivePassengerFail,
      payload,
   };
};

export const onAddPassngerShifts = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.addPassngerShifts,
      payload,
   };
};

export const onAddPassngerShiftsSuccess = <T = {}>(
   payload: T,
): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.addPassngerShiftsSuccess,
      payload,
   };
};

export const onAddPassngerShiftsFaild = <T = {}>(
   payload: T,
): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.addPassngerShiftsFaild,
      payload,
   };
};

export const onDelPassngerShifts = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.delPassngerShifts,
      payload,
   };
};

export const onDelPassngerShiftsSuccess = <T = {}>(
   payload: T,
): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.delPassngerShiftsSuccess,
      payload,
   };
};

export const onDelPassngerShiftsFaild = <T = {}>(
   payload: T,
): ReduxDispatch<T> => {
   return {
      type: PassengersShiftActionType.delPassngerShiftsFaild,
      payload,
   };
};
export const onRetriveShiftsSuccess = (
   payload: Array<IShifts>,
): ReduxDispatch<Array<IShifts>> => {
   return {
      type: PassengersShiftActionType.retriveShiftsSuccess,
      payload,
   };
};

export const onSetSelectedShift = (
   payload: ISelectedShifts | undefined,
): ReduxDispatch<ISelectedShifts | undefined> => {
   return {
      type: PassengersShiftActionType.setSelectedShift,
      payload,
   };
};
