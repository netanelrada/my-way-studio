import { AnyAction } from 'redux';
import { RefreshTokenStatus, SingupType } from 'src/types/login';
import { LoginActionType } from './actionType';
import { ReduxDispatch } from '../type';

export const initConnect = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: LoginActionType.initialConfiguration,
      payload,
   };
};

export const initConnectSuccess = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: LoginActionType.initialConfigurationSuccess,
      payload,
   };
};

export const initConnectFail = <T = {}>(payload: T): ReduxDispatch<T> => {
   return {
      type: LoginActionType.initialConfigurationFail,
      payload,
   };
};

export const setFcAccount = <IAccount>(
   payload: IAccount,
): ReduxDispatch<IAccount> => {
   return {
      type: LoginActionType.SetFcAccount,
      payload,
   };
};

export const setDate = <Date>(payload: Date): ReduxDispatch<Date> => {
   return {
      type: LoginActionType.SetDate,
      payload,
   };
};

export const setLogout = (): AnyAction => {
   return {
      type: LoginActionType.Logout,
   };
};

export const setLoginType = (payload: SingupType): AnyAction => {
   return {
      type: LoginActionType.SetLoginType,
      payload,
   };
};

export const setTokenRefreshStatus = (
   payload: RefreshTokenStatus,
): AnyAction => {
   return {
      type: LoginActionType.SetTokenRefreshStatus,
      payload,
   };
};
