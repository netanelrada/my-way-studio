export enum LoginActionType {
   initialConfiguration = 'initialConfiguration',
   initialConfigurationSuccess = 'initialConfigurationSuccess',
   initialConfigurationFail = 'initialConfigurationFail',
   ConnectFail = 'ConnectFail',
   UserSetting = 'UserSetting',
   UserData = 'UserData',
   SetFcAccount = 'SetFcAccount',
   SetDate = 'SetDate',
   Logout = 'Logout',
   SetLoginType = 'SetLoginType',
   SetTokenRefreshStatus = 'SetTokenRefreshStatus',
}

export enum LineActionType {
   retriveLines = 'retriveLines',
   retriveLineSuccess = 'retriveLineSuccess',
   retriveLineFaild = 'retriveLineFaild',
   selectedLineChange = 'selectedLineChange',
}

export enum PassengersShiftActionType {
   retrivePassenger = 'RetrivePassengerShifts',
   retrivePassengerSuccess = 'RetrivePassengerShiftsSuccess',
   addPassngerShifts = 'addPassngerShifts',
   addPassngerShiftsSuccess = 'addPassngerShiftsSuccess',
   addPassngerShiftsFaild = 'addPassngerShiftsFaild',
   delPassngerShifts = 'delPassngerShifts',
   delPassngerShiftsSuccess = 'delPassngerShiftsSuccess',
   delPassngerShiftsFaild = 'delPassngerShiftsFaild',
   retrivePassengerFail = 'RetrivePassengerShiftsFail',
   retriveShiftsSuccess = 'RetriveShiftsSuccess',
   setSelectedShift = 'setSelectedShift',
}

export enum PassengerActionType {
   retrivePassenger = 'RetrivePassenger',
   retrivePassengerSuccess = 'RetrivePassengerSuccess',
   setClientId = 'setClientId',
   setModefiedPassenger = 'modefiedPassenger',
   retrivePassengerFail = 'RetrivePassengerFail',
   changeSelection = 'ChangeSelection',
   deletePassenger = 'DeletePassenger',
   deletePassengerSuccess = 'DeletePassengerSuccess',
   deletePassengerFail = 'DeletePassengerFail',
   setPassenger = 'setPassenger',
   setPassengerSuccess = 'setPassengerSuccess',
   setPassengerFail = 'setPassengerFail',
}
