import {
   IDeletePassengerSagaRequest,
   IPassenger,
   ISetSagaPassenger,
} from 'src/api/passengerMangerApi/types';
import { PassengerActionType } from './actionType';
import { ReduxDispatch } from '../type';

export const onRetrivePassengersSuccess = (
   payload: Array<IPassenger>,
): ReduxDispatch<Array<IPassenger>> => {
   return {
      type: PassengerActionType.retrivePassengerSuccess,
      payload,
   };
};

export const onRetrivePassengersFaild = (
   payload: never,
): ReduxDispatch<never> => {
   return {
      type: PassengerActionType.retrivePassengerFail,
      payload,
   };
};

export const onChangeSelection = (
   payload: IPassenger[],
): ReduxDispatch<IPassenger[]> => {
   return {
      type: PassengerActionType.changeSelection,
      payload,
   };
};

export const onSetClientId = (
   payload: string | number | undefined | null,
): ReduxDispatch<string | number | undefined | null> => {
   return {
      type: PassengerActionType.setClientId,
      payload,
   };
};

export const onSetModefiedPassenger = (
   payload: IPassenger | undefined | null,
): ReduxDispatch<IPassenger | undefined | null> => {
   return {
      type: PassengerActionType.setModefiedPassenger,
      payload,
   };
};

export const onDeletePassengers = (
   payload: IDeletePassengerSagaRequest,
): ReduxDispatch<IDeletePassengerSagaRequest> => {
   return {
      type: PassengerActionType.deletePassenger,
      payload,
   };
};

export const onDeletePassengersSuccess = (
   payload: number[] | string[],
): ReduxDispatch<number[] | string[]> => {
   return {
      type: PassengerActionType.deletePassengerSuccess,
      payload,
   };
};

export const onDeletePassengersFaild = (): ReduxDispatch<unknown> => {
   return {
      type: PassengerActionType.deletePassengerFail,
      payload: null,
   };
};

export const onSetPassengers = (
   payload: ISetSagaPassenger,
): ReduxDispatch<ISetSagaPassenger> => {
   return {
      type: PassengerActionType.setPassenger,
      payload,
   };
};

export const onSetPassengersSuccess = (
   payload: IPassenger,
): ReduxDispatch<IPassenger> => {
   return {
      type: PassengerActionType.setPassengerSuccess,
      payload,
   };
};

export const onSetPassengersFaild = (): ReduxDispatch<unknown> => {
   return {
      type: PassengerActionType.setPassengerFail,
      payload: null,
   };
};
