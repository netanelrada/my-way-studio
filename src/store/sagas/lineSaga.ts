import { takeLatest, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga/index';
import { ILinesRequest } from 'src/api/types';

import { getLines } from '../../api/api';
import { LineActionType } from '../actions/actionType';
import {
   onRetriveLinesSuccess,
   onRetriveLinesFaild,
} from '../actions/LineAction';

interface IRetrivalLine {
   payload: ILinesRequest;
}

function* retriveLines({ payload }: IRetrivalLine): Iterator<any> {
   try {
      if (!payload) yield put(onRetriveLinesFaild([]));
      const response: any = yield getLines(payload);
      const { data = {} } = response;

      if (data?.response === '0') yield put(onRetriveLinesSuccess(data?.data));
      else yield put(onRetriveLinesFaild([]));
   } catch (error) {
      yield put(onRetriveLinesFaild([]));
   }
}

export default function* lineSaga(): SagaIterator<any> {
   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
   // @ts-ignore
   yield takeLatest(LineActionType.retriveLines, retriveLines);
}
