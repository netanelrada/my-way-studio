import { all } from 'redux-saga/effects';
// Import your saga files here
import loginSaga from './loginSaga';
import lineSaga from './lineSaga';
import passengerShiftSaga from './passengerShiftsSaga';
import passengerSaga from './passengerSaga';

export default function* watchRootSaga(): Iterator<any> {
   yield all([loginSaga(), lineSaga(), passengerShiftSaga(), passengerSaga()]);
}
