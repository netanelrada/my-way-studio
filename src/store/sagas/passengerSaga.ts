import { takeLatest, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga/index';
import {
   IDeletePassengerSagaRequest,
   ISetSagaPassenger,
} from 'src/api/passengerMangerApi/types';
import {
   deletePassenger,
   addOrUpdatePassenger,
} from 'src/api/passengerMangerApi/passengerMangerApi';
import { FcResponseState } from 'src/api/types';
import { RefreshTokenStatus } from 'src/types/login';
import { PassengerActionType } from '../actions/actionType';

import {
   onDeletePassengersFaild,
   onDeletePassengersSuccess,
   onSetPassengersFaild,
   onSetPassengersSuccess,
} from '../actions/passengerAction';
import { ISagaRequest } from '../type';
import { setTokenRefreshStatus } from '../actions/loginAction';

function* setPassnger({
   payload,
}: ISagaRequest<ISetSagaPassenger>): Iterator<any> {
   try {
      if (!payload) yield put(onDeletePassengersFaild());
      const { address, passenger, ...requestToken } = payload;

      const apiResponse: any = yield addOrUpdatePassenger({
         addressJson: JSON.stringify(address || []),
         ...requestToken,
      });

      const { response, passCode } = apiResponse.data;

      if (response === FcResponseState.TokenExpired)
         setTokenRefreshStatus(RefreshTokenStatus.Invalid);

      if (+response === 0) {
         yield put(onSetPassengersSuccess({ ...passenger, passCode }));
      } else yield put(onSetPassengersFaild());
   } catch (error) {
      yield put(onSetPassengersFaild());
   }
}

function* deletePassnger({
   payload,
}: ISagaRequest<IDeletePassengerSagaRequest>): Iterator<any> {
   try {
      if (!payload) yield put(onDeletePassengersFaild());
      const { passengers, ...requestToken } = payload;

      const apiResponse: any = yield deletePassenger({
         passengers: passengers.join(',').toString(),
         ...requestToken,
      });
      const { response } = apiResponse.data;

      if (response === FcResponseState.TokenExpired)
         setTokenRefreshStatus(RefreshTokenStatus.Invalid);

      if (+response === 0) {
         yield put(onDeletePassengersSuccess(passengers));
      } else yield put(onDeletePassengersFaild());
   } catch (error) {
      yield put(onDeletePassengersFaild());
   }
}

export default function* passengerSaga(): SagaIterator<any> {
   yield takeLatest(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      PassengerActionType.setPassenger,
      setPassnger,
   );

   yield takeLatest(
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      PassengerActionType.deletePassenger,
      deletePassnger,
   );
}
