import { takeLatest, put, StrictEffect } from 'redux-saga/effects';
import { IAccount } from 'src/types/login';
import { validateToken, updateToken } from '../../api/api';
import { LoginActionType } from '../actions/actionType';
import * as actions from '../actions/loginAction';
import { ILoginState } from '../type';

interface IRes {
   response: string;
   data: object;
   autoLogin: string;
}

/*  set validateToken and update token */

function* initConnect({
   payload,
}: Partial<{ payload: { deviceToken: string } }>): any {
   try {
      if (!payload || !payload.deviceToken) {
         yield put(actions.initConnectFail({}));
         return;
      }

      const response = yield validateToken({
         deviceToken: payload?.deviceToken,
      });

      const { data = {} } = response;

      if (data?.response === '0') {
         const promises: Array<Promise<void>> = [];
         const loginData: ILoginState = data?.data[0] || {};
         const fcAccounts: IAccount[] = loginData.fcAccounts || [];
         const token: string = loginData.token || '';

         fcAccounts.forEach(({ proxyUrl, contactUUID, dbUrl }) => {
            promises.push(updateToken({ token, proxyUrl, dbUrl, contactUUID }));
         });
         yield Promise.all(promises);

         yield put(
            actions.initConnectSuccess({
               ...loginData,
               loginType: Number(data?.autoLogin),
            }),
         );
      } else yield put(actions.initConnectFail({}));
   } catch (error) {
      yield put(actions.initConnectFail({}));
   }
}
function* initLogout({
   payload,
}: Partial<{ payload: { token: string } }>): any {
   try {
      const response = yield validateToken({
         deviceToken: payload?.token || '',
      });
      const { data = {} } = response;

      // if (data?.response === '0') {
      //    console.log('initConnectSuccess data:', data);
      //    yield put(
      //       actions.initConnectSuccess({
      //          ...(data?.data[0] || {}),
      //          loginType: Number(data?.auoLogin),
      //       }),
      //    );
      // } else yield put(actions.initConnectFail({}));
   } catch (error) {
      yield put(actions.initConnectFail({}));
   }
}

export default function* loginSaga(): any {
   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
   // @ts-ignore
   yield takeLatest(LoginActionType.initialConfiguration, initConnect);
   // eslint-disable-next-line @typescript-eslint/ban-ts-comment
   // @ts-ignore
   yield takeLatest(LoginActionType.Logout, initLogout);
}
