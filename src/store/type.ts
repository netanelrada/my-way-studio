import { DefaultRootState } from 'react-redux';
import { ReqStatus } from 'src/api/types';
import { IAccount, RefreshTokenStatus, SingupType } from '../types/login';

export type ReduxDispatch<T> = {
   type: string;
   payload: T;
};

export type ReduxReducer<T> = {
   type: string;
   payload: T;
};

export type State = any;

export interface IReduxProvider<T = {}> {
   (state: State, payload: T): State;
}

export interface ISagaRequest<T = {}> {
   payload: T;
}

export interface ILoginState extends DefaultRootState {
   isAuthenticated: ReqStatus | null;
   token: string;
   userUUID?: string;
   mobile?: string;
   fcAccounts: IAccount[];
   gps_server: string;
   gps_token: string;
   loginType: SingupType | null;
   refreshTokenStatus?: RefreshTokenStatus | undefined;
   selectedFcAccount?: IAccount | null;
   selectedDate?: Date;
   isIgnoreInitialRoute?: boolean;
}
