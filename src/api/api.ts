import { AxiosResponse } from 'axios';
import querystring from 'querystring';
import { ILine, IClient, IDepartment, ILocation } from 'src/types/line';
import instance, { gpsServerUrl, myWayUrl, proxy } from './index';
import {
   IResponse,
   FcLogin,
   ILinesRequest,
   ILineRequest,
   IPassengerTypeResponse,
   IFcRequest,
} from './types';
import { createPayload } from './utilis';

export const validateToken = async (payload: {
   deviceToken: string;
}): Promise<{ data: object }> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${myWayUrl}/ws_MyWayStudio_Validate_Token`, data);
};

export const sendSMS = async (payload: {
   userMobile: string;
   // deviceToken: string | null;
}): Promise<any> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${myWayUrl}/ws_MyWayStudio_Send_Sms`, data);
};

export const validateSms = async (payload: {
   userMobile: string;
   verifyCode: string;
}): Promise<any> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${myWayUrl}/ws_MyWayStudio_Validate_Mobile`, data);
};

export const setLoginType = async (payload: {
   deviceToken: string;
   userUUID: string;
   autologin: number;
   userName?: string;
   userPass?: string;
   pinCode?: string;
}): Promise<FcLogin> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance
      .post(`${myWayUrl}/ws_MyWayStudio_Update_Auto_Login_Type`, data)
      .then((result) => {
         return result.data;
      });
};

export const loginByPassword = (payload: {
   deviceToken: string;
   userName?: string;
   userPass?: string;
}): Promise<FcLogin> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance
      .post(`${myWayUrl}/ws_MyWayStudio_Login_By_UserName`, data)
      .then((result) => {
         return result.data;
      });
};

export const loginByPinCode = (payload: {
   deviceToken: string;
   pinCode: string;
}): Promise<FcLogin> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance
      .post(`${myWayUrl}/ws_MyWayStudio_Login_By_PinCode`, data)
      .then((result) => {
         return result.data;
      });
};

export const logout = (payload: {
   deviceToken?: string;
   userUUID: string;
}): Promise<FcLogin> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance
      .post(`${myWayUrl}/ws_MyWayStudio_UnRegister`, data)
      .then((result) => {
         return result.data;
      });
};

export const getActualDriver = async (payload: {
   accountCode: string | number;
   lineCode: string | number;
}): Promise<{ response: string; accountCode: string; driverCode: string }> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${myWayUrl}/ws_MyWayStudio_Get_Actual_Driver`, data);
};

export const updateToken = ({
   dbUrl,
   ...payload
}: {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
   contactUUID: string;
}): Promise<any> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });
   return instance.post(`${proxy}${dbUrl}/ws_MyWayStudio_Update_Token`, data);
};

export const getUserDetails = async ({
   clinetProxy,
   dbUrl,
   ...payload
}: any): Promise<any> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(
      `${proxy}${dbUrl}/ws_MyWayStudio_Get_User_Details`,
      data,
   );
};

export const getLines = async ({
   clinetProxy,
   dbUrl,
   ...payload
}: ILinesRequest): Promise<IResponse<ILine>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${proxy}${dbUrl}/ws_MyWayStudio_Get_Lines`, data);
};

export const getLocation = async ({
   gps_server,
   ...payload
}: ILineRequest): Promise<IResponse<Array<ILocation>>> => {
   const data = querystring.stringify({
      ...payload,
   });

   return instance.post(`${gpsServerUrl}/ws_MyWayStudio_Get_CurrentPos`, data);
};

export const getClinets = async ({
   proxyUrl,
   dbUrl,
   token,
}: any): Promise<IResponse<{ clients: IClient[]; response: string }>> => {
   const data = querystring.stringify({
      ...createPayload({ token }),
   });

   return instance.post(`${proxy}${dbUrl}/ws_MyWayStudio_Get_Clients`, data);
};

export const getDepartments = async ({
   proxyUrl,
   dbUrl,
   token,
   clientCode,
}: any): Promise<
   IResponse<{ departments: IDepartment[]; response: string }>
> => {
   const data = querystring.stringify({
      ...createPayload({ token, clientCode }),
   });

   return instance.post(
      `${proxy}${dbUrl}/ws_MyWayStudio_Get_Departments`,
      data,
   );
};

export const getPassengerType = async ({
   proxyUrl,
   dbUrl,
   ...payload
}: IFcRequest): Promise<AxiosResponse<IPassengerTypeResponse>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${proxy}${dbUrl}/ws_MyWayStudio_Get_PassTypes`, data);
};
