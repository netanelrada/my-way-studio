import { useState, useEffect } from 'react';
import { AxiosResponse } from 'axios';

const useAxios = (
   request: <T = any, R = AxiosResponse<T>>() => Promise<R>,
): { response: any; error: any; isLoading: boolean } => {
   const [response, setResponse] = useState(null);
   const [error, setError] = useState('');
   const [isLoading, setIsLoading] = useState(true);

   const fetchData = () => {
      request()
         .then((res) => {
            setResponse(res.data);
         })
         .catch((err) => {
            setError(err);
         })
         .finally(() => {
            setIsLoading(false);
         });
   };

   useEffect(() => {
      fetchData();
      // eslint-disable-next-line react-hooks/exhaustive-deps
   }, [request]);

   return { response, error, isLoading };
};

export default useAxios;
