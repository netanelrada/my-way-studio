import axios from 'axios';
import { isoDateRegex, fleetDateFormat } from 'src/utilis/utilis';

export const proxy =
   'https://israelProxy.y-it.co.il/ProxyService/ProxyService.svc/Proxy?url=';

export const myWayUrl =
   'https://4ynczhja78.execute-api.eu-west-1.amazonaws.com';

export const gpsServerUrl =
   'https://vp7ldm0qg0.execute-api.eu-west-1.amazonaws.com';

const dateConvert = (key: string, value: unknown): unknown => {
   if (
      typeof value === 'string' &&
      (fleetDateFormat.test(value) || isoDateRegex.test(value))
   ) {
      return new Date(value);
   }

   return value;
};

const instance = axios.create({
   headers: {
      'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
   },
   transformResponse: (res) => {
      try {
         return JSON.parse(res, dateConvert);
      } catch (error) {
         // eslint-disable-next-line no-console
         console.error(error, res);
         return res;
      }
   },
});

export default instance;
