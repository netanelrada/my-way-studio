import { FcResponseState } from '../types';

export interface IPassengerRequest {
   proxyUrl: string;
   dbUrl: string;
   token: string;
   clientCode?: string | number | null;
}

export interface IGetFutureLinesRequest {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
   passCode: string | number;
   clientCode?: string | number | null;
}

export interface IDeletePassengerRequest {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
}

export interface IResponseAddOrUpdatePassenger {
   response: FcResponseState;
   passCode: number | string;
}

export interface IDeletePassengerApiRequest extends IDeletePassengerRequest {
   passengers: string;
}

export interface IDeletePassengerSagaRequest extends IDeletePassengerRequest {
   passengers: number[] | string[];
}

export interface ISetPassengerRequest {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
   passcode?: string; // קוד נוסע – אם נשלח  - נוסע קיים אם לא נשלח – נוסע חדש
   clientCode: string | number; // קוד לקוח אליו שייך הנוסע
   departmentCode?: string | number; // אם המחלקה קיימת נעביר קוד מחלקה
   departmentName?: string; // אם המחלקה לא קיימת נעביר שם מחלקה
   passTypeCode?: number; // אם סוג נוסע קיים נעביר קוד סוג נוסע
   passType?: number; // אם לא קיים סוג נוסע נעביר קוד סוג נוסע
   internalCode?: string | undefined | number; // קוד פנימי של הנוסע
   firstName: string;
   lastName: string;
   mobilePhone?: string;
   phone1?: string;
   eMail?: string;
   contactName?: string;
   contactPhone1?: string;
   addressJson?: string; // אם לא שולחים עיר ושולחים קוד כתובת – הכתובת תמחק
}

export interface IAddress {
   isDefault: string;
   addressCode: string;
   addressIndex?: string;
   city?: string;
   street?: string;
   houseNum?: string;
   remarks?: string;
   isNew?: boolean;
}

export interface IPassenger {
   passCode: string;
   passId: string;
   firstName: string;
   lastName: string;
   fullName: string;
   internetLevel: string;
   passTypeCode: string;
   mobilePhone: string;
   phone1: string;
   eMail: string;
   contactName: string;
   departmentCode?: string;
   departmentName?: string;
   contactPhone1: string;
   address: IAddress[];
   mainAdress?: string;
   remarks?: string;
   isSelected?: boolean;
   isActive: string;
}

export interface ISetSagaPassenger extends ISetPassengerRequest {
   address: IAddress[];
   passenger: IPassenger;
}

export interface IModefiedPassenger extends IPassenger {
   supplierMobileNumer?: string;
   noSuppliermobileNumer?: string;

   supplierphone1?: string;
   noSupplierPhone1?: string;
}

export interface IFutureLine {
   account_code: number;
   is_pickup: number;
   client_name: string;
   line_description: string;
   line_status: number;
   line_date: string;
   station_time: string;
   driver_code: string;
   driver_name: string;
   driver_mobile: string;
   city: string;
   street: string;
   house: string;
   remarks: string;
   start_time: string;
   end_time: string;
}

export interface IEditPassenger {
   clientCode: string;
   departmentCode: string;
   departmentName: string;
   passType: string;
   internalCode: string;
   firstName: string;
   lastName: string;
   mobilePhone: string;
   phone1: string;
   eMail: string;
   contactName: string;
   contactPhone1?: string;
   address: IAddress[];
   mainAdress?: string;
   remarks?: string;
   isActive: string;
}

export interface IPassengerResponse {
   passList: IPassenger[];
   response: FcResponseState;
}
