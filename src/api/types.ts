import { ILine } from 'src/types/line';

export interface IResponse<T> {
   response: FcResponseState;
   data: T;
}

export enum QueryKey {
   VALIDATE_TOKEN = 'VALIDATE_TOKEN',
   VALIDATE_SMS = 'VALIDATE_SMS',
}

export enum ReqStatus {
   LOADING = 'LOADING',
   SUCCESS = 'SUCCESS',
   FAIL = 'FAIL',
}

export interface IFcRequest {
   token: string;
   proxyUrl?: string;
   dbUrl: string;
}

export interface FcAccount {
   accountCode: number;
   accountName: string;
   proxyUrl?: string;
   dbUrl: string;
   contactUUID: string;
}

export interface FcData {
   token: string;
   gps_server: string;
   gps_token: string;
   fcAccounts: FcAccount[];
}

export interface FcLogin {
   response: string;
   data: FcData[];
}

export interface ILinesRequest {
   relativeDate: Date | string;
   token: string;
   clinetProxy: string;
   dbUrl: string;
}

export interface ILineRequest {
   token: string;
   gps_server: string;
   driverCode: number;
   accountCode: number;
}

export enum FcResponseState {
   Ok = '0',
   TokenExpired = '1',
   MissingProprtis = '2',
}

export interface IPassengerType {
   passTypeCode: number;
   passType: string;
}

export interface IPassengerTypeResponse {
   passTypes: IPassengerType[];
   response: FcResponseState;
}
