import querystring from 'querystring';
import { AxiosResponse } from 'axios';

import instance, { proxy } from '../index';
import { IResponse } from '../types';
import {
   IShifts,
   IShiftRequest,
   IPassengerRequest,
   IResponsePassenger,
   ISetShiftOrder,
   IDelShiftOrderApi,
   IPassengerDate,
} from './types';
import { createPayload } from '../utilis';

export const getShifts = async ({
   proxyUrl,
   dbUrl,
   ...payload
}: IShiftRequest): Promise<AxiosResponse<IResponse<Array<IShifts>>>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(
      `${proxy}${dbUrl}/ws_MyWayStudio_Get_Shifts_By_Dates`,
      data,
   );
};

export const getPassngerShifts = async ({
   proxyUrl,
   dbUrl,
   ...payload
}: IPassengerRequest): Promise<AxiosResponse<IResponsePassenger>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(
      `${proxy}${dbUrl}/ws_MyWayStudio_Get_Passsengers_4_Dates`,
      data,
   );
};

export const setPassngerShifts = async ({
   proxyUrl,
   dbUrl,
   ...payload
}: ISetShiftOrder): Promise<AxiosResponse<IPassengerDate>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(
      `${proxy}${dbUrl}/ws_MyWayStudio_Set_Shift_Order`,
      data,
   );
};

export const delPassngerShifts = async ({
   proxyUrl,
   dbUrl,
   ...payload
}: IDelShiftOrderApi): Promise<AxiosResponse<IPassengerDate>> => {
   const data = querystring.stringify({
      ...createPayload(payload),
   });

   return instance.post(`${proxy}${dbUrl}/ws_MyWayStudio_Del_Orders`, data);
};
