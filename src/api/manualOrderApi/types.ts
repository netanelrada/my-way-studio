export interface IShiftsDate {
   relativeDate: string;
   isActive: string;
}

export interface IShifts {
   shiftId: number;
   shiftName: string;
   pickupTime?: string | undefined;
   dropTime?: string | undefined;
   accountCode: number;
   shiftIndex: string;
   dates: IShiftsDate[];
}

export interface ISelectedShifts extends IShifts {
   dateByPickUp: IShiftsDate[];
   dateByDrop: IShiftsDate[];
}

export interface IShiftRequest {
   departmentCode?: number;
   token: string;
   dbUrl: string;
   proxyUrl?: string;
   fromDate: string;
   toDate: string;
}

export interface IPickupDropOrder {
   orderCode: string;
   shiftDate: string;
   shiftTime: string;
}

export interface IPassengerDate {
   relativeDate: string;
   pickupOrders: IPickupDropOrder[];
   dropOrders: IPickupDropOrder[];
}

export interface IPassengerShifts {
   accountCode: string;
   passId: string;
   fullName: string;
   departmentCode: string;
   departmentName: string;
   city: string;
   street: string;
   houseNum: string;
   pickupCourse: string;
   dropCourse: string;
   dates: IPassengerDate[];
}

export interface IResponsePassenger {
   response: string;
   passengers: IPassengerShifts[];
}

export interface IPassengerRequest {
   clientCode: number;
   token: string;
   proxyUrl?: string;
   dbUrl: string;
   fromDate: string;
   toDate: string;
}

export interface ISetShiftOrder {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
   pass_id: number;
   dropTime?: string;
   pickupTime?: string;
   relative_date: string;
   pickupPassengers?: number;
   dropPassengers?: number;
}

export interface ISetShiftOrderSaga extends ISetShiftOrder {
   passenger: IPassengerShifts;
}

export interface IDelShiftOrder {
   proxyUrl?: string;
   dbUrl: string;
   token: string;
   pass_id: number;
   relative_date: string;
}

export interface IDelShiftOrderSaga extends IDelShiftOrder {
   orders: number[];
   passenger: IPassengerShifts;
   isPickUp: boolean;
}

export interface IDelShiftOrderApi extends IDelShiftOrder {
   orders: string;
}
